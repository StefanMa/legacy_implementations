from typing import Union
from abc import ABC, abstractmethod
from enum import Enum
from zeep import Client
from lxml import objectify
from sila2comlib.com.simulated_serial import SimulatedSerial
from sila1Receiver import MyReceiver
import logging
from copy import deepcopy
import os
from time import sleep
from threading import Thread
from simplejson import dumps
import json
import time


class ShakingMode(Enum):
    Slow = 0
    Medium = 1
    Fast = 2


# this plate reader can only meassure certain wavelengths
class Wavelength(Enum):
    WL_405 = 405
    WL_450 = 450
    WL_620 = 620


class ContainerFormat(Enum):
    P_96 = 96
    P_384 = 384


class MeassurementMode(Enum):
    Normal = 0
    Fast = 1


# dictionaries containing the names of the different commands and their parameters
sync_commands = dict(
    GetDeviceIdentification=["requestId", "lockId"],
    GetStatus=["requestId"],
)
sync_answer = dict(
    GetDeviceIdentification=["deviceDescription"],
    GetStatus=["deviceId", "state", "subStates", "locked", "PMSId", "currentTime"],
)
mandatory_commands = dict(
    Abort=["requestId", "lockId"],
    Initialize=["requestId", "lockId"],
    LockDevice=["requestId", "lockId", "lockTimeout", "eventReceiverURI", "PMSId"],
    Reset=["requestId", "lockId", "deviceId", "eventReceiverURI", "PMSId", "errorHandlingTimeout", "simulationMode"],
    UnlockDevice=["requestId", "lockId"],
)
required_commands = dict(
    GetParameters=["requestId", "lockId"],
    CloseDoor=["requestId", "lockId"],  # closes door
    OpenDoor=["requestId", "lockId"],  # opens door
    SetParameters=["requestId", "lockId", "paramsXML"],
)
optional_commands = dict(
    ExecuteMethod=["requestId", "methodName", "lockId", "priority"],
    GetTemperature=["requestId", "lockId"],
    ReadPlate=["requestId", "lockId", "index"],
    ReadWell=["requestId", "lockId", "index"],
    SetTemperature=["requestId", "lockId", "temperature"],
)
custom_commands = dict(
    GetReport=["requestId", "lockId", "filename"],
    GetErrors=["requestId", "lockId"],
)
async_commands = mandatory_commands.copy()
async_commands.update(required_commands)
async_commands.update(optional_commands)
async_commands.update(custom_commands)
all_commands = sync_commands.copy()
all_commands.update(async_commands)


allowed_params = {"UserActionTime", "EnableStatusEvent", "GasAtmosphereO2", "GasAtmosphereCO2", "MeasurementStep",
                  "Wavelength1", "Wavelength2", "FilterPosition", "OutputSessionPath"}


def construct_paramXML(**kwargs):
    return ""


sila1_returnCodes = {
    1: "Success",
    2: "Asynchronous command accepted",
    3: "Asynchronous command has finished",
    4: "Device is busy due to other command execution",
    5: "Error on lockId",
    6: "Error on requestId",
    7: "Error on deviceId",
    8: "Error on certificate check",
    9: "Command not allowed in this state",
    10: "Error in Data sent to Event Receiver",
    11: "Invalid command parameter",
    12: "Finished with warning",
    13: "Command unexecuted de-queued due to error in previous command execution"
}


# skanIT specifications ... no idea which one is correct
baseAddress2 = 'http://localhost:8733/MIP/Reader?singleWsdl'
file_ext = '.skax'


class FunctionStatus(Enum):
    WAITING = 0
    RUNNING = 1
    SUCCESSFUL = 2
    ERROR = 3
    REALLY_FINISHED = 4  # the device returns code 3 before OpenDoor/CloseDoor are really done


def retCode_to_status(code):
    if code in [2]:
        return FunctionStatus.RUNNING
    elif code in [1, 3, 12]:
        return FunctionStatus.SUCCESSFUL
    else:
        return FunctionStatus.ERROR


class FunctionInfo:
    def __init__(self, uuid, name, status=FunctionStatus.WAITING, args=[]):
        self.uuid = uuid
        self.name = name
        self.status = status
        self.start = time.time()
        self.duration = exp_duration[name] if name in exp_duration else 0
        self.args = args


class PlateReaderProtocol(ABC):
    """
    A class for utility functions to enable easy communication with a PlateReader via the inherent protocol
    """
    def __init__(self, client: Union[Client, SimulatedSerial]):
        self.method_dir = os.path.join(os.path.dirname(__file__), 'methods')
        self.client = client
        self.is_simulation = not isinstance(client, Client)
        self.stop = False
        self.inner_state = InnerStatus(name='varioskan', mother=self, simulation_mode=self.is_simulation,
                                               defaults=dict(
                                                    carrier_in=True,
                                                    carrier_occupied=False,
                                                    executing_protocol=False,
                                                    protocol_requestId=None,
                                                    data_dir=self.method_dir,
                                                    next_request_id=0
                                               ))
        self.deviceId = "0"
        self.lockId = '342bb9ba-0960-4010-8702-b9991dc66ace'  # 302C753D-B45F-4A1B-A82A-57F108AE07C7
        if self.is_simulation:
            self.client.register_receiver(self.handle_status, self.handle_response, self.handle_data)
        else:
            self.receiver = MyReceiver()
            self.receiver.run(in_thread=True)
            self.receiverURI = "http://localhost:8008/"
            self.receiver.register_status_handler(self.handle_status)
            self.receiver.register_data_handler(self.handle_data)
            self.receiver.register_response_handler(self.handle_response)
            self.receiver.register_error_handler(self.handle_error)
        self.delay = 1
        self.function_info = {}
        self.function_result = {}

    def init(self):
        request_id, response = self.send_command("GetStatus")
        if response.state.lower() == "startup":
            request_id, response = self.send_command("Reset", deviceId=self.deviceId, eventReceiverURI=self.receiverURI,
                              PMSId="P1", errorHandlingTimeout=10, simulationMode=self.is_simulation)
            timeout = 10
            start = time.time()
            while time.time() - start < timeout:  # TODO make functions finish() and really_finish()
                sleep(1.)
                if self.function_finished(request_id):
                    break
            request_id, response = self.send_command("GetStatus")
        if response.state.lower() == "standby":
            self.send_command("Initialize")

    def is_idle(self):
        request_id, response = self.send_command("GetStatus")
        return response.state.lower() == "idle"

    def function_running(self, request_id):
        return self.function_info[request_id].status in [FunctionStatus.RUNNING, FunctionStatus.WAITING]

    def function_success(self, request_id):
        return self.function_info[request_id].status in [FunctionStatus.SUCCESSFUL, FunctionStatus.REALLY_FINISHED]

    def function_finished(self, request_id):
        return self.function_info[request_id].status == FunctionStatus.REALLY_FINISHED

    def send_command(self, command, **kwargs):
        try:
            # check whether command exists
            if command not in all_commands:
                print(f"command {command} does not exist.")
                return
            kwargs["requestId"] = self.inner_state.next_request_id
            kwargs["lockId"] = self.lockId
            # check whether all required parameters are given
            for required in all_commands[command]:
                if required not in kwargs:
                    print(f"missing parameter {required}")
                    return
            # order of parameters is important
            args = [kwargs[param_name] for param_name in all_commands[command]]
            print(f"sending command {command} with requestID {self.inner_state.next_request_id}")
            self.function_info[self.inner_state.next_request_id] = \
                FunctionInfo(self.inner_state.next_request_id, command, args=args)
            sila1_response = self.execute(command, args)
            # sila1_response = self.client.service.__getattribute__(command)(*args)
            # synchronous commands are executed instantly
            if command in sync_commands:
                pass
            else:
                message = sila1_response.message
                ret_value = sila1_response.returnCode
                self.function_info[self.inner_state.next_request_id].status = retCode_to_status(ret_value)
                print(f"command {command} got message {message} and returnCode {sila1_returnCodes[ret_value]}")
                self.function_result[self.inner_state.next_request_id] = None
            # return the id of the sent command
            self.inner_state.next_request_id += 1
            return self.inner_state.next_request_id - 1, sila1_response
        except Exception as ex:
            print(f"Sending of command {command} did not work: {ex}")

    # this is ugly, but necessary because zeep client did fuck up its __getattribute__()
    def execute(self, command, args):
        if command == "GetStatus":
            return self.client.service.GetStatus(*args)
        elif command == "GetDeviceIdentification":
            return self.client.service.GetDeviceIdentification(*args)
        elif command == "CloseDoor":
            return self.client.service.CloseDoor(*args)
        elif command == "OpenDoor":
            return self.client.service.OpenDoor(*args)
        elif command == "ExecuteMethod":
            return self.client.service.ExecuteMethod(*args)
        elif command == "GetParameters":
            return self.client.service.GetParameters(*args)
        elif command == "Reset":
            return self.client.service.Reset(*args)
        elif command == "Initialize":
            return self.client.service.Initialize(*args)
        elif command == "SetParameters":
            return self.client.service.SetParameters(*args)
        elif command == "GetTemperature":
            return self.client.service.GetTemperature(*args)
        elif command == "SetTemperature":
            return self.client.service.SetTemperature(*args)
        else:
            print(f"Command {command} not implemented.")

    def handle_status(self, device_id, return_value, event_description):
        ret_code = return_value['returnCode']
        message = return_value['message']
        if ret_code == 1:  # that means success
            if "ready to use" in message:
                for info in self.function_info.values():
                    # simply mark all info as really finished... that's easiest
                    if info.status == FunctionStatus.SUCCESSFUL and info.name in ["Reset", "OpenDoor", "CloseDoor"]:
                        info.status = FunctionStatus.REALLY_FINISHED
            # parse event_description
            # event = objectify.fromstring(event_description)
        logging.debug(f"Got status update")

    def handle_data(self, request_id, data_value):
        pass

    # gets called by the simulation or the sila1-eventReceiver
    def handle_response(self, request_id, return_value, response_data):
        self.function_result[request_id] = response_data
        ret_code = return_value['returnCode']
        message = return_value['message']
        print(f"got response id={request_id}, code={ret_code}")
        # TODO this should be done via inner_state.protocol_requestId. function_info[request_id] might not exists
        f_type = self.function_info[request_id].name
        if retCode_to_status(ret_code) == FunctionStatus.SUCCESSFUL:
            if f_type == "ExecuteMethod":
                print("Protocol finished")
                self.inner_state.executing_protocol = False
        self.function_info[request_id].status = retCode_to_status(ret_code)

    def handle_error(self, request_id, return_value, continuation_task):
        pass


# TODO use python settings
# expected duration of all commands, that set the COMMAND_IN_EXECUTION bit
exp_duration = dict(
    CloseDoor=20,  # device seems to check something... maybe occupancy
    OpenDoor=10,
    Initialize=1,
    Reset=3,
)


class InnerStatus:
    """
    In this class we handle the status data of any device which is not saved by the device itself.
    This data is saved to a json file frequently, so the server can be restarted without loosing this data.
    """
    def __init__(self, name, defaults, simulation_mode=True, mother=None):
        self.filename = f"{name}_inner_status_{'sim' if simulation_mode else 'real'}.json"
        self.mother = mother
        self.defaults = defaults
        for attr, value in defaults.items():
            self.__setattr__(attr, value)
        # load the last state
        if self.filename not in os.listdir():
            self.reset()
            self.save()
        else:
            self.load()
        self.auto_save_thread = Thread(target=self.auto_save, daemon=True)
        self.auto_save_thread.start()
        self.last_data = None
        self.runtime_backup()

    def copy_real_to_sim(self):
        self.save(self.filename.replace('real', 'sim'))

    def auto_save(self):
        while not self.mother.stop:
            sleep(.2)
            if self.changes_happened():
                self.save()

    def changes_happened(self):
        for dat, dat_backup in zip([self.__getattribute__(attr) for attr in self.defaults.keys()],
                                   [self.last_data[attr] for attr in self.defaults.keys()]):
            if not dumps(dat) == dumps(dat_backup):
                return True
        return False

    def save(self, filename=None):
        if filename is None:
            filename = self.filename
        self.runtime_backup()
        with open(filename, "w") as outstream:
            json.dump(self.last_data, outstream, indent=4)

    def load(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, "r") as instream:
            data = json.load(instream)
            for attr in self.defaults.keys():
                self.__setattr__(attr, data[attr])
        self.runtime_backup()

    def runtime_backup(self):
        self.last_data = {attr: deepcopy(self.__getattribute__(attr)) for attr in self.defaults.keys()}

    def reset(self):
        for attr, value in self.defaults.items():
            self.__setattr__(attr, value)


class PlateReaderProtocolReal(PlateReaderProtocol, Client):
    def __init__(self):
        PlateReaderProtocol.__init__(self, self)
        Client.__init__(self, baseAddress2)
        self.method_dir = r"C:\Users\Public\Documents\varioskanLUX\tests"
        self.inner_state.data_dir = r"C:\Users\Public\Documents\varioskanLUX\tests"
