"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Container Carrier Controller_defined_errors*

:details: ContainerCarrierController Defined SiLA Error factories:
    General carrier controller to move a carrier in/out and to check the occupation of a carrier.
    There are two states: Carrier In for processing and Carrier Out for transfer.

:file:    ContainerCarrierController_defined_errors.py
:authors: mark doerr, stefan maak

:date: (creation)          2021-09-15T20:26:13.294717
:date: (last modification) 2021-09-15T20:26:13.294717

.. note:: Code generated by sila2codegenerator 0.3.6

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.1"

# import general packages
from sila2lib.error_handling.server_err import SiLAExecutionError

# SiLA Defined Error factories

def generate_def_error_CarrierMovementInterrupted(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "CarrierMovementInterrupted"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""Moving container carrier was interrupted. Check if any obstacle prevents movement.
     \n{extra_message}"""
    return SiLAExecutionError(error_identifier="CarrierMovementInterrupted",
                         msg=msg)

