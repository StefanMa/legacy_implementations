#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*ThermoVarioskanLUX client*

:details: ThermoVarioskanLUX:
    Compact SiLA2 service for an Thermo Varioskan LUX plate reader.

:file:    DataProvider_client.py
:authors: mark doerr, stefan maak

:date: (creation)          2021-09-27T11:49:25.684496
:date: (last modification) 2021-09-27T11:49:25.684496

.. note:: Code generated by sila2codegenerator 0.3.6

_______________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.0.2"

# import general packages
import logging
import argparse
import grpc
import time
import datetime

# import meta packages
from typing import Union, Optional

# import SiLA2 library modules
from sila2lib.framework import SiLAFramework_pb2 as silaFW_pb2
from sila2lib.sila_client import SiLA2Client
from sila2lib.framework.std_features import SiLAService_pb2 as SiLAService_feature_pb2
from sila2lib.error_handling import client_err
from sila2lib.error_handling.client_err import SiLAClientError
import sila2lib.utils.py2sila_types as p2s
#   Usually not needed, but - feel free to modify
# from sila2lib.framework.std_features import SimulationController_pb2 as SimController_feature_pb2

# import feature gRPC modules
# Import gRPC libraries of features
from ContainerCarrierController.gRPC import ContainerCarrierController_pb2
from ContainerCarrierController.gRPC import ContainerCarrierController_pb2_grpc
# import default arguments for this feature
from ContainerCarrierController.ContainerCarrierController_default_arguments import default_dict as ContainerCarrierController_default_dict
from ProtocolExecutionService.gRPC import ProtocolExecutionService_pb2
from ProtocolExecutionService.gRPC import ProtocolExecutionService_pb2_grpc
# import default arguments for this feature
from ProtocolExecutionService.ProtocolExecutionService_default_arguments import default_dict as ProtocolExecutionService_default_dict
from RobotInteractionServicePR.gRPC import RobotInteractionServicePR_pb2
from RobotInteractionServicePR.gRPC import RobotInteractionServicePR_pb2_grpc
# import default arguments for this feature
from RobotInteractionServicePR.RobotInteractionServicePR_default_arguments import default_dict as RobotInteractionServicePR_default_dict
from TemperatureControllerPR.gRPC import TemperatureControllerPR_pb2
from TemperatureControllerPR.gRPC import TemperatureControllerPR_pb2_grpc
# import default arguments for this feature
from TemperatureControllerPR.TemperatureControllerPR_default_arguments import default_dict as TemperatureControllerPR_default_dict
from DataProvider.gRPC import DataProvider_pb2
from DataProvider.gRPC import DataProvider_pb2_grpc
# import default arguments for this feature
from DataProvider.DataProvider_default_arguments import default_dict as DataProvider_default_dict


# noinspection PyPep8Naming, PyUnusedLocal
class DataProviderClient:
    """
        Compact SiLA2 service for an Thermo Varioskan LUX plate reader.

    .. note:: For an example on how to construct the parameter or read the response(s) for command calls and properties,
              compare the default dictionary that is stored in the directory of the corresponding feature.
    """
    # The following variables will be filled when run() is executed
    #: Storage for the connected servers version
    server_version: str = ''
    #: Storage for the display name of the connected server
    server_display_name: str = ''
    #: Storage for the description of the connected server
    server_description: str = ''

    def __init__(self,
                 channel = None):
        """Class initialiser"""

        # Create stub objects used to communicate with the server
        self.DataProvider_stub = \
            DataProvider_pb2_grpc.DataProviderStub(channel)
        

        # initialise class variables for server information storage
        self.server_version = ''
        self.server_display_name = ''
        self.server_description = ''

    def SetDataDir(self,
                Directory: str = 'default string'
                     ): # -> (DataProvider):
        """
        Wrapper to call the unobservable command SetDataDir on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling SetDataDir:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = DataProvider_pb2.SetDataDir_Parameters(
                                    Directory=silaFW_pb2.String(value=Directory)
                )
    
            response = self.DataProvider_stub.SetDataDir(parameter, metadata)
            logging.debug(f"SetDataDir response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return 
    
    
    def GetData(self,
                FileName: str = 'default string'
                     ): # -> (DataProvider):
        """
        Wrapper to call the unobservable command GetData on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling GetData:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = DataProvider_pb2.GetData_Parameters(
                                    FileName=silaFW_pb2.String(value=FileName)
                )
    
            response = self.DataProvider_stub.GetData(parameter, metadata)
            logging.debug(f"GetData response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response.Data.value
    
    

    def Get_DataDir(self) \
            -> DataProvider_pb2.Get_DataDir_Responses:
        """Wrapper to get property DataDir from the server."""
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Reading unobservable property DataDir:")
        try:
            response = self.DataProvider_stub.Get_DataDir(
                DataProvider_pb2.Get_DataDir_Parameters()
            )
            logging.debug(
                'Get_DataDir response: {response}'.format(
                    response=response
                )
            )
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response.DataDir.value
    
    def Get_AvailableData(self) \
            -> DataProvider_pb2.Get_AvailableData_Responses:
        """Wrapper to get property AvailableData from the server."""
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Reading unobservable property AvailableData:")
        try:
            response = self.DataProvider_stub.Get_AvailableData(
                DataProvider_pb2.Get_AvailableData_Parameters()
            )
            logging.debug(
                'Get_AvailableData response: {response}'.format(
                    response=response
                )
            )
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return [file.value for file in response.AvailableData]
    
    

    @staticmethod
    def grpc_error_handling(error_object: grpc.Call) -> None:
        """Handles exceptions of type grpc.RpcError"""
        # pass to the default error handling
        grpc_error =  client_err.grpc_error_handling(error_object=error_object)

        logging.error(grpc_error.error_type)
        if hasattr(grpc_error.message, "parameter"):
            logging.error(grpc_error.message.parameter)
        logging.error(grpc_error.message.message)
        if grpc_error.error_type == client_err.SiLAError.DEFINED_EXECUTION_ERROR:
          if grpc_error.message.errorIdentifier == 'FileNotExisting' :
            raise FileNotExistingError(grpc_error.message.message)
        

class FileNotExistingError(SiLAClientError):
    """File Not Existing
    """
    pass


