"""
A unittest for the cytomat client-server pair. It uses the virtual_cytomat to emulate a real one.
The test can be run in fast-mode for fast testing or in real mode for realistic reaction/working times of the device.
"""
import time
from time import sleep, time
import unittest
import os
from threading import Thread
from sila2_simulation.deviceTest import DeviceTest
from ThermoVarioskanLUX_client import ThermoVarioskanLUXClient
from ThermoVarioskanLUX_server import ThermoVarioskanLUXServer
from sila2lib.error_handling.client_err import SiLAClientError


class PlateReaderTest(DeviceTest, unittest.TestCase):
    def is_error(self, feedback) -> bool:
        # evaluate whether the feedback is an error
        return issubclass(type(feedback), SiLAClientError)

    def setUp(self, port=50051) -> None:
        super().setUp()
        self.server = ThermoVarioskanLUXServer(cmd_args=self.create_args(port=port, name="Reader"),
                                               simulation_mode=True, block=False)
        # give the server some time to register with zeroconfig
        sleep(.5)
        self.server.hardware_interface.inner_state.reset()
        self.client = ThermoVarioskanLUXClient()

    def pull_temperature(self, temperature_provider, t_list):
        while temperature_provider.is_active():
            try:
                t_list[0] = temperature_provider.next().CurrentTemperature.value
                sleep(.1)
            except:
                pass

    def test_process(self):
        carrier = self.client.containerCarrierController_client
        thermometer = self.client.temperatureControllerPR_client
        robotAPI = self.client.robotInteractionServicePR_client
        protocol = self.client.protocolExecutionService_client
        data = self.client.dataProvider_client
        self.send_command(thermometer.Subscribe_CurrentTemperature, .2, .2)
        temperature_provider = self.feedback
        temperature = [0]
        t = Thread(target=self.pull_temperature, daemon=True, args=[temperature_provider, temperature])
        t.start()
        self.send_command(carrier.ContainerCarrierOut, 2.2, .3)
        self.send_command(carrier.ContainerCarrierIn, 2.2, .3)
        # get temperature (and save)
        # control temperature to 40 grad C
        self.send_command(thermometer.ControlTemperature, .5, .5, args=[273.15+40])
        assert not temperature[0] == 0
        temperature_provider.cancel()
        # start reading (should not work, since carrier out)
        # load_carrier via robot interaction service
        self.send_command(robotAPI.Get_CanReceiveContainer, .2, .2)
        # should be able te receive
        assert self.feedback[0]
        self.send_command(robotAPI.PrepareToReceiveContainerOnSite, 1.2, .2, args=[0])
        uuid = self.feedback.commandExecutionUUID
        self.send_command(robotAPI.PrepareToReceiveContainerOnSite_Info, .2, .2, args=[uuid])
        status = self.feedback
        timeout = 10
        start = time()
        while time()-start < timeout and status.is_active():
            try:
                n = status.next()
            except:
                pass
        self.send_command(robotAPI.PrepareToReceiveContainerOnSite_Result, .2, .2, args=[uuid])
        print("RESULT:", self.feedback)
        assert self.feedback
        self.send_command(robotAPI.ReceivedContainer, 2.2, .5, args=[0])

        method = "single_read"
        self.send_command(protocol.Get_AvailableProtocols, .2, .2)
        assert method+'.skax' in self.feedback
        self.send_command(data.SetDataDir, .5, .5, args=[os.path.join(os.path.dirname(__file__), 'methods')])
        timeout = 10
        start = time()
        while True:
            assert time()-start < timeout
            try:
                self.send_command(protocol.ExecuteProtocol, 5, 1, args=[method])
                break
            except:
                print("device not yet ready. waiting....")
                sleep(1)
        # put carrier in
        # get shaking-setting
        # change shaking setting
        # control if shaking setting changed
        # start shaking
        # check whether is shaking
        # set to kinetic, wl=620, n=3, interval=20s, first 5 rows
        # start reading
        # unload carrier (should not work)
        # ask for put_plate and get plate of robot-interaction. both should not work
        # check temperature: should be different
        # wait for reading to finish via uuid
        # carrier out (should not work, since is shaking)
        # stop shaking
        # get results (assume only the set 5 rows of results)
        # prepare to take plate (robot interaction)
        # assume carrier is out
        # take plate(robot interaction)
        # assume carrier empty

        sleep(1)


if __name__ == '__main__':
    mytest = PlateReaderTest()
    mytest.runSimulation()

