"""
This is meant so simulate the behavior of a ThermoFischer plate reader
"""

from threading import Timer, Thread
from time import sleep
import os
import json
import re
import logging
from lxml import objectify, etree
from plateReaderProtocol import *
from sila2comlib.com.simulated_serial import SimulatedSerial
import numpy as np


activities = {'waiting', 'loading', 'reading'}
namespace = 'http://www.w3.org/2001/XMLSchema'
sila_namespace = "http://sila-standard.org"


class SilaResponse:
    def __init__(self, ret_code=2, message="Hello World!"):
        self.returnCode = ret_code
        self.message = message
        self.duration = "a while"
        self.deviceClass = 4


class ResponseData:
    # temperature should be given as list[current, target]
    # param_dict should have keys {file_path, wl1, status_reports}
    def __init__(self, param_dict=None, temperatures=None):
        elm = objectify.ElementMaker(annotate=False, namespace=sila_namespace, nsmap={None: sila_namespace})
        if param_dict is None and temperatures is None:
            self.root = elm("ResponseData")
        else:
            params = []
            if temperatures is not None:
                params.append(elm("Parameter", elm("Float64", str(temperatures[0])),
                                  dict(parameterType="Float64", name="IncubatorTemperature")))
                params.append(elm("Parameter", elm("Float64", str(temperatures[1])),
                                  dict(parameterType="Float64", name="IncubatorSetTemperature")))
            if param_dict is not None:
                params.append(elm("Parameter", elm('Boolean', str(param_dict['status_reports'])),
                                  dict(parameterType="Boolean", name="EnableStatusReports")))
                params.append(elm("Parameter", elm('unsignedInt', str(param_dict['wl1'])),
                                  dict(parameterType="unsignedInt", name="Wavelength1")))
                params.append(elm("Parameter", elm('String', str(param_dict['file_path'])),
                                  dict(parameterType="String", name="OutputSessionPath")))
            param_set = elm("ParameterSet", *params)
            self.root = elm("ResponseData", param_set)

    def __str__(self):
        return etree.tostring(self.root, encoding='unicode', pretty_print=True)


class ReturnValue:
    def __init__(self, param_dict=None, temperatures=None):
        elm = objectify.ElementMaker(annotate=False, namespace=sila_namespace, nsmap={None: sila_namespace})

        self.root = elm("")

    def __str__(self):
        return etree.tostring(self.root, encoding='unicode')


class SilaResult:
    def __init__(self, request_id, ret_code=1, message="Hello World", data={}):
        self.requestId = request_id
        self.returnCode = ret_code
        self.message = message
        self.data = data


class VirtualPlateReader(SimulatedSerial):
    def __init__(self):
        self.service = self  # just so it can be called just like a zeep client
        self.current_temperature = 22
        self.target_temperature = 25
        self.settings = dict(
            shaking=False,
            shaking_mode=ShakingMode.Slow,
            kinetic_mode=False,
            kinetic_interval=10,  # seconds
            measurement_mode=MeassurementMode.Normal,
            n_read=1,
            wavelength_main=Wavelength.WL_620,
            wavelength_ref=Wavelength.WL_620,
            n_wells=ContainerFormat(96),
            wait_for_warmup=False,
        )
        self.readings_done = 0
        self.total_shaking_time = 1337
        self.n_rows = 8
        self.n_cols = 12
        self.tray_occupied = False
        self.tray_inside = True
        self.state = "standby"
        self.wells_to_read = np.array(
            [[j % 2 == 0 for j in range(self.n_cols)] for i in range(self.n_rows)]
        )
        # We read in the real occupancy, but will never write in it
        heater = Thread(target=self.heat, daemon=True)
        heater.start()
        # the function that is given the response after finishing a command
        self.status_fkt = None
        self.response_fkt = None
        self.data_fkt = None

    def register_receiver(self, status_fkt, response_fkt, data_fkt):
        self.status_fkt = status_fkt
        self.response_fkt = response_fkt
        self.data_fkt = data_fkt

    def simulate_command(self, command, request_id, **kwargs):
        def simulation(_self):
            sleep(.02)  # this is realistic and necessary
            response_data = None
            if command == "GetTemperature":
                response_data = ResponseData(temperatures=[_self.current_temperature, _self.target_temperature])
            elif command == "Initialize":
                _self.state = "initializing"
                _self.do_stuff(1)
                _self.state = "idle"
            elif command == "Reset":
                _self.state = "resetting"
                _self.do_stuff(1)
                _self.state = "standby"
            elif command == "SetTemperature":
                _self.target_temperature = kwargs["temperature"]
            elif command == "CloseDoor":
                _self.do_stuff(1)
                _self.tray_inside = True
            elif command == "OpenDoor":
                _self.do_stuff(1)
                _self.tray_inside = False
            elif command == "ExecuteMethod":
                _self.do_stuff(4)
                print(f"Executed method {kwargs['methodName']}")
            elif command == "GetParameters":
                response_data = ResponseData(param_dict=dict(wl1=600, status_reports=True, file_path="./methods"))
            elif command == "SetParameters":
                pass
            else:
                print("Unknown Command")
            # create default response if nothing special was created
            if response_data is None:
                response_data = ResponseData()
            # give the response to the registered handler
            _self.response_fkt(request_id, dict(returnCode=3, message="some message"), str(response_data))
            # make an extra status update to show, its done
            if command in ['Reset', 'OpenDoor', 'CloseDoor']:
                sleep(2)
                _self.status_fkt('0', dict(returnCode=1, message="Device ready to use"), 'Hello World')
        t = Thread(target=simulation, daemon=True, args=[self])
        t.start()

    def heat(self):
        while True:
            if abs(self.current_temperature - self.target_temperature) < .1:
                self.current_temperature = self.target_temperature
            elif self.current_temperature < self.target_temperature:
                self.current_temperature += .1
            elif self.current_temperature > self.target_temperature:
                self.current_temperature -= .1
            if self.fast_mode:
                sleep(.5)
            else:
                sleep(5)

    def do_enquiry(self, command):
        pass

    def GetStatus(self, request_id):
        result = SilaResponse()
        result.state = self.state
        return result

    def GetDeviceIdentification(self, request_id, lock_id):
        result = SilaResponse()
        return result

    def Initialize(self, request_id, lock_id):
        self.simulate_command("Initialize", request_id)
        return SilaResponse()

    def GetTemperature(self, request_id, lock_id):
        self.simulate_command("GetTemperature", request_id)
        # the data is returned in a separate response
        return SilaResponse()

    def SetTemperature(self, request_id, lock_id, temperature):
        self.simulate_command("SetTemperature", request_id, temperature=temperature)
        return SilaResponse()

    def GetParameters(self, request_id, lock_id):
        self.simulate_command("GetParameters", request_id)
        return SilaResponse()

    def SetParameters(self, request_id, lock_id, param_xml):
        self.simulate_command("SetParameters", request_id)
        return SilaResponse()

    def ExecuteMethod(self, request_id, method, lock_id, priority):
        if self.state == "idle":
            self.simulate_command("ExecuteMethod", request_id, methodName=method)
            # check for availability
            return SilaResponse()
        else:
            return SilaResponse(ret_code=9)

    def CloseDoor(self, request_id, lock_id):
        if self.state == "idle":
            self.simulate_command("CloseDoor", request_id)
            return SilaResponse()
        else:
            return SilaResponse(ret_code=9)

    def OpenDoor(self, request_id, lock_id):
        if self.state == "idle":
            self.simulate_command("OpenDoor", request_id)
            return SilaResponse()
        else:
            return SilaResponse(ret_code=9)

    def get_answer(self, command, terminator='\r', encoding='utf-8', max_wait=2):
        pass

    def do_stuff(self, duration):
        self.state = "busy"
        if not self.fast_mode:
            sleep(duration)
        else:
            sleep(duration/10)
        self.state = "idle"

    def fatal_crash(self, msg=''):
        logging.error(f"AAAAAHHHHHHHRGGGGGG!!!!!!  {msg}")

    def __str__(self):
        d = 8
        s = f'Plate Reader ({self.current_temperature} -> {self.target_temperature})\n'
        
        to_read = [''.join(['X' if self.wells_to_read[row, col] else 'O' for col in range(self.n_cols)])
            for row in range(self.n_rows)]

        settings = [
            f"shaking: {'On' if self.settings['shaking'] else 'Off'}",
            f"shaking_mode: {self.settings['shaking_mode'].name}",
            f"kinetic_mode: {'On' if self.settings['kinetic_mode'] else 'Off'}" ,
            f"kinetic_interval: {self.settings['kinetic_interval']} seconds",
            f"readings_done: {self.readings_done}/{self.settings['n_read']}",
            f"wavelength_main: {self.settings['wavelength_main'].value}",
            f"wavelength_ref: {self.settings['wavelength_ref'].value}",
            f"container format: {self.settings['n_wells'].value} wells",
            f"measurement mode: {self.settings['measurement_mode'].name}",
            f"wait for warmup: {'On' if self.settings['wait_for_warmup'] else 'Off'}",
            f"current state: {self.state}"
        ]

        if len(settings) > len(to_read):
            to_read += ["" for i in range(len(settings)-len(to_read))]
        else:
            settings += ["" for i in range(len(to_read)-len(settings))]
        
        for row, setting in zip(to_read, settings):
            s += f"{row.ljust(2*d, ' ')}{setting}\n"

        carrier = f"|{'XXXXXXXXX' if self.tray_occupied else '_________'}|"
        if not self.tray_inside:
            carrier = carrier.rjust(d*4, " ")
        else:
            carrier = carrier.ljust(d*4, " ")

        s += f"\n{carrier}\n"
        return s


class PlateReaderProtocolSimulated(PlateReaderProtocol, VirtualPlateReader):
    def __init__(self):
        VirtualPlateReader.__init__(self)
        PlateReaderProtocol.__init__(self, self)
