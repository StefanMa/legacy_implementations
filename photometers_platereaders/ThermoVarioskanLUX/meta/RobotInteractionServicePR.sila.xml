<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" MaturityLevel="Draft" Originator="de.tuberlin.bioprocess" Category="storing"
        xmlns="http://www.sila-standard.org"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>RobotInteractionServicePR</Identifier>
    <DisplayName>Robot Interaction Service</DisplayName>
    <Description>
        This service is designed to enable communication between a robot that transports containers and any device these
        containers are transported to. The latter should have this feature. The methods get called by the transporting
        robot. The methods include an enquiry whether a transport to/from which site is possible, an
        observable command to prepare access and a notification, that the transport was successful. The latter can be
        left empty in case the device has an own sensor for it. Finally there is a notification that the transport got
        cancelled. The communication should be:
        1. The robot asks all (usually two) participating devices whether transport is possible.
        2. If so, the robot tells all devices to prepare access and waits with each access for the corresponding
        positive reply.
        3. After each access, the robot senses to be successful the corresponding device is notified. If the transport
        failed (f.e. container was dropped) all waiting devices should receive cancel notifications.
    </Description>
    <Command>
        <Identifier>PrepareToGiveContainerFromSite</Identifier>
        <DisplayName>Prepare To Give Container From Site</DisplayName>
        <Description>
            Observable command telling a device to prepare for the robot taking a Container from the specified site.
            The observability models that this may take some time for some devices.
        </Description>
        <Observable>Yes</Observable>
        <Parameter>
            <Identifier>Site</Identifier>
            <DisplayName>Site</DisplayName>
            <Description>Index the the devices site that shall be accessed.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>Ready</Identifier>
            <DisplayName>Ready</DisplayName>
            <Description>
                Boolean indicating whether the device is ready for access or can finally not be accessed.
            </Description>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>PrepareToReceiveContainerOnSite</Identifier>
        <DisplayName>Prepare To Receive Container On Site</DisplayName>
        <Description>
            Observable command telling a device to prepare for the robot placing a Container on the specified site.
            The observability models that this may take some time for some devices.
        </Description>
        <Observable>Yes</Observable>
        <Parameter>
            <Identifier>Site</Identifier>
            <DisplayName>Site</DisplayName>
            <Description>Index the the devices site that shall be accessed.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>Ready</Identifier>
            <DisplayName>Ready</DisplayName>
            <Description>
                Boolean indicating whether the device is ready for access or can finally not be accessed.
            </Description>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>ReceivedContainer</Identifier>
        <DisplayName>Received Container</DisplayName>
        <Description>Notification, that a container was placed on the specified site.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Site</Identifier>
            <DisplayName>Site</DisplayName>
            <Description>The index of the site within the device.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
    </Command>
    <Command>
        <Identifier>ContainerGotTaken</Identifier>
        <DisplayName>Container Got Taken</DisplayName>
        <Description>Notification, that a container was taken from the specified site.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Site</Identifier>
            <DisplayName>Site</DisplayName>
            <Description>The index of the site within the device.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
    </Command>
    <Command>
        <Identifier>TransportCancelled</Identifier>
        <DisplayName>Transport Cancelled</DisplayName>
        <Description>
            Notification that the Transport to/from the specified site got cancelled due to unforeseen errors.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Site</Identifier>
            <DisplayName>Site</DisplayName>
            <Description>The index of the site within the device.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
    </Command>
    <Property>
        <Identifier>CanReceiveContainer</Identifier>
        <DisplayName>Can Receive Container</DisplayName>
        <Description>
            Enquiry a list of booleans indicating on which sites the device is currently able to receive a container.
            The list is sorted by site index.
        </Description>
        <Observable>No</Observable>
        <DataType>
            <List>
                <DataType>
                    <Basic>Boolean</Basic>
                </DataType>
            </List>
        </DataType>
    </Property>
    <Property>
        <Identifier>CanGiveContainer</Identifier>
        <DisplayName>Can Give Container</DisplayName>
        <Description>
            Enquiry a list of booleans indicating on which sites the device is currently able to provide a container.
            The list is sorted by site index.
        </Description>
        <Observable>No</Observable>
        <DataType>
            <List>
                <DataType>
                    <Basic>Boolean</Basic>
                </DataType>
            </List>
        </DataType>
    </Property>
</Feature>