import logging
logging.basicConfig(level=logging.INFO)
from pysimplesoap.server import SoapDispatcher, SOAPHandler
from threading import Thread
from http.server import HTTPServer


# noinspection PyPep8Naming,PyUnusedLocal
class MyReceiver:
    def __init__(self):
        sila_namespace = "http://sila-standard.org"
        url = "http://localhost:8008/"
        self.status_handler = None
        self.data_handler = None
        self.response_handler = None
        self.error_handler = None

        self.dispatcher = SoapDispatcher('EventReceiver',
                                         location=url,
                                         action=url,  # SOAPAction
                                         namespace=sila_namespace,
                                         trace=True,
                                         ns=True)

        self.dispatcher.register_function("StatusEvent", self.status_event,
                                          returns={'returnCode': int},
                                          args={'deviceId': str, 'returnValue': {'returnCode': int, 'message': str,
                                                                                 'duration': str, 'deviceClass': int},
                                                'eventDescription': str})
        self.dispatcher.register_function("DataEvent", self.data_event,
                                          returns={'returnCode': int},
                                          args={'requestId': int, 'dataValue': str})
        self.dispatcher.register_function("ResponseEvent", self.response_event,
                                          returns={'returnCode': int},
                                          args={'requestId': int, 'returnValue': {'returnCode': int, 'message': str,
                                                                                  'duration': str, 'deviceClass': int},
                                                'responseData': str})
        self.dispatcher.register_function("ErrorEvent", self.error_event,
                                          returns={'returnCode': int},
                                          args={'requestId': int, 'returnValue': str, 'continuationTask': str})

    def status_event(self, deviceId, returnValue, eventDescription):
        print(f"StatusEvent received: returnValue={returnValue}, eventDescription={eventDescription}")
        if self.status_handler is not None:
            self.status_handler(deviceId, returnValue, eventDescription)
        return 1

    def data_event(self, requestId, dataValue):
        print(f"DataEvent received")
        if self.data_handler is not None:
            self.data_handler(requestId, dataValue)
        return 1

    def response_event(self, requestId, returnValue, responseData):
        print(f"ResponseEvent for requestId {requestId} received:{responseData[:200]}. returnValue={returnValue}")
        if self. response_handler is not None:
            self.response_handler(requestId, returnValue, responseData)
        return 1

    def error_event(self, requestId, returnValue, continuationTask):
        print(f"ErrorEvent received")
        if self.error_handler is not None:
            self.error_handler(requestId, returnValue, continuationTask)
        return 1

    def register_status_handler(self, status_function):
        self.status_handler = status_function

    def register_data_handler(self, data_function):
        self.data_handler = data_function

    def register_response_handler(self, response_function):
        self.response_handler = response_function

    def register_error_handler(self, error_function):
        self.error_handler = error_function

    def run(self, in_thread=False):
        print(f"Starting event receiver soap server... on localhost, port 8008")
        httpd = HTTPServer(("", 8008), SOAPHandler)
        httpd.dispatcher = self.dispatcher
        if in_thread:
            t = Thread(target=httpd.serve_forever, daemon=True)
            t.start()
        else:
            httpd.serve_forever()


if __name__ == '__main__':
    server = MyReceiver()
    server.run()
