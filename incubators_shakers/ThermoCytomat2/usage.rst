Usage
=====

Use this package to control a cytomat

Using the simulation
~~~~~~~~~~~~~~~~~~~~

The best way to try out the cytomat server is openeing a terminal and running

>>> python cytomat_test -sm

This will start a server, connected with a simulated device. The script will start to
print a actualizing visualization of the simulated device to the console using *curses*.
it should look similar to this:

>>>
    Tower Shaker (Temperature: 25 --> 25, bs:00000000)
           Shaker_1       Shaker_2
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
              False          False
        Still(100 )    Still(100 )
    Shaker NOT initialized
    .
     Transfer station: OCCUPIED


Then open another tab in the console and start python or ipython.
There you have to import and create a client:

>>> from ThermoCytomat2_client import ThermoCytomat2Client
>>> client = ThermoCytomat2Client()

If no error message appears, your client successfully connected to the server in the other tab.
You can now start sending commands to your virtual cytomat and see it change.
For example:

>>> client.stackerController_client.LoadContainerToStacker(1, 4)
>>> client.shakingController_client.SetShakingFrequency(500, 1)
>>> client.shakingController_client.StartShaking()