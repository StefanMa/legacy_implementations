"""
This is meant so simulate the behavior of a ThermoFischer incubator shaker
"""

from threading import Timer, Thread, active_count
import threading
from time import sleep
from numpy.random import rand
import os
import json
import re
import logging
from sila2_simulation.simulated_serial import SimulatedSerial
from cytomat_protocol import *

COMMAND_IN_EXECUTION = 2 ** 0
LAST_COMMAND_SUCCESS = 2 ** 1
ERROR_OCCURRED = 2 ** 3
SHOVEL_OCCUPIED = 2 ** 4
PLATE_ON_TRANSFER = 2 ** 7

FAIL_CHANCE = 0


class VirtualCytomat(SimulatedSerial):
    def __init__(self):
        self.overview = 0
        self.current_temperature = 22
        self.target_temperature = 25
        self.is_shaking = [False, False]
        self.shaker_initialized = False
        self.shaker_addresses = ['20', '21']
        self.occupancy_known = False
        if "shakerSettings.json" not in os.listdir():
            self.frequency = [100, 100]
        else:
            with open("shakerSettings.json", "r") as infile:
                parameters = json.load(infile)
                self.frequency = [int(parameters["frequency1"]), int(parameters["frequency2"])]
        # We read in the real occupancy, but will never write in it
        if "stackOccupancy.json" not in os.listdir():
            self.stack_occupancy = [[False for position in range(16)] for stack in range(2)]
        else:
            with open("stackOccupancy.json", "r") as instream:
                self.stack_occupancy = json.load(instream)
        self.is_dead = False
        heater = Thread(name="heater", target=self.heat, daemon=True)
        heater.start()

    def heat(self):
        while not self.is_dead:
            if abs(self.current_temperature - self.target_temperature) < .1:
                self.current_temperature = self.target_temperature
            elif self.current_temperature < self.target_temperature:
                self.current_temperature += .1
            elif self.current_temperature > self.target_temperature:
                self.current_temperature -= .1
            # print(self.current_temperature, "-->", self.target_temperature)
            if not self.fast_mode:
                sleep(5)
            else:
                sleep(.1)

    def get_answer(self, command, terminator, encoding='utf-8', max_wait=2):
        command = command.strip()
        if not self.fast_mode:
            sleep(.02)
        # produce an answer here
        answer = "er 02"  # unknown command
        try:
            # all the movement commands can't be done when the device is busy
            if max([key in command for key in ["mv:st", "mv:ts", "mv:sc", "ll:vi", "ll:va"]]):
                if self.overview & COMMAND_IN_EXECUTION:
                    answer = f"er {'01'}"
                else:
                    answer = f"ok {self.overview}"
            if "mv:st" in command:
                if self.overview & PLATE_ON_TRANSFER:
                    answer = f"er {0x32}"
            elif "mv:ts" in command:
                if not self.overview & PLATE_ON_TRANSFER:
                    answer = f"er {0x31}"
            elif "mv:sc" == command:
                pass
            elif "ll:va" in command:
                if not self.shaker_initialized:
                    self.fatal_crash()
            elif "ll:vd" in command:
                answer = f"ok {self.overview}"

            elif command == "ch:bs":
                # this device sends its registry entry as hex numbers
                answer = f"bs {str(hex(self.overview)).replace('0x','')}"
                self.overview &= ~LAST_COMMAND_SUCCESS
            elif command == "ch:it":
                answer = f"tb {str(self.target_temperature).rjust(4,'0')}" \
                         f" {str(round(self.current_temperature, 1)).rjust(4,'0')}"
            elif "ll:it" in command:
                target = re.search(r'[\d.]+', command).group()
                if len(target) != 4:
                    error = 1/0
                self.target_temperature = float(target)
                answer = f"ok {self.overview}"
            elif "se:pb" in command:
                shaker_address = re.findall(r"[\d]+", command)[0]
                frequency = re.findall(r"[\d]+", command)[1]
                if shaker_address in self.shaker_addresses and len(frequency) == 4 and int(frequency) in range(50, 1200):
                    self.frequency[self.shaker_addresses.index(shaker_address)] = int(frequency)
                    answer = f"ok {self.overview}"
            elif "ch:pb" in command:
                shaker_address = re.findall(r"[\d]+", command)[0]
                if shaker_address in self.shaker_addresses:
                    answer = f"pb {str(self.frequency[self.shaker_addresses.index(shaker_address)]).rjust(4, '0')}"
            elif "ch:sc" in command:
                if self.occupancy_known:
                    pos = re.findall(r'\d+', command)[0]
                    p = int(pos)-1
                    if len(pos) == 3 and p in range(32):
                        answer = f"sc {int(self.stack_occupancy[p // 16][p % 16])}"
        except Exception as e:
            logging.error(str(e))
            logging.debug("Some error in command processing. Returning unknown-command-error")
            answer = "er 02"  # unknown command

        # simulate the execution here
        def process_command(_command):
            if "mv:st" in _command:
                pos = int(re.search(r'\d+', _command).group())-1
                if not self.stack_occupancy[pos//16][pos % 16]:
                    self.do_stuff(5, success=False)
                else:
                    if self.is_shaking[pos // 16]:
                        self.fatal_crash()
                    else:
                        self.do_stuff(5)
                        self.stack_occupancy[pos // 16][pos % 16] = False
                        self.spawn_plate_on_transfer()
            elif "mv:ts" in _command:
                pos = int(re.search(r'\d+', _command).group())-1
                if self.stack_occupancy[pos//16][pos % 16]:
                    self.fatal_crash()
                else:
                    if self.is_shaking[pos // 16]:
                        self.fatal_crash()
                    else:
                        success = rand() > FAIL_CHANCE
                        if success:
                            self.take_plate_from_transfer()
                        self.do_stuff(5, success=success)
                        if success:
                            self.stack_occupancy[pos // 16][pos % 16] = True
            elif "ll:vi" in command:
                self.is_shaking = [False, False]
                self.do_stuff(5)
                self.shaker_initialized = True
            elif "ll:va" in command:
                self.do_stuff(4)
                shaker_pos = re.search(r"\d+", command)
                if shaker_pos is not None:
                    shaker_pos = int(shaker_pos.group())-1
                    self.is_shaking[shaker_pos] = True
                else:
                    self.is_shaking = [True, True]
            elif "ll:vd" in command:
                self.do_stuff(1)
                shaker_pos = re.search(r"\d+", command)
                if shaker_pos is not None:
                    shaker_pos = int(shaker_pos.group())-1
                    self.is_shaking[shaker_pos] = False
                else:
                    self.is_shaking = [False, False]
            elif "mv:sc" == command:
                self.do_stuff(10)
                self.occupancy_known = True
        # actually simulate doing something if the answer is ok
        if "ok" in answer:
            t = Thread(name=f"call_of_{command}", target=process_command, args=[command], daemon=True)
            t.start()
        if "ch:bs" not in command:
            logging.debug(f"Got command {command}. Answered {answer}")
        return answer

    def spawn_plate_on_transfer(self):
        if self.overview & PLATE_ON_TRANSFER:
            logging.error("There is already a plate on the transfer station")
        else:
            self.overview |= PLATE_ON_TRANSFER

    def take_plate_from_transfer(self):
        if not self.overview & PLATE_ON_TRANSFER:
            logging.error("There is no plate on the transfer station")
        else:
            self.overview &= ~PLATE_ON_TRANSFER

    def do_stuff(self, duration, success=True):
        self.overview |= COMMAND_IN_EXECUTION
        if not self.fast_mode:
            sleep(duration)
        if success:
            self.overview |= LAST_COMMAND_SUCCESS
        else:
            self.overview |= ERROR_OCCURRED
            self.overview &= ~LAST_COMMAND_SUCCESS
        self.overview &= ~COMMAND_IN_EXECUTION

    def transfer_station_occupied(self):
        return self.overview & PLATE_ON_TRANSFER

    def fatal_crash(self, msg='Something dangerous happened to the device.'):
        logging.error(f"AAAAAHHHHHHHRGGGGGG!!!!!!  {msg}")

    def __str__(self):
        d = 15
        s = f"\nTower Shaker (Temperature: {self.current_temperature} --> {self.target_temperature}," \
            f" bs:{str(bin(self.overview)).replace('0b', '').rjust(8,'0')})\n"
        s += f"{'Shaker_1'.rjust(d,' ')}{'Shaker_2'.rjust(d,' ')}\n"
        for i in range(16):
            s += f"{str(self.stack_occupancy[0][i]).rjust(d)}{str(self.stack_occupancy[1][i]).rjust(d)}\n"
        s += f"{('Shaking' if self.is_shaking[0] else 'Still').rjust(9,' ')}({str(self.frequency[0]).ljust(4,' ')})"
        s += f"{('Shaking' if self.is_shaking[1] else 'Still').rjust(9,' ')}({str(self.frequency[1]).ljust(4,' ')})\n"
        s += f"Shaker {'' if self.shaker_initialized else 'NOT '}initialized\n"
        full = self.transfer_station_occupied()
        s += f"\n Transfer station: {'OCCUPIED' if full else 'EMPTY'}\n"
        return s

    def close(self):
        self.is_dead = True


class CytomatProtocolSimulated(CytomatProtocol, VirtualCytomat):
    def __init__(self):
        CytomatProtocol.__init__(self, self)
        VirtualCytomat.__init__(self)

