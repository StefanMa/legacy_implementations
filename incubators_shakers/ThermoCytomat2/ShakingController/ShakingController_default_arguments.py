# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import ShakingController_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()


default_dict['StartShaking_Parameters'] = {
    'UnitNumber': silaFW_pb2.Integer(value=1)
}

default_dict['StartShaking_Responses'] = {
    
}

default_dict['StopShaking_Parameters'] = {
    'UnitNumber': silaFW_pb2.Integer(value=1)
}

default_dict['StopShaking_Responses'] = {
    
}

default_dict['SetShakingFrequency_Parameters'] = {
    'TargetFrequency': silaFW_pb2.Integer(value=1),
    'UnitNumber': silaFW_pb2.Integer(value=1)
}

default_dict['SetShakingFrequency_Responses'] = {
    
}

default_dict['Get_IsCurrentlyShaking_Responses'] = {
    'IsCurrentlyShaking': [silaFW_pb2.Boolean(value=False)]
}

default_dict['Get_TargetShakingFrequency_Responses'] = {
    'TargetShakingFrequency': [silaFW_pb2.Integer(value=1)]
}

default_dict['Get_CurrentShakingFrequency_Responses'] = {
    'CurrentShakingFrequency': [silaFW_pb2.Integer(value=1)]
}

default_dict['Get_MinShakingFrequency_Responses'] = {
    'MinShakingFrequency': silaFW_pb2.Integer(value=1)
}

default_dict['Get_MaxShakingFrequency_Responses'] = {
    'MaxShakingFrequency': silaFW_pb2.Integer(value=1)
}

default_dict['Get_TotalShakingTime_Responses'] = {
    'TotalShakingTime': silaFW_pb2.Real(value=1.0)
}
