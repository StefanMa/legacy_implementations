<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" MaturityLevel="Draft" Originator="org.silastandard" Category="core"
         xmlns="http://www.sila-standard.org"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>StackerController</Identifier>
    <DisplayName>Stacker Controller</DisplayName>
    <Description>
        This feature is a generic stacker controller. It should be usable with any devices that stack containers in one or multiple stacks.
        Containers / Plates stacks can be organised by stackers or any other kind of physical device.
        Some stacker devices might only grant sequential access to the containers (non-random-access stacks).
        In case there is a random container access, one can specify the container by providing the stack and the position within the stack.
        Mind that in some cases the physical arrangement might not correspond to the logical representation: This feature could also
        represent a 2 dimensional deck, where the stack number corresponds to a column or row.
        The device might have multiple transfer positions. In that case the parameter TransferStation should be used with an enumeration of them.
        Reorganising of the containers within the stacker might be possible - commands for container reorganisation might follow in the future.
    </Description>
     <!-- Commands -->
    <Command>
        <Identifier>LoadContainerToStacker</Identifier>
        <DisplayName>Load Container to Stacker</DisplayName>
        <Description>
            Loads a container from transfer position to the specified stack and position.
            Raises a StackerPositionOccupied error, in case the position is already occupied
        </Description>
        <Observable>Yes</Observable>
        <Parameter>
            <Identifier>StackNumber</Identifier>
            <DisplayName>Stack Number - integer number </DisplayName>
            <Description>Select, which stack shall be used - integer</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>ContainerPosition</Identifier>
            <DisplayName>Container Position - integer number </DisplayName>
            <Description>Position of the Container in the (logical) stack - integer</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>TransferStation</Identifier>
            <DisplayName>Transfer Station - integer number </DisplayName>
            <Description>
                Select the id indicating which transfer station shall be used - integer.
                In case the device has only one transfer stations, this parameter can be omitted.
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <DefinedExecutionErrors>
            <Identifier>DeviceStillBusy</Identifier>
            <Identifier>StackerNotReady</Identifier>
            <Identifier>StackerPositionOccupied</Identifier>
            <Identifier>NoContainerInRequestedPosition</Identifier>
            <Identifier>PositionNotExisting</Identifier>
            <Identifier>CommandNotAccepted</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>UnloadContainerFromStacker</Identifier>
        <DisplayName>Unload Container From Stacker</DisplayName>
        <Description>
            Gets the container from the specified positions in the specified stacker and moves it onto a transfer position.
            Raises a NoContainerInRequestedPosition error in case no container was found there.
        </Description>
        <Observable>Yes</Observable>
        <Parameter>
            <Identifier>StackNumber</Identifier>
            <DisplayName>Stack Number - integer number </DisplayName>
            <Description>Select, which stack shall be used - integer</Description>
            <DataType>
                 <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>ContainerPosition</Identifier>
            <DisplayName>Container/Container Position - integer number </DisplayName>
            <Description>Position of the Container or Container in the (logical) stack - integer</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>TransferStation</Identifier>
            <DisplayName>Transfer Station - integer number </DisplayName>
            <Description>
                Select the id indicating which transfer station shall be used - integer.
                In case the device has only one transfer stations, this parameter can be omitted.
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <DefinedExecutionErrors>
            <Identifier>DeviceStillBusy</Identifier>
            <Identifier>StackerNotReady</Identifier>
            <Identifier>NoContainerInRequestedPosition</Identifier>
            <Identifier>PositionNotExisting</Identifier>
            <Identifier>TransferPositionOccupied</Identifier>
            <Identifier>CommandNotAccepted</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>NullifyOccupancy</Identifier>
        <DisplayName>Nullify Occupancy</DisplayName>
        <Description>Sets the internal memory of slot occupancy to empty for all slots</Description>
        <Observable>No</Observable>
    </Command>
    <Command>
        <Identifier>CheckOccupancy</Identifier>
        <DisplayName>Check Occupancy</DisplayName>
        <Description>
            Makes the stacker check in which positions there are currently containers.
            Warning: This may takes several minutes and should not be interrupted. Provides an observable boolean
            indicating whether the check is completed.
        </Description>
        <Observable>Yes</Observable>
        <Response>
            <Identifier>CheckCompleted</Identifier>
            <DisplayName>Check Completed</DisplayName>
            <Description>A boolean indicating whether the check is completed.</Description>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>DeviceStillBusy</Identifier>
            <Identifier>StackerNotReady</Identifier>
            <Identifier>CommandNotAccepted</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Property>
        <Identifier>NumFreePositions</Identifier>
        <DisplayName>Num Free Positions</DisplayName>
        <Description>The number of free/available/attainable positions of the device</Description>
        <Observable>No</Observable>
        <DataType>
            <Basic>Integer</Basic>
        </DataType>
    </Property>
    <Property>
        <Identifier>NextFreePosition</Identifier>
        <DisplayName>Next Free Position</DisplayName>
        <Description>
            The stack number and position of the next free/available/attainable position of the device
            - for faster loading. Given as
        </Description>
        <Observable>No</Observable>
        <DataType>
            <Structure>
                <Element>
                    <Identifier>Stack</Identifier>
                    <DisplayName>Stack</DisplayName>
                    <Description>Stack</Description>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Position</Identifier>
                    <DisplayName>Position</DisplayName>
                    <Description>Position</Description>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
    </Property>
    <Property>
        <Identifier>Occupancy</Identifier>
        <DisplayName>Occupancy</DisplayName>
        <Description>
            Provides the complete currently (internally saved) occupancy of all stacks given as a one dimensional list
            (because of restrictions) the number ans sizes can be requested with NumberOfStacks and PositionsPerStack
            [stack_1, .., stack_n] of booleans.
            To be sure the saved occupancy is correct you can first run the CheckOccupancy command.
        </Description>
        <Observable>No</Observable>
        <DataType>
            <List>
                <DataType>
                    <Basic>Boolean</Basic>
                </DataType>
            </List>
        </DataType>
    </Property>
    <Property>
        <Identifier>NumberOfStacks</Identifier>
        <DisplayName>Number Of Stacks</DisplayName>
        <Description>
            The Number of stacks (or boxes/arms/columns) in this device.
            The size of each can be queried with PositionsPerStack.
        </Description>
        <Observable>No</Observable>
        <DataType>
            <Basic>Integer</Basic>
        </DataType>
    </Property>
    <Property>
        <Identifier>PositionsPerStack</Identifier>
        <DisplayName>Positions Per Stack</DisplayName>
        <Description>The number of positions in each of the stacks (or boxes/arms/columns) of this device</Description>
        <Observable>No</Observable>
        <DataType>
            <Basic>Integer</Basic>
        </DataType>
    </Property>
  <DefinedExecutionError>
    <Identifier>StackerNotReady</Identifier>
    <DisplayName>Stacker Not Ready</DisplayName>
    <Description>
        The Stacker is currently not ready to take or give containers.
        For example switched off or shaking.
    </Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>NoContainerInRequestedPosition</Identifier>
    <DisplayName>No Container In Requested Position</DisplayName>
    <Description>There is currently no Container in the requested position.</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>StackerPositionOccupied</Identifier>
    <DisplayName>Stacker Position Not Available</DisplayName>
    <Description>The given position is currently occupied by a container.</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>TransferPositionOccupied</Identifier>
    <DisplayName>Transfer Position Occupied</DisplayName>
    <Description>The Transfer position is currently occupied by a container.</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>PositionNotExisting</Identifier>
    <DisplayName>Position Not Existing</DisplayName>
    <Description>The given position does not exist in this stacker.</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>DeviceStillBusy</Identifier>
    <DisplayName>Device Still Busy</DisplayName>
    <Description>The device is still busy executing another command. Please try again in a few seconds.</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>CommandNotAccepted</Identifier>
    <DisplayName>Command Not Accepted</DisplayName>
    <Description>The device did not execute the command (unknown reason).</Description>
  </DefinedExecutionError>
</Feature>
