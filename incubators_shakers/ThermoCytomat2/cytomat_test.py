"""
A unittest for the cytomat client-server pair. It uses the virtual_cytomat to emulate a real one.
The test can be run in fast-mode for fast testing or in real mode for realistic reaction/working times of the device.
"""
import os
import unittest
from time import sleep
import json
from sila2lib.error_handling.server_err import SiLAExecutionError
from sila2lib.error_handling.client_err import SiLAClientError
from ThermoCytomat2_client import ThermoCytomat2Client
from ThermoCytomat2_server import ThermoCytomat2Server
from sila2_simulation.deviceTest import DeviceTest, parse_command_line
from StackerController.StackerController_client import StackerPositionOccupiedError, DeviceStillBusyError,\
    TransferPositionOccupiedError
from TemperatureController.TemperatureController_client import TemperatureNotReachableError


class CytomatTest(DeviceTest, unittest.TestCase):
    def setUp(self, port=50051) -> None:
        super().setUp()
        self.server = ThermoCytomat2Server(cmd_args=self.create_args(port=port, name="Cytomat"),
                                           simulation_mode=True, block=False)
        # give the server some time to register with zeroconfig
        sleep(.5)
        self.server.hardware_interface.get_in_fast_mode()
        self.server.hardware_interface.inner_state.reset()
        self.client = ThermoCytomat2Client()

    def manual_adjustments(self):
        self.server.hardware_interface.spawn_plate_on_transfer()

    def is_error(self, feedback) -> bool:
        return issubclass(type(feedback), SiLAClientError)

    def test_process(self):
        stacker_client = self.client.stackerController_client
        shaker_client = self.client.shakingController_client
        temperature_client = self.client.temperatureController_client
        # test whether all json files were created
        for f in self.generated_files:
            assert f in os.listdir()

        # cytomat should be empty
        self.send_command(stacker_client.Get_Occupancy)
        assert not max([max(stack) for stack in self.feedback])
        # spawn plate
        self.server.hardware_interface.spawn_plate_on_transfer()
        sleep(.6)
        # put plate in 0,0
        self.send_command(stacker_client.LoadContainerToStacker, .1, .1, args=[0, 0])
        # spawn next plate
        self.server.hardware_interface.spawn_plate_on_transfer()
        sleep(.5)
        if not self.fast_mode:
            sleep(.6)

        # try to put plate in 0,0 ... should be occupied
        self.send_command(stacker_client.LoadContainerToStacker, .2, .2, args=[0, 0], to_fail=True,
                          error_type=StackerPositionOccupiedError)
        if not self.fast_mode:
            # should not work, since stacker still busy
            self.send_command(stacker_client.LoadContainerToStacker, 6, args=[1, 4], to_fail=True,
                              error_type=DeviceStillBusyError)
        # try again .. should work
        self.send_command(stacker_client.LoadContainerToStacker, 6, args=[1, 4])

        # next free position should be 0,1
        self.send_command(stacker_client.Get_NextFreePosition)
        assert self.feedback == [0, 1]

        # frequency should be the one from shakerSettings.json
        assert shaker_client.Get_TargetShakingFrequency()[0] ==\
               json.load(open('cytomat_inner_status_sim.json'))['frequency'][0]

        # get temperature
        self.send_command(temperature_client.Subscribe_CurrentTemperature, .3, .3)
        temperature = temperature_client.current_temperature

        # change temperature (the feature takes kelvin)
        # 444°C should be too much
        self.send_command(temperature_client.ControlTemperature, args=[273.15+444], to_fail=True,
                          error_type=TemperatureNotReachableError)

        # 44°C should be ok
        self.send_command(temperature_client.ControlTemperature, args=[273.15+44])

        # start_shaking unit 1
        self.send_command(shaker_client.StartShaking, 11.1, 1.1, args=[0])

        # assume first unit and only first unit is shaking
        self.send_command(shaker_client.Get_IsCurrentlyShaking)
        assert self.feedback == [True, False]

        # start both units
        self.send_command(shaker_client.StartShaking, 10, 0)
        # try unload... should not work because its shaking
        self.send_command(stacker_client.UnloadContainerFromStacker, 2, 0, args=[0, 0], to_fail=True,
                          error_type=DeviceStillBusyError)

        # stop both units
        self.send_command(shaker_client.StopShaking, 1.6, .6)

        # unload 0,0
        print(shaker_client.Get_IsCurrentlyShaking())
        self.send_command(stacker_client.UnloadContainerFromStacker, 6.5, .5, args=[0, 0])

        # stack occupancy should only be 1,4
        self.send_command(stacker_client.Get_Occupancy)
        assert self.feedback[1][4] and not self.feedback[0][0]

        # unload 1,4 ... should not work since transfer station occupied
        self.send_command(stacker_client.UnloadContainerFromStacker, 1, 1, args=[1, 4], to_fail=True,
                          error_type=TransferPositionOccupiedError)

        # assume temperature changed
        assert not temperature_client.current_temperature == temperature

        # clear the occupancy
        self.send_command(stacker_client.NullifyOccupancy, .4, .4)


if __name__ == '__main__':
    mytest = CytomatTest()
    mytest.runSimulation()
