.. ThermoCytomat2_SiLA_Server documentation master file, created by
   sphinx-quickstart on Tue Aug 31 18:25:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ThermoCytomat2 SiLA Server's documentation!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   usage
   api
   support

:doc:`mydoc`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
