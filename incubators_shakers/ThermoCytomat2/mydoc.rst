Using the test environment
==========================

You can run the SiLA server or a unittest by

>>> python cytomat_test.py [options]

.. option:: -m, --manual

    Using this option with prevent the script from executing the unittest.
    Instead, the server is just started and can be interacted with manually.

.. option:: -s, --show-mode

    Using this option will create a visualization of the cytomat and its actions
    using the *curses* module.

.. option:: -r, --real-time

    Using this option will cause the simulation to increase the time used to perform actions.
    This is meant to reflect the operating speed of a real cytomat.
