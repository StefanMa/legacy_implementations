Installation
============

To run the SiLA2-server with a real device, you just need to install SiLA2.
To run it with a simulated cytomat, you first need to install the simulation tool of sila_python

Installing SiLA2
----------------

1. Clone `this branch <https://gitlab.com/SiLA2/sila_python/-/tree/feature/greifswald_lara_devices_implementation>`_ of the sila\_python repository.
2. Go into the root directory containing ``sila2install.py``.
3. Follow the in installation instructions in ``README.md``.

Installing sila2_simulation
---------------------------

1. Go from the root directory of sila\_python to  ``sila_tools/sila2_simulation/``:
    >>> cd sila_tools/sila2_simulation
2. Install the python package. For example by
    >>> pip install .