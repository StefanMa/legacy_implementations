#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*ThermoCytomat2 client*

:details: ThermoCytomat2:
    SiLA2 service for Thermo Cytomat

:file:    ErrorRecoveryService_client.py
:authors: stefan maak, mark doerr

:date: (creation)          2021-03-12T10:57:54.545475
:date: (last modification) 2021-03-12T10:57:54.545475

.. note:: Code generated by sila2codegenerator 0.3.4

_______________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"

# import general packages
import logging
import argparse
import grpc
import time
from collections import namedtuple

# import meta packages
from typing import Union, Optional

# import SiLA2 library modules
from sila2lib.framework import SiLAFramework_pb2 as silaFW_pb2
from sila2lib.sila_client import SiLA2Client
from sila2lib.framework.std_features import SiLAService_pb2 as SiLAService_feature_pb2
from sila2lib.error_handling import client_err
#   Usually not needed, but - feel free to modify
# from sila2lib.framework.std_features import SimulationController_pb2 as SimController_feature_pb2

# import feature gRPC modules
# Import gRPC libraries of features
from ErrorRecoveryService.gRPC import ErrorRecoveryService_pb2
from ErrorRecoveryService.gRPC import ErrorRecoveryService_pb2_grpc
# import default arguments for this feature
from ErrorRecoveryService.ErrorRecoveryService_default_arguments import default_dict as ErrorRecoveryService_default_dict


RecoverableError = namedtuple('RecoverableError', ['cmd_id', 'uuid', 'default', 'message', 'options'])
ContinuationOption = namedtuple("ContinuationOption", ["option_id", "description", "required_data"])


# noinspection PyPep8Naming, PyUnusedLocal
class ErrorRecoveryServiceClient:
    """
        SiLA2 service for Thermo Cytomat

    .. note:: For an example on how to construct the parameter or read the response(s) for command calls and properties,
              compare the default dictionary that is stored in the directory of the corresponding feature.
    """
    # The following variables will be filled when run() is executed
    #: Storage for the connected servers version
    server_version: str = ''
    #: Storage for the display name of the connected server
    server_display_name: str = ''
    #: Storage for the description of the connected server
    server_description: str = ''

    def __init__(self, channel=None):
        """Class initialiser"""

        # Create stub objects used to communicate with the server
        self.ErrorRecoveryService_stub = \
            ErrorRecoveryService_pb2_grpc.ErrorRecoveryServiceStub(channel)

        # initialise class variables for server information storage
        self.server_version = ''
        self.server_display_name = ''
        self.server_description = ''

    def ExecuteContinuationOption(self, CommandExecutionUUID: str = 'default string',
                                  ContinuationOption: str = 'default string',
                                  InputData: str = 'default string'
                                  ):  # -> (ErrorRecoveryService):
        """
        Wrapper to call the unobservable command ExecuteContinuationOption on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling ExecuteContinuationOption:")
        try:
            # resolve to default if no value given
            if parameter is None:
                parameter = ErrorRecoveryService_pb2.ExecuteContinuationOption_Parameters(
                    CommandExecutionUUID=silaFW_pb2.String(value=CommandExecutionUUID),
                    ContinuationOption=silaFW_pb2.String(value=ContinuationOption),
                    InputData=silaFW_pb2.String(value=InputData)
                )
    
            response = self.ErrorRecoveryService_stub.ExecuteContinuationOption(parameter, metadata)
            logging.debug(f"ExecuteContinuationOption response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return

    def SetErrorHandlingTimeout(self, ErrorHandlingTimeout: int = 0
                                ):  # -> (ErrorRecoveryService):
        """
        Wrapper to call the unobservable command SetErrorHandlingTimeout on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling SetErrorHandlingTimeout:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = ErrorRecoveryService_pb2.SetErrorHandlingTimeout_Parameters(
                                ErrorHandlingTimeout=silaFW_pb2.Integer(value=ErrorHandlingTimeout)
                )
    
            response = self.ErrorRecoveryService_stub.SetErrorHandlingTimeout(parameter, metadata)
            logging.debug(f"SetErrorHandlingTimeout response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return

    def Subscribe_RecoverableErrors(self) \
            -> ErrorRecoveryService_pb2.Subscribe_RecoverableErrors_Responses:
        """Wrapper to get property RecoverableErrors from the server."""
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Reading observable property RecoverableErrors:")
        try:
            response = self.ErrorRecoveryService_stub.Subscribe_RecoverableErrors(
                ErrorRecoveryService_pb2.Subscribe_RecoverableErrors_Parameters()
            )
            logging.debug(
                'Subscribe_RecoverableErrors response: {response}'.format(
                    response=response
                )
            )
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return response

    @staticmethod
    def grpc_error_handling(error_object: grpc.Call) -> None:
        """Handles exceptions of type grpc.RpcError"""
        # pass to the default error handling
        grpc_error = client_err.grpc_error_handling(error_object=error_object)

        # Access more details using the return value fields
        logging.error(grpc_error.error_type)
        if hasattr(grpc_error.message, "parameter"):
            logging.error(grpc_error.message.parameter)
        logging.error(grpc_error.message.message)
