# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import ErrorRecoveryService_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()
default_dict['DataType_ContinuationOption'] = {
    'ContinuationOption': pb2.DataType_ContinuationOption.ContinuationOption_Struct(
        Identifier=silaFW_pb2.String(value='default string'),
        Description=silaFW_pb2.String(value='default string'),
        RequiredInputData=silaFW_pb2.String(value='default string'))
}

default_dict['DataType_RecoverableError'] = {
    'RecoverableError': pb2.DataType_RecoverableError.RecoverableError_Struct(
        CommandIdentifier=silaFW_pb2.String(value='default string'),
        CommandExecutionUUID=silaFW_pb2.String(value='default string'),
        ErrorMessage=silaFW_pb2.String(value='default string'),
        ContinuationOptions=[pb2.DataType_RecoverableError.RecoverableError_Struct.ContinuationOptions_Struct(
            Identifier=silaFW_pb2.String(value='default string'),
            Description=silaFW_pb2.String(value='default string'),
            RequiredInputData=silaFW_pb2.String(value='default string'))],
        DefaultOption=silaFW_pb2.String(value='default string'), AutomaticExecutionTimeout=silaFW_pb2.Integer(value=0))
}

default_dict['ExecuteContinuationOption_Parameters'] = {
    'CommandExecutionUUID': silaFW_pb2.String(value='default string'),
    'ContinuationOption': silaFW_pb2.String(value='default string'),
    'InputData': silaFW_pb2.String(value='default string')
}

default_dict['ExecuteContinuationOption_Responses'] = {
    
}

default_dict['SetErrorHandlingTimeout_Parameters'] = {
    'ErrorHandlingTimeout': silaFW_pb2.Integer(value=0)
}

default_dict['SetErrorHandlingTimeout_Responses'] = {
    
}

default_dict['Subscribe_RecoverableErrors_Responses'] = {
    'RecoverableErrors': [pb2.DataType_RecoverableError(**default_dict['DataType_RecoverableError'])]
}
