ThermoCytomat2 API
==================

Your friendly API :-)

The SiLA client
---------------

.. automodule:: ThermoCytomat2
.. automodule:: ThermoCytomat2.ThermoCytomat2_client
.. autoclass:: ThermoCytomat2Client

The test and simulation module
------------------------------

.. automodule:: ThermoCytomat2.cytomat_test
    :members:
.. autoclass:: ThermoCytomat2.cytomat_test.CytomatTest
.. autofunction:: ThermoCytomat2.cytomat_test.CytomatTest.test_process
