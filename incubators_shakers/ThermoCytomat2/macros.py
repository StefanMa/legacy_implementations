from time import sleep

def watch_loading(client, stack, pos):
	uuid = client.LoadContainerToStacker(stack, pos)
	info = client.LoadContainerToStacker_Info(uuid.commandExecutionUUID)
	print(info)
	while info.is_active():
		n= info.next()
		print(f"time_remaining:{n.estimatedRemainingTime.seconds}/{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}: {n.CommandStatus.Name(n.commandStatus)}")
		sleep(.1)

def watch_unloading(client, stack, pos):
	uuid = client.UnloadContainerFromStacker(stack, pos)
	info = client.UnloadContainerFromStacker_Info(uuid.commandExecutionUUID)
	while info.is_active():
		n= info.next()
		print(f"time_remaining:{n.estimatedRemainingTime.seconds}/{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}: {n.CommandStatus.Name(n.commandStatus)}")
		sleep(.1)

def watch_start(client, stack):
	uuid = client.StartShaking(stack)
	info = client.StartShaking_Info(uuid.commandExecutionUUID)
	while info.is_active():
		n= info.next()
		print(f"time_remaining:{n.estimatedRemainingTime.seconds}/{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}: {n.CommandStatus.Name(n.commandStatus)}")
		sleep(.1)

def watch_stop(client, stack):
	uuid = client.StopShaking(stack)
	info = client.StartShaking_Info(uuid.commandExecutionUUID)
	while info.is_active():
		n= info.next()
		print(f"time_remaining:{n.estimatedRemainingTime.seconds}/{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}: {n.CommandStatus.Name(n.commandStatus)}")
		sleep(.1)


def watch_oc_check(client):
	uuid = client.CheckOccupancy()
	info = client.CheckOccupancy_Info(uuid.commandExecutionUUID)
	while info.is_active():
		n= info.next()
		print(f"time_remaining:{n.estimatedRemainingTime.seconds}/{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}: {n.CommandStatus.Name(n.commandStatus)}")
		sleep(.1)


