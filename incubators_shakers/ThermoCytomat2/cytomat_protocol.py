from builtins import super

from sila2comlib.com.com_serial import ComSerial
from sila2comlib.com.simulated_serial import SimulatedSerial
from typing import Union
from abc import ABC
from enum import Enum
import os
import json
import logging
from threading import Thread
from time import sleep, time
import re
from copy import deepcopy
from simplejson import dumps


fail = 0
ok = 1
min_shaking_frequency = 50
max_shaking_frequency = 1200
# will be interpreted as hexadecimal numbers by the shaker
shaker_addresses = [20, 21]


class Status(Enum):
    COMMAND_IN_EXECUTION = 0
    LAST_COMMAND_SUCCESS = 1
    ERROR_OCCURRED = 3
    SHOVEL_OCCUPIED = 4
    PLATE_ON_TRANSFER = 7


def temperature_reachable(temperature):
    # cytomat can do 15-50 °C
    return int(temperature - 273.15) in range(15, 50)


class FunctionStatus(Enum):
    WAITING = 0,
    RUNNING = 1,
    SUCCESSFUL = 2,
    ERROR = 3


class FunctionInfo:
    def __init__(self, uuid, name, status=FunctionStatus.WAITING, args=[]):
        self.uuid = uuid
        self.name = name
        self.status = status
        self.start = time()
        self.duration = exp_duration[name]
        self.args = args


class CytomatProtocol(ABC):
    """
    A class for utility functions to enable easy communication with a Thermo-Fischer cytomat via the inherent protocol
    """
    def __init__(self, com: Union[ComSerial, SimulatedSerial]):
        self.com = com
        # the inner copy for the overview register
        self._status = {status: False for status in Status}
        self.last_update = 0
        self.status_thread = Thread(name='Status updater', target=self.frequent_update, daemon=True)
        self.stop = False  # a flag for loop threads to know to cancel
        self.status_thread.start()
        self.inner_state = InnerStatus(simulation_mode=isinstance(com, SimulatedSerial), mother=self)
        # a dictionary keeping all information on running and finished commands, that use the COMMAND_IN_EXECUTION bit
        self.function_info = dict(dummy_uuid=FunctionInfo('dummy_uuid', 'stop_shaking', FunctionStatus.SUCCESSFUL))
        self.current_uuid = "dummy_uuid"

    # refreshes status at least every 500 ms
    def frequent_update(self):
        sleep(.1)
        while not self.stop:
            try:
                if time() - self.last_update > .5:
                    self.update_status()
                    sleep(.1)
            except:
                pass

    def get_status(self, state: Status):
        self.update_status()
        return self._status[state]

    def function_running(self, uuid: str):
        return self.function_info[uuid].status in [FunctionStatus.WAITING, FunctionStatus.RUNNING]

    def finish(self, uuid: str, timeout=60):
        start = time()
        while True:
            status = self.function_info[uuid].status
            if status in [FunctionStatus.ERROR, FunctionStatus.SUCCESSFUL]:
                # wait for COMMAND_IN_EXECUTION bit to vanish
                while time() - start < timeout:
                    if not self.get_status(Status.COMMAND_IN_EXECUTION):
                        return status
                    sleep(.5)
                # return running if the COMMAND_IN_EXECUTION bit did not vanish
                return FunctionStatus.RUNNING
            # timed out ?
            if time() - start > timeout:
                return status
            # the outer loop does not contain device communication, so we do not need to sleep long
            sleep(.05)

    def update_status(self):
        if time() - self.last_update >= .05:
            cmd = "ch:bs\r"
            answer = self.com.get_answer(cmd, terminator="\r", max_wait=.5)
            if "er" not in answer:
                try:
                    code = re.search(r"(bs\s)(.+)", answer).group(2)
                    int_code = int(code, 16)
                    for status in Status:
                        self._status[status] = bool(int_code & 2 ** status.value)
                    if self._status[Status.LAST_COMMAND_SUCCESS]:
                        if self.current_uuid is not None:
                            self.function_info[self.current_uuid].status = FunctionStatus.SUCCESSFUL
                        else:
                            logging.error("There should be a uuid of the currently running command")
                    self.last_update = time()
                except:
                    print(f"status update went wrong. answer: {answer}")

    def get_frequency(self, stack):
        if stack in [0, 1]:
            cmd = f"ch:pb {shaker_addresses[stack]}\r"
            answer = self.com.get_answer(cmd, terminator="\r", max_wait=.5)
            success = fail if "er" in answer else ok
            if success == ok:
                frequency = int(answer.replace('pb ', ''))
                self.inner_state.frequency[stack] = frequency
                return success, frequency
            else:
                return success, None

    def get_temperature(self):
        cmd = "ch:it\r"
        answer = self.com.get_answer(cmd, terminator="\r", max_wait=.5)
        if 'er' in answer:
            return fail, None, None
        else:
            return [ok, *re.findall(r"[\d.]+", answer)]

    def set_frequency(self, stack: int, frequency: int):
        cmd = f"se:pb {shaker_addresses[stack]} {str(frequency).rjust(4,'0')}"
        answer = self.com.get_answer(cmd, terminator="\r", max_wait=.5)
        success = fail if "er" in answer else ok
        if success == ok:
            self.inner_state.frequency[stack] = frequency
        return success, answer

    def set_temperature(self, temperature):
        temperature = round(temperature - 273.15, 1)
        cmd = f"ll:it {temperature}\r"
        answer = self.com.get_answer(cmd, terminator="\r", max_wait=.5)
        success = fail if "er" in answer else ok
        return success, answer

    def init_shakers(self, uuid=None):
        cmd = "ll:vi\r"
        self.function_info[uuid] = FunctionInfo(uuid, 'init_shakers')
        answer = self.com.get_answer(cmd, terminator="\r", max_wait=2)
        success = fail if "er" in answer else ok
        if success == ok:
            self.function_info[uuid].status = FunctionStatus.RUNNING
            self.current_uuid = uuid
        else:
            self.function_info[uuid].status = FunctionStatus.ERROR
        return success, answer

    def start_shaking(self, stack=-1, uuid=None):
        if stack in [-1, 0, 1]:
            if stack == -1:
                cmd = "ll:va\r"
            else:
                cmd = f"ll:va 00{stack+1}\r"
            self.function_info[uuid] = FunctionInfo(uuid, 'start_shaking', args=[stack])
            answer = self.com.get_answer(cmd, terminator="\r", max_wait=2)
            success = fail if "er" in answer else ok
            # update the inner state
            if success == ok:
                self.function_info[uuid].status = FunctionStatus.RUNNING
                self.current_uuid = uuid
                if stack in [0, 1]:
                    self.inner_state.is_shaking[stack] = True
                else:
                    self.inner_state.is_shaking = [True, True]
            else:
                self.function_info[uuid].status = FunctionStatus.ERROR
            return success, answer
        else:
            return fail, None

    def stop_shaking(self, stack=-1, uuid=None):
        if stack in [-1, 0, 1]:
            if stack == -1:
                cmd = "ll:vd\r"
            else:
                cmd = f"ll:vd 00{stack+1}\r"
            self.function_info[uuid] = FunctionInfo(uuid, 'stop_shaking', args=[stack])
            answer = self.com.get_answer(cmd, terminator="\r", max_wait=2)
            success = fail if "er" in answer else ok
            # update the inner state
            if success == ok:
                if stack in [0, 1]:
                    self.inner_state.is_shaking[stack] = False
                else:
                    self.inner_state.is_shaking = [False, False]
                self.function_info[uuid].status = FunctionStatus.RUNNING
                self.current_uuid = uuid
            else:
                self.function_info[uuid].status = FunctionStatus.ERROR
            return success, answer
        else:
            return fail, None

    def move_in(self, stack, pos, uuid=None):
        intern_position = stack * 16 + pos + 1
        cmd = f"mv:ts {str(intern_position).rjust(3, '0')}\r"
        self.function_info[uuid] = FunctionInfo(uuid, 'move_in', args=[stack, pos])
        answer = self.com.get_answer(cmd, terminator="\r", max_wait=2)
        success = fail if "er" in answer else ok
        if success == ok:
            self.inner_state.occupied[stack][pos] = True
            self.function_info[uuid].status = FunctionStatus.RUNNING
            self.current_uuid = uuid
        else:
            self.function_info[uuid].status = FunctionStatus.ERROR
        return success, answer

    def move_out(self, stack, pos, uuid=None):
        intern_position = stack * 16 + pos + 1
        cmd = f"mv:st {str(intern_position).rjust(3, '0')}\r"
        self.function_info[uuid] = FunctionInfo(uuid, 'move_out', args=[stack, pos])
        answer = self.com.get_answer(cmd, terminator="\r", max_wait=2)
        success = fail if "er" in answer else ok
        if success == ok:
            self.inner_state.occupied[stack][pos] = False
            self.function_info[uuid].status = FunctionStatus.RUNNING
            self.current_uuid = uuid
        else:
            self.function_info[uuid].status = FunctionStatus.ERROR
        return success, answer

    def check_occupancy(self, uuid=None):
        # this will run while the check is ongoing and request its outcome once the check is done
        def observe_check_running(_self, _uuid):
            while self.function_info[_uuid].status in [FunctionStatus.RUNNING, FunctionStatus.WAITING]:
                sleep(.4)
            # do the checks for all positions
            occupancy = [[False for pos in range(16)] for stack in range(2)]
            for stack in range(2):
                for pos in range(16):
                    ch_answer = _self.com.get_answer(f"ch:sc {str(16 * stack + pos + 1).rjust(3, '0')}\r",
                                                     terminator="\r", max_wait=.5)
                    if "sc" in ch_answer:
                        b = re.search(r'[10\-\?]', ch_answer).group()
                        occupancy[stack][pos] = b in '1?'
                    else:
                        logging.error(f"Something went wrong while checking the occupancy entries. answer:{ch_answer}")
            self.inner_state.occupied = occupancy
        answer = self.com.get_answer("mv:sc\r", terminator="\r", max_wait=2)
        self.function_info[uuid] = FunctionInfo(uuid, 'check_occupancy')
        success = fail if "er" in answer else ok
        if success == ok:
            self.function_info[uuid].status = FunctionStatus.RUNNING
            self.current_uuid = uuid
            t = Thread(name='OcCheck observer', target=observe_check_running, args=[self, uuid], daemon=True)
            t.start()
        else:
            self.function_info[uuid].status = FunctionStatus.ERROR
        return success, answer


# TODO use python settings
# expected duration of all commands, that set the COMMAND_IN_EXECUTION bit
exp_duration = {
    CytomatProtocol.init_shakers.__name__: 6,
    CytomatProtocol.start_shaking.__name__: 16,
    CytomatProtocol.stop_shaking.__name__: 16,
    CytomatProtocol.move_in.__name__: 27,
    CytomatProtocol.move_out.__name__: 25,
    CytomatProtocol.check_occupancy.__name__: 11*60,
}


class InnerStatus:
    """
    In this class we handle the status data of the cytomat which is not saved by the device itself.
    This data is saved to json files, so the server can be restarted without loosing this data.
    """
    def __init__(self, simulation_mode=True, mother=None):
        self.filename = f"cytomat_inner_status_{'sim' if simulation_mode else 'real'}.json"
        self.mother = mother
        self._is_shaking = []
        self._occupied = []
        self._frequency = []
        if self.filename not in os.listdir():
            self.reset()
            self.save()
        else:
            self.load()
        self.auto_save_thread = Thread(target=self.auto_save, daemon=True)
        self.auto_save_thread.start()
        self.last_data = None
        self.runtime_backup()

    def copy_real_to_sim(self):
        self.save(self.filename.replace('real', 'sim'))

    def auto_save(self):
        while not self.mother.stop:
            sleep(.2)
            if self.changes_happened():
                self.save()

    def changes_happened(self):
        for dat, dat_backup in zip([self._occupied, self._is_shaking, self._frequency],
                                   [self.last_data[key] for key in ['occupied', 'is_shaking', 'frequency']]):
            if not dumps(dat) == dumps(dat_backup):
                return True
        return False

    def save(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, "w") as outstream:
            data = dict(
                occupied=self._occupied,
                is_shaking=self._is_shaking,
                frequency=self._frequency,
            )
            json.dump(data, outstream, indent=4)
        self.runtime_backup()

    def load(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, "r") as instream:
            data = json.load(instream)
            self._occupied = data['occupied']
            self._is_shaking = data['is_shaking']
            self._frequency = data['frequency']
        self.runtime_backup()

    def runtime_backup(self):
        self.last_data = dict(
            occupied=deepcopy(self._occupied),
            is_shaking=deepcopy(self._is_shaking),
            frequency=deepcopy(self._frequency),
        )

    def reset(self):
        self._occupied = [[False for pos in range(16)] for stack in range(2)]
        self._is_shaking = [False, False]
        self._frequency = [100, 100]

    @property
    def is_shaking(self):
        return self._is_shaking

    @is_shaking.setter
    def is_shaking(self, ar):
        self._is_shaking = ar

    @property
    def occupied(self):
        return self._occupied

    @occupied.setter
    def occupied(self, ar):
        self._occupied = ar

    @property
    def frequency(self):
        return self._frequency

    @frequency.setter
    def frequency(self, ar):
        self._frequency = ar


class CytomatProtocolReal(CytomatProtocol, ComSerial):
    def __init__(self):
        CytomatProtocol.__init__(self, self)
        ComSerial.__init__(self)





