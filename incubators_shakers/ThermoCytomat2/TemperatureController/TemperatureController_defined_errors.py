"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Temperature Controller_defined_errors*

:details: TemperatureController Defined SiLA Error factories:
    This is a simple example of a generic Feature for controlling and retrieving the temperature.
    A new target temperature can be set anytime with the 'Control Temperature' Command.
    The temperature range has been limited to prevent major damages of a device.
    In case the first target temperature has not been reached, a ControlInterrupted Error should be thrown.

:file:    TemperatureController_defined_errors.py
:authors: stefan maak, mark doerr

:date: (creation)          2021-07-01T21:05:11.975007
:date: (last modification) 2021-07-01T21:05:11.975007

.. note:: Code generated by sila2codegenerator 0.3.6

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.1"

# import general packages
from sila2lib.error_handling.server_err import SiLAExecutionError

# SiLA Defined Error factories

def generate_def_error_TemperatureNotReachable(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "TemperatureNotReachable"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""The ambient conditions prohibit the device from reaching the target temperature. \n{extra_message}"""
    return SiLAExecutionError(error_identifier="TemperatureNotReachable",
                         msg=msg)

def generate_def_error_ControlInterrupted(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "ControlInterrupted"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""The control of temperature could not be finished as it has been interrupted by another 'Control Temperature' command.
     \n{extra_message}"""
    return SiLAExecutionError(error_identifier="ControlInterrupted",
                         msg=msg)

