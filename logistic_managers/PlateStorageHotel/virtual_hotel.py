"""
This is meant so simulate the behavior of a ThermoFischer incubator shaker
"""

from time import sleep
import os
import json
import logging
from sila2_simulation.simulated_serial import SimulatedSerial
from hotelProtocol import HotelProtocol


num_stacks = 4
num_pos = 10


class VirtualHotel(SimulatedSerial):
    def __init__(self):
        self.front_stack = 2  # the stack that faces the transfer direction
        # We read in the real occupancy, but will never write in it
        self.occupancy = [[False for pos in range(num_pos)] for stack in range(num_stacks)]
        self.fast_mode = False
        self.is_running = {}

    def fit_occupancy(self, occupied):
        for stack in range(num_stacks):
            for pos in range(num_pos):
                self.occupancy = occupied[stack][pos]

    def do_enquiry(self, command):
        return ""

    def process_command(self, command):
        return ""

    # simulate the execution here
    def simulate_command(self, param, val):
        pass

    def get_answer(self, command, terminator='\r', encoding='utf-8', max_wait=2):
        return ""

    def turn_sim(self, front, uuid):
        if not front == self.front_stack:
            self.do_stuff(1, uuid)
            self.front_stack = front

    def add_sim(self, stack, pos):
        if self.occupancy[stack][pos]:
            self.fatal_crash("there was already a plate")
        self.occupancy[stack][pos] = True

    def remove_sim(self, stack, pos):
        if not self.occupancy[stack][pos]:
            logging.error("no container there")
        self.occupancy[stack][pos] = False

    def do_stuff(self, duration, uuid="buff"):
        self.is_running[uuid] = True
        if not self.fast_mode:
            sleep(duration)
        self.is_running[uuid] = False

    def fatal_crash(self, msg=''):
        logging.error(f"AAAAAHHHHHHHRGGGGGG!!!!!!  {msg}")

    def __str__(self):
        d = 10
        s = f"{'Hotel'.rjust(round(1.9*d), ' ')}\n"
        for i in range(num_stacks):
            s += f"stack{(i+self.front_stack)%num_stacks}".ljust(d, ' ')
        s += "\n\n"
        for j in range(num_pos):
            for i in range(num_stacks):
                oc = "FULL" if self.occupancy[(i+self.front_stack) % num_stacks][j] else "EMPTY"
                s += oc.ljust(d, ' ')
            s += "\n"
        return s[:-1]


class HotelProtocolSimulated(HotelProtocol, VirtualHotel):
    def __init__(self):
        HotelProtocol.__init__(self, self, "dummy_wrapper.dll")
        VirtualHotel.__init__(self)

