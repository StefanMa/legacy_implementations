"""
A unittest for the cytomat client-server pair. It uses the virtual_cytomat to emulate a real one.
The test can be run in fast-mode for fast testing or in real mode for realistic reaction/working times of the device.
"""
from time import sleep
import unittest
from sila2_simulation.deviceTest import DeviceTest
from sila2lib.error_handling.server_err import SiLAExecutionError
from PlateStorageHotel_client import PlateStorageHotelClient
from PlateStorageHotel_server import PlateStorageHotelServer


class HotelTest(DeviceTest, unittest.TestCase):
    def is_error(self, feedback) -> bool:
        return issubclass(type(feedback), SiLAExecutionError)

    def setUp(self, port=50051) -> None:
        super().setUp()
        self.server = PlateStorageHotelServer(cmd_args=self.create_args(port=port), simulation_mode=True, block=False)
        # give the server some time to register with zeroconfig
        sleep(.5)
        self.client = PlateStorageHotelClient()

    def test_process(self):
        client = self.client


if __name__ == '__main__':
    mytest = HotelTest()
    mytest.runSimulation()

