# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import RobotInteractionService_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()


default_dict['PrepareToGiveContainerFromSite_Parameters'] = {
    'Site': silaFW_pb2.Integer(value=1)
}

default_dict['PrepareToGiveContainerFromSite_Responses'] = {
    'Ready': silaFW_pb2.Boolean(value=False)
}

default_dict['PrepareToReceiveContainerOnSite_Parameters'] = {
    'Site': silaFW_pb2.Integer(value=1)
}

default_dict['PrepareToReceiveContainerOnSite_Responses'] = {
    'Ready': silaFW_pb2.Boolean(value=False)
}

default_dict['ReceivedContainer_Parameters'] = {
    'Site': silaFW_pb2.Integer(value=1)
}

default_dict['ReceivedContainer_Responses'] = {
    
}

default_dict['ContainerGotTaken_Parameters'] = {
    'Site': silaFW_pb2.Integer(value=1)
}

default_dict['ContainerGotTaken_Responses'] = {
    
}

default_dict['TransportCancelled_Parameters'] = {
    'Site': silaFW_pb2.Integer(value=1)
}

default_dict['TransportCancelled_Responses'] = {
    
}

default_dict['Get_CanReceiveContainer_Responses'] = {
    'CanReceiveContainer': [silaFW_pb2.Boolean(value=False)]
}

default_dict['Get_CanGiveContainer_Responses'] = {
    'CanGiveContainer': [silaFW_pb2.Boolean(value=False)]
}
