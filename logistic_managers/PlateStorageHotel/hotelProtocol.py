from sila2comlib.com.com_serial import ComSerial
from sila2comlib.com.simulated_serial import SimulatedSerial
from typing import Union
from abc import ABC
from time import time, sleep
import os
from threading import Thread
from enum import Enum
import json
from copy import deepcopy
from simplejson import dumps


SIMULATE_WINDOOF = False


def construct_wrapper(dll_file, simulation):
    if SIMULATE_WINDOOF or not simulation:
        import clr
        import System
        try:
            p = os.path.dirname(__file__)
            for d in ["..", "HelpingHand", "cs_wrapper", dll_file]:
                p = os.path.join(p, d)
            dll = clr.AddReference(p)
            s = None
            for i in dll.DefinedTypes:
                s = str(i)
            cl = dll.GetType(s)
            wrapper = System.Activator.CreateInstance(cl)
            wrapper.DummyFunction()
            return wrapper
        except:
            print("Something went wrong constructing the C# wrapper :-(")
            return None
    else:
        class Wrapper:
            def Init(self, name): return "dummy_c_uuid"
            def HomeAll(self): return "dummy_c_uuid"
            def SmartMoveTo(self, position, offset): return "dummy_c_uuid"
            def SmartMoveSafe(self): return "dummy_c_uuid"

        return Wrapper()


class FunctionStatus(Enum):
    WAITING = 0
    RUNNING = 1
    SUCCESSFUL = 2
    ERROR = 3
    CANCELLED = 4


class FunctionInfo:
    def __init__(self, uuid, c_uuid, name, status=FunctionStatus.WAITING, args=[]):
        self.uuid = uuid
        self.c_uuid = c_uuid
        self.name = name
        self.status = status
        self.start = time()
        self.duration = 5
        self.args = args


num_columns = 8
slots_per_column = 25  # some slots are skipped to make room for bigger containers
# convert the 2-dim representation into 1-dim


def pos(stack, _pos):
    return stack*slots_per_column + _pos


# convert the 1-dim representation into 2-dim
def stack_pos(_pos):
    return _pos // slots_per_column, _pos % slots_per_column


class HotelProtocol(ABC):
    """
    A class for utility functions to enable easy communication with a PlateStorageHotel via the inherent protocol
    """
    def __init__(self, com: Union[ComSerial, SimulatedSerial], wrapper_dll: str):
        self.com = com
        # a dictionary keeping all information on running and finished commands, that use the COMMAND_IN_EXECUTION bit
        self.current_uuid = None
        self.function_info = {}
        self.is_simulation = not isinstance(self, HotelProtocolReal)
        self.wrapper = construct_wrapper(wrapper_dll, self.is_simulation)
        self.wrapper.Init("Carousel")
        self.stop = False
        self.inner_state = InnerHotelStatus(self.is_simulation, mother=self)
        # in initialization the motor is homed, so we always start with column 0 in the front
        self.current_pos = 0
        if self.is_simulation:
            self.com.fit_occupancy(self.inner_state.occupied)

    def add(self, stack, _pos):
        if self.is_simulation:
            self.add_sim(stack, _pos)
        self.inner_state.occupied[stack][_pos] = True

    def remove(self, stack, _pos):
        if self.is_simulation:
            self.remove_sim(stack, _pos)
        self.inner_state.occupied[stack][_pos] = False

    def is_busy(self):
        if self.current_uuid is None:
            return False
        if self.is_simulation:
            return self.is_running[self.current_uuid]
        else:
            status = self.get_status(self.current_uuid)
            return status in [FunctionStatus.WAITING, FunctionStatus.RUNNING]

    # do not change function name. It gets overwritten in simulated arm
    def function_running(self, uuid: str):
        if self.is_simulation:
            return self.is_running[uuid]
        else:
            return self.get_status(uuid) in [FunctionStatus.WAITING, FunctionStatus.RUNNING]

    def get_status(self, uuid: str):
        if self.is_simulation:
            raise NotImplementedError("This function should not be called in simulation_mode")
        # get the corresponding uuid of the command in c_sharp
        c_uuid = self.function_info[uuid].c_uuid
        state = self.wrapper.get_status(c_uuid)
        # convert the int coming from the enum in c_sharp back to enum
        return FunctionStatus(state)

    def finish(self, uuid, timeout=20):
        start = time()
        while self.function_running(uuid):
            sleep(.2)
            if time() - start > timeout:
                print(f"function with uuid {uuid} timed out")
                return False
        if self.is_simulation:
            return True
        return self.get_status(uuid) == FunctionStatus.SUCCESSFUL

    def home_all(self, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.HomeAll()
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'home_all', FunctionStatus.WAITING, args=[])

    def turn(self, col: int, uuid: str):
        self.current_uuid = uuid
        if self.is_simulation:
            self.turn_sim(col, uuid)
        position = f"STDloc:column[{col+1}]"
        c_uuid = self.wrapper.SmartMoveTo(position, [0,0,0])
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'move_to', FunctionStatus.WAITING, args=[position])

    def smart_move_safe(self, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.SmartMoveSafe()
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'smart_move_safe', FunctionStatus.WAITING, args=[])


class InnerHotelStatus:
    """
    In this class we handle the status data of the cytomat which is not saved by the device itself.
    This data is saved to json files, so the server can be restarted without loosing this data.
    """
    def __init__(self, simulation_mode=True, mother=None):
        self.filename = f"hotel_inner_status_{'sim' if simulation_mode else 'real'}.json"
        self.mother = mother
        self._occupied = []
        if self.filename not in os.listdir():
            self.reset()
            self.save()
        else:
            self.load()
        self.auto_save_thread = Thread(target=self.auto_save, daemon=True)
        self.auto_save_thread.start()
        self.last_data = None
        self.runtime_backup()

    def copy_real_to_sim(self):
        self.save(self.filename.replace('real', 'sim'))

    def auto_save(self):
        while not self.mother.stop:
            sleep(.2)
            if self.changes_happened():
                self.save()

    def changes_happened(self):
        # this is formulated a bit complicated, to match the formulation for cytomat... will be moved to general class
        for dat, dat_backup in zip([self._occupied],
                                   [self.last_data[key] for key in ['occupied']]):
            if not dumps(dat) == dumps(dat_backup):
                return True
        return False

    def save(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, "w") as outstream:
            data = dict(
                occupied=self._occupied,
            )
            json.dump(data, outstream, indent=4)
        self.runtime_backup()

    def load(self, filename=None):
        if filename is None:
            filename = self.filename
        with open(filename, "r") as instream:
            data = json.load(instream)
            self._occupied = data['occupied']
        self.runtime_backup()

    def runtime_backup(self):
        self.last_data = dict(
            occupied=deepcopy(self._occupied),
        )

    def reset(self):
        self._occupied = [[False for pos in range(slots_per_column)] for stack in range(num_columns)]

    @property
    def occupied(self):
        return self._occupied

    @occupied.setter
    def occupied(self, ar):
        self._occupied = ar


class HotelProtocolReal(HotelProtocol, ComSerial):
    def __init__(self):
        HotelProtocol.__init__(self, self, "real_wrapper.dll")
        ComSerial.__init__(self)




