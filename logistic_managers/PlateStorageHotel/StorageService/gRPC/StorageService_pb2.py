# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: StorageService.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='StorageService.proto',
  package='sila2.de.tuberlin.bioprocess.storing.storageservice.v1',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x14StorageService.proto\x12\x36sila2.de.tuberlin.bioprocess.storing.storageservice.v1\x1a\x13SiLAFramework.proto\"_\n GetLabwareInformation_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"W\n\x1fGetLabwareInformation_Responses\x12\x34\n\x0bLabwareType\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"_\n SetLabwareInformation_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"W\n\x1fSetLabwareInformation_Responses\x12\x34\n\x0bLabwareType\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"Y\n\x1aGetFromPosition_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"V\n\x19GetFromPosition_Responses\x12\x39\n\x10HandoverPosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"W\n\x18PutToPosition_Parameters\x12;\n\x12\x41ttainablePosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"T\n\x17PutToPosition_Responses\x12\x39\n\x10HandoverPosition\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1c\n\x1aGet_TowerNumber_Parameters\"Q\n\x19Get_TowerNumber_Responses\x12\x34\n\x0bTowerNumber\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1f\n\x1dGet_HandoverNumber_Parameters\"W\n\x1cGet_HandoverNumber_Responses\x12\x37\n\x0eHandoverNumber\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"$\n\"Get_AttainablePositions_Parameters\"a\n!Get_AttainablePositions_Responses\x12<\n\x13\x41ttainablePositions\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer2\xfa\n\n\x0eStorageService\x12\xcc\x01\n\x15GetLabwareInformation\x12X.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Parameters\x1aW.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Responses\"\x00\x12\xcc\x01\n\x15SetLabwareInformation\x12X.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Parameters\x1aW.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Responses\"\x00\x12\xba\x01\n\x0fGetFromPosition\x12R.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Parameters\x1aQ.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Responses\"\x00\x12\xb4\x01\n\rPutToPosition\x12P.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Parameters\x1aO.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Responses\"\x00\x12\xba\x01\n\x0fGet_TowerNumber\x12R.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Parameters\x1aQ.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Responses\"\x00\x12\xc3\x01\n\x12Get_HandoverNumber\x12U.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Parameters\x1aT.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Responses\"\x00\x12\xd2\x01\n\x17Get_AttainablePositions\x12Z.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Parameters\x1aY.sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Responses\"\x00\x62\x06proto3'
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_GETLABWAREINFORMATION_PARAMETERS = _descriptor.Descriptor(
  name='GetLabwareInformation_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=101,
  serialized_end=196,
)


_GETLABWAREINFORMATION_RESPONSES = _descriptor.Descriptor(
  name='GetLabwareInformation_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='LabwareType', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Responses.LabwareType', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=198,
  serialized_end=285,
)


_SETLABWAREINFORMATION_PARAMETERS = _descriptor.Descriptor(
  name='SetLabwareInformation_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=287,
  serialized_end=382,
)


_SETLABWAREINFORMATION_RESPONSES = _descriptor.Descriptor(
  name='SetLabwareInformation_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='LabwareType', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Responses.LabwareType', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=384,
  serialized_end=471,
)


_GETFROMPOSITION_PARAMETERS = _descriptor.Descriptor(
  name='GetFromPosition_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=473,
  serialized_end=562,
)


_GETFROMPOSITION_RESPONSES = _descriptor.Descriptor(
  name='GetFromPosition_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='HandoverPosition', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Responses.HandoverPosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=564,
  serialized_end=650,
)


_PUTTOPOSITION_PARAMETERS = _descriptor.Descriptor(
  name='PutToPosition_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePosition', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Parameters.AttainablePosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=652,
  serialized_end=739,
)


_PUTTOPOSITION_RESPONSES = _descriptor.Descriptor(
  name='PutToPosition_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='HandoverPosition', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Responses.HandoverPosition', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=741,
  serialized_end=825,
)


_GET_TOWERNUMBER_PARAMETERS = _descriptor.Descriptor(
  name='Get_TowerNumber_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=827,
  serialized_end=855,
)


_GET_TOWERNUMBER_RESPONSES = _descriptor.Descriptor(
  name='Get_TowerNumber_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='TowerNumber', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Responses.TowerNumber', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=857,
  serialized_end=938,
)


_GET_HANDOVERNUMBER_PARAMETERS = _descriptor.Descriptor(
  name='Get_HandoverNumber_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=940,
  serialized_end=971,
)


_GET_HANDOVERNUMBER_RESPONSES = _descriptor.Descriptor(
  name='Get_HandoverNumber_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='HandoverNumber', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Responses.HandoverNumber', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=973,
  serialized_end=1060,
)


_GET_ATTAINABLEPOSITIONS_PARAMETERS = _descriptor.Descriptor(
  name='Get_AttainablePositions_Parameters',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1062,
  serialized_end=1098,
)


_GET_ATTAINABLEPOSITIONS_RESPONSES = _descriptor.Descriptor(
  name='Get_AttainablePositions_Responses',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='AttainablePositions', full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Responses.AttainablePositions', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1100,
  serialized_end=1197,
)

_GETLABWAREINFORMATION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_GETLABWAREINFORMATION_RESPONSES.fields_by_name['LabwareType'].message_type = SiLAFramework__pb2._INTEGER
_SETLABWAREINFORMATION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_SETLABWAREINFORMATION_RESPONSES.fields_by_name['LabwareType'].message_type = SiLAFramework__pb2._INTEGER
_GETFROMPOSITION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_GETFROMPOSITION_RESPONSES.fields_by_name['HandoverPosition'].message_type = SiLAFramework__pb2._INTEGER
_PUTTOPOSITION_PARAMETERS.fields_by_name['AttainablePosition'].message_type = SiLAFramework__pb2._INTEGER
_PUTTOPOSITION_RESPONSES.fields_by_name['HandoverPosition'].message_type = SiLAFramework__pb2._INTEGER
_GET_TOWERNUMBER_RESPONSES.fields_by_name['TowerNumber'].message_type = SiLAFramework__pb2._INTEGER
_GET_HANDOVERNUMBER_RESPONSES.fields_by_name['HandoverNumber'].message_type = SiLAFramework__pb2._INTEGER
_GET_ATTAINABLEPOSITIONS_RESPONSES.fields_by_name['AttainablePositions'].message_type = SiLAFramework__pb2._INTEGER
DESCRIPTOR.message_types_by_name['GetLabwareInformation_Parameters'] = _GETLABWAREINFORMATION_PARAMETERS
DESCRIPTOR.message_types_by_name['GetLabwareInformation_Responses'] = _GETLABWAREINFORMATION_RESPONSES
DESCRIPTOR.message_types_by_name['SetLabwareInformation_Parameters'] = _SETLABWAREINFORMATION_PARAMETERS
DESCRIPTOR.message_types_by_name['SetLabwareInformation_Responses'] = _SETLABWAREINFORMATION_RESPONSES
DESCRIPTOR.message_types_by_name['GetFromPosition_Parameters'] = _GETFROMPOSITION_PARAMETERS
DESCRIPTOR.message_types_by_name['GetFromPosition_Responses'] = _GETFROMPOSITION_RESPONSES
DESCRIPTOR.message_types_by_name['PutToPosition_Parameters'] = _PUTTOPOSITION_PARAMETERS
DESCRIPTOR.message_types_by_name['PutToPosition_Responses'] = _PUTTOPOSITION_RESPONSES
DESCRIPTOR.message_types_by_name['Get_TowerNumber_Parameters'] = _GET_TOWERNUMBER_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_TowerNumber_Responses'] = _GET_TOWERNUMBER_RESPONSES
DESCRIPTOR.message_types_by_name['Get_HandoverNumber_Parameters'] = _GET_HANDOVERNUMBER_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_HandoverNumber_Responses'] = _GET_HANDOVERNUMBER_RESPONSES
DESCRIPTOR.message_types_by_name['Get_AttainablePositions_Parameters'] = _GET_ATTAINABLEPOSITIONS_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_AttainablePositions_Responses'] = _GET_ATTAINABLEPOSITIONS_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetLabwareInformation_Parameters = _reflection.GeneratedProtocolMessageType('GetLabwareInformation_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETLABWAREINFORMATION_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Parameters)
  })
_sym_db.RegisterMessage(GetLabwareInformation_Parameters)

GetLabwareInformation_Responses = _reflection.GeneratedProtocolMessageType('GetLabwareInformation_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETLABWAREINFORMATION_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetLabwareInformation_Responses)
  })
_sym_db.RegisterMessage(GetLabwareInformation_Responses)

SetLabwareInformation_Parameters = _reflection.GeneratedProtocolMessageType('SetLabwareInformation_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETLABWAREINFORMATION_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Parameters)
  })
_sym_db.RegisterMessage(SetLabwareInformation_Parameters)

SetLabwareInformation_Responses = _reflection.GeneratedProtocolMessageType('SetLabwareInformation_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETLABWAREINFORMATION_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.SetLabwareInformation_Responses)
  })
_sym_db.RegisterMessage(SetLabwareInformation_Responses)

GetFromPosition_Parameters = _reflection.GeneratedProtocolMessageType('GetFromPosition_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETFROMPOSITION_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Parameters)
  })
_sym_db.RegisterMessage(GetFromPosition_Parameters)

GetFromPosition_Responses = _reflection.GeneratedProtocolMessageType('GetFromPosition_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETFROMPOSITION_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.GetFromPosition_Responses)
  })
_sym_db.RegisterMessage(GetFromPosition_Responses)

PutToPosition_Parameters = _reflection.GeneratedProtocolMessageType('PutToPosition_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _PUTTOPOSITION_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Parameters)
  })
_sym_db.RegisterMessage(PutToPosition_Parameters)

PutToPosition_Responses = _reflection.GeneratedProtocolMessageType('PutToPosition_Responses', (_message.Message,), {
  'DESCRIPTOR' : _PUTTOPOSITION_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.PutToPosition_Responses)
  })
_sym_db.RegisterMessage(PutToPosition_Responses)

Get_TowerNumber_Parameters = _reflection.GeneratedProtocolMessageType('Get_TowerNumber_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_TOWERNUMBER_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Parameters)
  })
_sym_db.RegisterMessage(Get_TowerNumber_Parameters)

Get_TowerNumber_Responses = _reflection.GeneratedProtocolMessageType('Get_TowerNumber_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_TOWERNUMBER_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_TowerNumber_Responses)
  })
_sym_db.RegisterMessage(Get_TowerNumber_Responses)

Get_HandoverNumber_Parameters = _reflection.GeneratedProtocolMessageType('Get_HandoverNumber_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_HANDOVERNUMBER_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Parameters)
  })
_sym_db.RegisterMessage(Get_HandoverNumber_Parameters)

Get_HandoverNumber_Responses = _reflection.GeneratedProtocolMessageType('Get_HandoverNumber_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_HANDOVERNUMBER_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_HandoverNumber_Responses)
  })
_sym_db.RegisterMessage(Get_HandoverNumber_Responses)

Get_AttainablePositions_Parameters = _reflection.GeneratedProtocolMessageType('Get_AttainablePositions_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_ATTAINABLEPOSITIONS_PARAMETERS,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Parameters)
  })
_sym_db.RegisterMessage(Get_AttainablePositions_Parameters)

Get_AttainablePositions_Responses = _reflection.GeneratedProtocolMessageType('Get_AttainablePositions_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_ATTAINABLEPOSITIONS_RESPONSES,
  '__module__' : 'StorageService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.de.tuberlin.bioprocess.storing.storageservice.v1.Get_AttainablePositions_Responses)
  })
_sym_db.RegisterMessage(Get_AttainablePositions_Responses)



_STORAGESERVICE = _descriptor.ServiceDescriptor(
  name='StorageService',
  full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=1200,
  serialized_end=2602,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetLabwareInformation',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.GetLabwareInformation',
    index=0,
    containing_service=None,
    input_type=_GETLABWAREINFORMATION_PARAMETERS,
    output_type=_GETLABWAREINFORMATION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='SetLabwareInformation',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.SetLabwareInformation',
    index=1,
    containing_service=None,
    input_type=_SETLABWAREINFORMATION_PARAMETERS,
    output_type=_SETLABWAREINFORMATION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetFromPosition',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.GetFromPosition',
    index=2,
    containing_service=None,
    input_type=_GETFROMPOSITION_PARAMETERS,
    output_type=_GETFROMPOSITION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='PutToPosition',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.PutToPosition',
    index=3,
    containing_service=None,
    input_type=_PUTTOPOSITION_PARAMETERS,
    output_type=_PUTTOPOSITION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Get_TowerNumber',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.Get_TowerNumber',
    index=4,
    containing_service=None,
    input_type=_GET_TOWERNUMBER_PARAMETERS,
    output_type=_GET_TOWERNUMBER_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Get_HandoverNumber',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.Get_HandoverNumber',
    index=5,
    containing_service=None,
    input_type=_GET_HANDOVERNUMBER_PARAMETERS,
    output_type=_GET_HANDOVERNUMBER_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Get_AttainablePositions',
    full_name='sila2.de.tuberlin.bioprocess.storing.storageservice.v1.StorageService.Get_AttainablePositions',
    index=6,
    containing_service=None,
    input_type=_GET_ATTAINABLEPOSITIONS_PARAMETERS,
    output_type=_GET_ATTAINABLEPOSITIONS_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_STORAGESERVICE)

DESCRIPTOR.services_by_name['StorageService'] = _STORAGESERVICE

# @@protoc_insertion_point(module_scope)
