 - generate random strings in c#   DONE
 - dictionaries in c#   DONE
 - pythonseite vom wrapper vervollständigen   DONE
 - status-abfrage für laufenden command    DONE
 - infrastruktur für observable commands vom cytomaten übernehmen DONE
 - parsing der locations machen (auf python-seite) DONE
 - position-struktur in python bauen: [home, devices][safe, nests] DONE
 - commands vom sila-treiber observable machen 
 - gripcheck so bauen, dass es eine antwort statt einen error wirft (hoffentlich geht das ohne in einen ErrorState zu verfallen) DONE
 - mark fragen: command-queue oder DeviceStillBusyError? Ich wäre für letzteres, weil sonst deadlocks wahrscheinlicher werden bzw. der scheduler de kontrolle verliert
 - gibt es pause/continue/cancel im MoverFramework? Falls ja: CancelController/PauseController implementieren? :-( :-(
 - namensgebung für device, site, position, know_devices, usw. ueberdenken und anpassen DONE
 - wrapper vervollständigen, wenn alles läuft
 - make finish(timeout)->bool function für harwareinterface

communication_ideas: 
	- geraete sollten in access_clients korrekten namen haben(wie in *****.loc) DONE
	- bei pick, place, move wird das RobotInteractionFeature angesprochen, falls eines hinterlegt ist, ansonsten wirds einfach gemacht DONE
		-> was passiert bei (device=cytomat, site=10) ? vielleicht manuell alle auf die transferstation mappen DONE
		-> naming beim cytomat ist generell mist: trnasfer heist "nest" statt "nest[1]" DONE
    - mit MoveToPosition, GripOpen, GripClose kann der arm ohne RobotInteractionFeature genutzt werden DONE


todo:
	- remove printouts by arm when waiting for response by device whether its ready for transport DONE
	- hotel.prepare seems to lack a sleep(.2) DONE
	- indexing has a mistake: canGive(2) greift vermutlich auf occupied[2] zu DONE
		-> change everything to start counting at 0. change that only for position-name-parsing. SAFE will be -1 (flag in armProtocol) DONE
		-> cytomat starts numbering its slots at 0. transfer can then be accessed with 32 (user needs to know, but no essential feature) DONE
	- reading von cytomat_output irgendwie zerstückelt MAYBE_DONE
	- clone plateReaderTreiber und anschauen WHERE????
	- pick/place should use proper naming/indexing  DONE



ps:
There is a constant battle between programmers creating more idiot-proof pograms and the world creating bigger idiots... so far the world seems to be winning
