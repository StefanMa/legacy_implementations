from sila2comlib.com.com_serial import ComSerial
from sila2comlib.com.simulated_serial import SimulatedSerial
from typing import Union
from abc import ABC
from time import time, sleep
import os
import re
from threading import Thread
from enum import Enum
from lxml import objectify

SIMULATE_WINDOOF = False


if SIMULATE_WINDOOF:
    import clr
    import System

    def construct_wrapper(dll_file):
        try:
            p = os.path.join(os.path.join(os.path.dirname(__file__), 'cs_wrapper'), dll_file)
            dll = clr.AddReference(p)
            s = None
            for i in dll.DefinedTypes:
                s = str(i)
            cl = dll.GetType(s)
            wrapper = System.Activator.CreateInstance(cl)
            wrapper.DummyFunction()
            return wrapper
        except:
            print("Something went wrong constructing the C# wrapper :-(")
            return None
else:
    class Wrapper:
        def Init(self, name): pass
        def HomeAll(self): pass
        def SmartMoveTo(self, position, offset): pass
        def SmartMoveSafe(self): pass
        def GripOpen(self): pass
        def GripClose(self): pass
        def GripCheck(self, expectancy): pass
        def GripTo(self, distance): pass
        def Get(self, position, offset, force): pass
        def Put(self, position, offset, force): pass
        def Delay(self, milliseconds): pass
        def MoveCartesian(self, dx, dy, dz): pass
        def MoveStraightTo(self, position): pass

    def construct_wrapper(dll_file):
        return Wrapper()


# code the safe position of devices. The sites are numbered starting from 0
SAFE = -1


def format_site(device, site, safe_nest=False):
    position = f"{device}:"
    if site == SAFE:
        position += "safe"
    else:
        # this is not especially clean, but the easiest way to match the naming of the cytomat nests
        # to the naming of the rotanta nests
        if "Cytomat" in device or "Varioskan" in device:
            position += "nest"
        else:
            # 25 slots per column
            if "Carousel" in device:
                site = site % 25
            position += f"nest[{site+1}]"
        if safe_nest:
            position += ".safe"
    return position


def matches_loc_file(file: str):
    return re.search(r'\.loc', file) is not None


def parse_locations(file: str):
    root = objectify.parse(file).getroot()
    loc_names = set({})
    try:
        for loc in root.locations.location:
            loc_names.add(loc.get('name'))
        for i in range(root.interpolations.countchildren()):
            pattern = root.interpolations.interpolate[i].get('pattern')
            idx = pattern.index('#')
            l = pattern[:idx]
            r = pattern[idx+1:]
            matching = []
            for pos in loc_names:
                rest = pos.replace(l, '').replace(r, '')
                if rest.isnumeric():
                    matching.append(int(rest))
            mini = min(matching)
            maxi = max(matching)
            for mid in range(mini+1, maxi):
                loc_names.add(f"{l}{mid}{r}")
    except:
        pass
    return loc_names


def parse_positions(directory: str = None):
    # dictionary device -> list of nests (0 is safe pos)
    nests = {}
    path = directory
    if path is None:
        path = os.path.dirname(__file__)
        for d in ["..", "ThermoMovers", "F5"]:
            path = os.path.join(path, d)
    for file in os.listdir(path):
        if matches_loc_file(file):
            device = file.replace('.loc', '')
            nests[device] = parse_locations(os.path.join(path, file))
    return nests


class FunctionStatus(Enum):
    WAITING = 0
    RUNNING = 1
    SUCCESSFUL = 2
    ERROR = 3
    CANCELLED = 4


class FunctionInfo:
    def __init__(self, uuid, c_uuid, name, status=FunctionStatus.WAITING, args=[]):
        self.uuid = uuid
        self.c_uuid = c_uuid
        self.name = name
        self.status = status
        self.start = time()
        self.duration = exp_duration[name]
        self.args = args


class ArmProtocol(ABC):
    """
    A class for utility functions to enable easy communication with a HettichRotantaR460 via the inherent protocol
    """
    def __init__(self, com: Union[ComSerial, SimulatedSerial], wrapper_dll: str):
        self.com = com
        self.access_clients = {}
        self.wrapper = construct_wrapper(wrapper_dll)
        self.wrapper.Init("F5")
        # the robot does not open the gripper by itself before trying to grab a plate
        self.wrapper.GripOpen()
        # a dictionary keeping all information on running and finished commands, that use the COMMAND_IN_EXECUTION bit
        self.current_uuid = None
        self.function_info = {}
        self.is_simulation = not isinstance(self, ArmProtocolReal)
        self.occupied = False

    def is_busy(self):
        if self.current_uuid is None:
            return False
        if self.is_simulation:
            return self.is_running[self.current_uuid]
        else:
            status = self.get_status(self.current_uuid)
            return status in [FunctionStatus.WAITING, FunctionStatus.RUNNING]

    # do not change function name. It gets overwritten in simulated arm
    def function_running(self, uuid: str):
        if self.is_simulation:
            if uuid in self.is_running:
                return self.is_running[uuid]
            else:
                # not very clean. simple commands are simply done instantly
                return False
        else:
            return self.get_status(uuid) in [FunctionStatus.WAITING, FunctionStatus.RUNNING]

    def get_status(self, uuid: str):
        # get the corresponding uuid of the command in c_sharp
        c_uuid = self.function_info[uuid].c_uuid
        state = self.wrapper.get_status(c_uuid)
        # convert the int coming from the enum in c_sharp back to enum
        return FunctionStatus(state)

    def get_result(self, uuid: str):
        if self.is_simulation:
            return None
        result = self.wrapper.get_result(uuid)
        return result

    def register_access_client(self, device_name, client):
        self.access_clients[device_name] = client

    def ask_to_place(self, device, site):
        if device in self.access_clients.keys():
            return self.access_clients[device].Get_CanReceiveContainer()[site]
        else:
            print("given device has no registered RobotInteractionService")
            return False

    def prepare_to_place(self, device, site):
        if device in self.access_clients.keys():
            uuid = self.access_clients[device].PrepareToReceiveContainerOnSite(site)
            t = time()
            timeout = 60
            while time() - t < timeout:
                sleep(.2)
                ready = self.access_clients[device].PrepareToReceiveContainerOnSite_Result(uuid.commandExecutionUUID)
                if ready is not None:
                    return ready
        else:
            print("given device has no registered RobotInteractionService")
            return False

    def placed(self, device, site):
        if device in self.access_clients.keys():
            self.access_clients[device].ReceivedContainer(site)
        else:
            print("given device has no registered RobotInteractionService")

    def ask_to_take(self, device, site):
        if device in self.access_clients.keys():
            return self.access_clients[device].Get_CanGiveContainer()[site]
        else:
            print("given device has no registered RobotInteractionService")
            return False

    def prepare_to_take(self, device, site):
        if device in self.access_clients.keys():
            uuid = self.access_clients[device].PrepareToGiveContainerFromSite(site)
            t = time()
            timeout = 60
            while time() - t < timeout:
                sleep(.2)
                ready = self.access_clients[device].PrepareToGiveContainerFromSite_Result(uuid.commandExecutionUUID)
                if ready is not None:
                    return ready
        else:
            print("given device has no registered RobotInteractionService")
            return False

    def took(self, device, site):
        if device in self.access_clients.keys():
            self.access_clients[device].ContainerGotTaken(site)
        else:
            print("given device has no registered RobotInteractionService")

    def finish(self, uuid, timeout=120):
        start = time()
        while self.function_running(uuid):
            sleep(.2)
            if time() - start > timeout:
                print(f"function with uuid {uuid} timed out")
                return False
        if self.is_simulation:
            return True
        return self.get_status(uuid) == FunctionStatus.SUCCESSFUL

    def delay(self, milliseconds, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.Delay(milliseconds)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'delay', FunctionStatus.WAITING, args=[milliseconds])

    def pick(self, position, uuid: str, offset=[0, 0, 0], force=.6):
        self.current_uuid = uuid
        if self.is_simulation:
            self.pick_sim(uuid)
        c_uuid = self.wrapper.Get(position, offset, force)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'pick', FunctionStatus.WAITING, args=[position])
        self.occupied = True

    def grip_check(self, expectancy, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.GripCheck(expectancy)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'grip_check', FunctionStatus.WAITING, args=[expectancy])

    def open_grip(self, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.GripOpen()
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'open_grip', FunctionStatus.WAITING, args=[])

    def close_grip(self, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.GripClose()
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'close_grip', FunctionStatus.WAITING, args=[])

    def grip_to(self, millimeters, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.GripTo(millimeters)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'grip_to', FunctionStatus.WAITING, args=[millimeters])

    def home_all(self, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.HomeAll()
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'home_all', FunctionStatus.WAITING, args=[])

    def move_to(self, site: str, uuid: str, offset=[0, 0, 0]):
        self.current_uuid = uuid
        if self.is_simulation:
            self.move_to_sim(site, uuid)
        c_uuid = self.wrapper.SmartMoveTo(site, offset)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'move_to', FunctionStatus.WAITING, args=[site])

    def place(self, position, uuid: str, offset=[0, 0, 0], force=.6):
        self.current_uuid = uuid
        if self.is_simulation:
            self.place_sim(uuid)
        c_uuid = self.wrapper.Put(position, offset, force)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'place', FunctionStatus.WAITING, args=[position])
        self.occupied = False

    def smart_move_safe(self, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.SmartMoveSafe()
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'smart_move_safe', FunctionStatus.WAITING, args=[])

    def move_cartesian(self, dx, dy, dz, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.MoveRelativeCartesian(dx, dy, dz)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'move_cartesian', FunctionStatus.WAITING, args=[])

    def move_straight_to(self, position: str, uuid: str):
        self.current_uuid = uuid
        c_uuid = self.wrapper.MoveStraightTo(position)
        self.function_info[uuid] = FunctionInfo(uuid, c_uuid, 'move_straight_to', FunctionStatus.WAITING, args=[])


exp_duration = {
    ArmProtocol.delay.__name__: 10,
    ArmProtocol.pick.__name__: 10,
    ArmProtocol.grip_check.__name__: 10,
    ArmProtocol.open_grip.__name__: 10,
    ArmProtocol.close_grip.__name__: 10,
    ArmProtocol.grip_to.__name__: 10,
    ArmProtocol.home_all.__name__: 10,
    ArmProtocol.move_to.__name__: 10,
    ArmProtocol.place.__name__: 10,
    ArmProtocol.smart_move_safe.__name__: 10,
    ArmProtocol.move_cartesian.__name__: 1,
    ArmProtocol.move_straight_to.__name__: 2,
    'transfer': 40,
}


class ArmProtocolReal(ArmProtocol, ComSerial):
    def __init__(self):
        ArmProtocol.__init__(self, self, "real_wrapper.dll")
        ComSerial.__init__(self)




