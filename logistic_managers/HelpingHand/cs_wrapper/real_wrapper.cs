using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Threading;
using Thermo.Automation.Motion.Framework;
using Thermo.Automation.Motion.Framework.MotionPlatforms;
using Thermo.Automation.Motion.Framework.QueuedCommands;
using  Thermo.Automation.Motion.Framework.Locations;


namespace arm_wrapper{
    class Wrapper{
        bool initialized = false;

        // This is for ISmartMover we are going to create and command.
        ISmartMover mover = null;
        // And this holds the CommandFormatter we need to create commands for it.
        CommandFormatter formatter;
        //A dictionary for accessing the status of queued commands. 
        IDictionary<string, QueuedCommand> cmd_by_uuid = new Dictionary<string, QueuedCommand>();
        // the five stati a command can have
        int WAITING = 0;
        int RUNNING = 1;
        int SUCCESS = 2;
        int ERROR = 3;
        int CANCELLED = 4;
        // A dictionary for saving results of a command or error messages or whatever is expected
        IDictionary<string, string> result_by_uuid = new Dictionary<string, string>();
        void message(string s){
            Console.WriteLine("Greetings from C#! "+s);
        }
        public int Init(string moverName){
            //
            // NOTE: change this to whatever the name of your mover is...
            //
            //string moverName = "F5";

            #region Create the ISmartMover, and connect to the mover hardware

            //
            // Look up the mover in the system mover list:
            //
            SmartMoverList moverList = new SmartMoverList();
            moverList.Load();   // Actually retrieve the list from storage.

            // Attempt to look up the mover, and get the information about it:
            SmartMoverInfo moverInfo = moverList.GetSmartMoverInfoByName(moverName);
            if (moverInfo == null)
            {
                // We don't have a mover by that name... report this, and exit.
                Console.WriteLine("Can't find a mover called '" + moverName + "' on this system.");
                return 1;
            }

            // At this point, moverInfo has all of the information n
            //
            // Get the mover ready for operation.   The first step is to make sure that its motor power is ON.
            //   object to control the mover.

            Console.WriteLine("Connecting to the mover...");

            // Create the ISmartMover object, and connect to the mover hardware.
            mover = SmartMoverFactory.CreateMover(moverInfo);
            // We need the command formatter for this mover so that we can build commands for it.
            formatter = mover.GetCommandFormatter();

            // Wait for the Mover Framework to finish initialising itself.  This may take a second or two.
            while (mover.GetMoverState().StateSummary == MoverStateSummary.Initialising)
            {               
                // We could also have subscribed to the mover.StateChanged event and used
                //   that to control the wait.
                Thread.Sleep(100);
            }
            Console.WriteLine("   ...done.");

            #endregion


            #region Change the default settings to safer values for testing

            //
            // Change the default motion settings to slow things down a bit.
            // Note that, in general, it _is_ safe to run at the default (full) speed on
            //   production systems, but slower better for examples, for obvious reasons.
            //   Under normal conditions, you likely won't need to adjust the default settings
            //   at all.
            //
            {
                // Get the defaults:
                MotionSettings settings = mover.GetMotionSettings();
                settings.Acceleration = 0.20;       // 20% of maximum acceleration
                settings.Velocity = 0.20;           // 20% of maximum velocity

                // Set this as the new default:
                mover.SetMotionSettings(settings);
            }

            #endregion

            
            #region Get the mover ready for operation

            //
            // Get the mover ready for operation.   The first step is to make sure that its motor power is ON.
            //
            {
                // Get the state to see if we need to turn the power on.
                MoverState state = mover.GetMoverState();

                if (!state.IsPowered)
                {
                    // Power is off.  Give the user directions relevant to an Orbitor.
                    Console.WriteLine("Motor power is turned off.");
                    Console.WriteLine("Make sure the green Motor On light is lit, then hit Enter to continue.");
                    Console.ReadLine();

                    // Now tell the Orbitor to power up.
                    Console.WriteLine("Turning on motor power...");
                    try
                    {
						QueuedCommand command = formatter.SetPowerState(true);
                        mover.Enqueue(command);

                        Finish(command);
                    }
                    catch (Exception x)
                    {
                        Console.WriteLine("Could not turn on motor power: " + x.Message);
                        return 1;         // exit
                    }
                    Console.WriteLine("   ...done.");
                }
            }

            //
            // Home the mover, if necessary.   Note that the Orbitor mover needs to be
            //   homed only when it has been power cycled, to determine exactly what position
            //   the mover is in, so it can be moved accurately.
            //
            {
                // See if we need to home the mover.
                MoverState state = mover.GetMoverState();

                if (!state.IsHomed)
                {
                    // We do need to home... do so.
                    Console.WriteLine("Homing the mover...");
                    try
                    {	
						QueuedCommand command = formatter.HomeAll(true);
                        mover.Enqueue(command);
                        Finish(command);
                    }
                    catch (Exception x)
                    {
                        Console.WriteLine("Homing failed: " + x.Message);
                        return 1;         // exit
                    }

                    Console.WriteLine("   ...done.");
                }
                else
                {
                    Console.WriteLine("The mover is already homed.");
                }
            }

            #endregion

            initialized = true;
            message("Mover " + moverName + " initialized");
            return 0;
        }
        public static void DummyFunction(){
            Console.WriteLine("This is a dummy function.");
        }

          #region Code for waiting for things to finish

        /// <summary>
        /// Wait for a mover command to finish.  This routine works by monitoring the state
        ///   of the QueuedCommand itself (which is updated by the SmartMover as the command
        ///   executes.)
        /// </summary>
        /// <param name="command">The command to wait for.</param>
        /// <remarks>If the command state goes to error, we retrieve the current ErrorException from
        /// the mover and throw it.  This way, you can catch command failures using try/catch.</remarks>
        void Finish(QueuedCommand command)
        {
            while (command.State != CommandState.Success
                && command.State != CommandState.Error)
            {
                // Wait for the command to finish.  Note that it would be more efficient (but also
                //   less simple) to subscribe to the StateChanged event of the command, and
                //   let that drive the wait.
                Thread.Sleep(100);
            }

            // Check to see if the command actually succeeded...
            if (command.State == CommandState.Error)
            {
                // For this example, we will simply throw an appropriate exception to make it simple
                //   for the main program to catch the error.
                MoverErrorState e = mover.GetErrorState();
                if (e.ErrorException != null)
                {
                    // If we are in an error state, we should always get here...
                    throw e.ErrorException;
                }
                else
                {
                    // This should never happen.
                    Debug.Assert(false);
                }
            }
        }



        /// <summary>
        /// Wait for the mover to go to the <c>MoverStateSummary.Ready</c> state.
        /// </summary>
        /// <param name="mover">The ISmartMover to wait for.</param>time
        void Finish()
        {
            MoverState s;
            do
            {
                // Note that we could have subscribed to the mover.StateChanged event
                //   to allow waiting for an event, but this is a very simple approach.
                Thread.Sleep(100);

                // Get the current state of the mover
                s = mover.GetMoverState();
            }
            while (s.StateSummary != MoverStateSummary.Ready
                && s.StateSummary != MoverStateSummary.Fault);

            if (s.StateSummary == MoverStateSummary.Fault)
            {
                MoverErrorState e = mover.GetErrorState();
                throw e.ErrorException;
            }

            return;
        }
        
        #endregion

        string execute(QueuedCommand command){
            if(! initialized){
                message("You have to call init() first.");
                return null;
            }
            if (command != null)
            {
				string line = "something";
                //generate a uuid to identify this command by other functions
                string uuid = System.Guid.NewGuid().ToString();
                try
                {
                    //link the command to the uuid
                    cmd_by_uuid.Add(uuid, command);
                    //add the uuid to the result dictionary
                    result_by_uuid.Add(uuid, null);
                    //actually give the command to the mover
                    mover.Enqueue(command);
                    Console.WriteLine("Executing " + line);
                    //return uuid, so command can be referenced later on
                    return uuid;
                }
                catch (Exception x)
                {
                    // if this is a failed GripCheck() safe the result... right now the same as for every command
                    if(x is MissingObjectInGripperException || x is UnexpectedObjectInGripperException){
                        if(command is GripCheck){
                            result_by_uuid[uuid] = x.Message;
                        }
                    }
                    // storing the error message should always be a good idea
                    result_by_uuid[uuid] = x.Message;
                    Console.WriteLine("'" + line + "' command failed: " + x.Message);
                }
            }
            return null;         // exit
        }
        // returns the status of the corresponding command: Waiting, Running, Fault or Finished
        public int get_status(string uuid){
            QueuedCommand command = cmd_by_uuid[uuid];
            CommandState state = command.State;
            switch(state){
                case CommandState.Pending:
                    return WAITING;
                case CommandState.Executing:
                    return RUNNING;
                case CommandState.Success:
                    return SUCCESS;
                case CommandState.Error:
                    return ERROR;
                case CommandState.Cancelled:
                    return CANCELLED;
            }
            return -1;
        }
        // returns the result of the corresponting command converted into string if there is any, else returns null
        public string get_result(string uuid){
            return result_by_uuid[uuid];
        }
        // tries to recover from an error and returns whether it succeeded
        bool recover_error(){
            // get the error messagetime
            // safe it to the result of the corresponding ommand
            // try to recover:  resolve the error-state 
            //                  move to the next safe position
            // return false if timeout happens or error is not recoverable.... :-(
            return true;
        }
        public string Delay(uint miliseconds){
            QueuedCommand command = formatter.Delay(miliseconds);
            return execute(command);            
        }
        public string Get(string position, float[] off, double force){
			//copy current offset and add the height
            Transform offset= new Transform(mover.GetOffsetTransform());
			Vector3 pos = offset.Position;
			pos.Z = pos.Z + off[2];
			offset.Position = pos;
			// copy current gripper settings and adapt the gripping force
			GripperSettings standard = mover.GetGripperSettings();
			GripperSettings custom = new GripperSettings(force, standard.OpenDistance, standard.ObjectDetectionWidth);
            QueuedCommand command = formatter.Get(position, custom, mover.GetMotionSettings(), offset);
            return execute(command);
        }
        public string GripCheck(bool expectancy){
            QueuedCommand command = formatter.GripCheck(expectancy);
                return execute(command);    
        }
        public string GripOpen(){
            QueuedCommand command = formatter.GripOpen();
            return execute(command);
        }
        public string GripClose(){
            QueuedCommand command = formatter.GripClose();
            return execute(command);
        }
        //grips to a specified distance
        public string GripTo(int millimeters){
            QueuedCommand command = formatter.GripTo(millimeters);
            return execute(command);
        }
        public string HomeAll(){
            QueuedCommand command = formatter.HomeAll();
            return execute(command);
        }

        public string MoveTo(string position){
            QueuedCommand command = formatter.SmartMoveTo(position);
            return execute(command);

        }
        public string Put(string position, float[] off, double force){
			//copy current offset and add the height
            Transform offset= new Transform(mover.GetOffsetTransform());
			Vector3 pos = offset.Position;
			pos.Z = pos.Z + off[2];
			offset.Position = pos;
			// copy current gripper settings and adapt the gripping force
			GripperSettings standard = mover.GetGripperSettings();
			GripperSettings custom = new GripperSettings(force, standard.OpenDistance, standard.ObjectDetectionWidth);
            QueuedCommand command = formatter.Put(position, custom , mover.GetMotionSettings(), offset);
            return execute(command);
        }

        public string SmartMoveTo(string position, float[] off){
			Console.WriteLine("standard: " + mover.GetOffsetTransform().ToString());
            Transform offset= new Transform(mover.GetOffsetTransform());
			Vector3 pos = offset.Position;
			pos.X = pos.X + off[0];
			pos.Y = pos.Y + off[1];
			pos.Z = pos.Z + off[2];
			offset.Position = pos;
			Console.WriteLine("custom: " + offset.ToString() + pos.ToString() + offset.Position);
            QueuedCommand command = formatter.SmartMoveTo(position, mover.GetMotionSettings(), offset);
            return execute(command);            
        }

        public string SmartMoveSafe(){
            QueuedCommand command = formatter.SmartMoveSafe();
            return execute(command);            
        }
        public string MoveRelativeCartesian(double dx, double dy, double dz){
            CartesianLocation movement = new CartesianLocation();
            bool straight = true;
            movement.Position = new Transform(new Vector3(dx, dy, dz), new Quaternion(new Vector3(0, 0, 0)));
            QueuedCommand command = formatter.MoveRelativeCartesian(movement, straight);
            return execute(command);            
        }
        public string MoveStraightTo(string position){
            QueuedCommand command = formatter.MoveStraightTo(position);
            return execute(command);            
        }

    }
}