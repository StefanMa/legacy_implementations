import clr
import System
import os

def get_path(s):
    return os.path.join(os.path.abspath(os.curdir), s)
    
def instanciate(s):
    p = get_path(s)
    dll = clr.AddReference(p)
    s = None
    for i in dll.DefinedTypes:
        s = str(i)
    if s is None:
        print("no type found")
    else:
        print(f"Constructing {s}")
    cl = dll.GetType(s)
    return System.Activator.CreateInstance(cl)
