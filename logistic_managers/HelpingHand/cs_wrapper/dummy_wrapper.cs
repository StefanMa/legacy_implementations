using System;

namespace arm_wrapper{
    class Wrapper{
        bool initialized = false;

        void message(string s){
            Console.WriteLine("Greetings from C#! "+s);
        }
        public int Init(string name){
            initialized = true;
            message(name + " initialized");
            return 0;
        }
        public static void DummyFunction(){
            Console.WriteLine("This is a dummy function.");
        }
        string simulate_command(string command){
            if(initialized){
                message(command);
                return "";
            }
            else{
                message("You have to call init() first.");
                return null;
            }
        }
        public string SmartMoveTo(string position, float[] offset){
            return simulate_command("SmartMoving to position "+position);
        }
        public string MoveTo(string position, float[] offset){
            return simulate_command("SmartMoving to position "+position);
        }
        public string Get(string position, float[] offset){
            return simulate_command("Taking plate from position "+position);
        }
        public string Put(string position, float[] offset){
            return simulate_command("Placing plate on "+position);
        }
        public string GripOpen(){
            return simulate_command("Opening claw");
        }
        public string GripClose(){
            return simulate_command("Closing claw");
        }
        public string GripCheck(bool expectance){
            return simulate_command("Checking for claw_occupancy == "+expectance);
        }
        public string MoveRelativeCartesian(double dx, double dy, double dz){
            return simulate_command("Moving up by (" + dx + ", " + dy + ", " + dz + ") units");
        }
        public string MoveStraightTo(string position){
            return simulate_command("Moving straight to " + position);
        }
        static void Main(String[] args){
            Console.WriteLine("Hello World");
        }
    }
}
