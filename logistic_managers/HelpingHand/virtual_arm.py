"""
This is meant so simulate the behavior of a ThermoFischer incubator shaker
"""

from threading import Timer, Thread
from time import sleep
import os
import json
import logging
from sila2_simulation.simulated_serial import SimulatedSerial
from armProtocol import ArmProtocol, parse_positions


devices = ["Cytomat1550_1", "Rotanta_Rotor", "VarioskanLUX", "Carousel", ]


class VirtualArm(SimulatedSerial):
    def __init__(self):
        # We read in the real occupancy, but will never write in it
        if "siteOccupancy.json" not in os.listdir():
            self.occupancy = []
        else:
            with open("siteOccupancy.json", "r") as instream:
                self.occupancy = json.load(instream)
        self.fast_mode = True
        self.nests = parse_positions()
        self.occupied = False
        self.position = 3
        self.is_running = {}

    # some functions, we have to implement as child of SimulatedSerial
    def do_enquiry(self, command):
        return ""

    def process_command(self, command):
        return ""

    def simulate_command(self, param, val):
        pass

    def get_answer(self, command, terminator='\r', encoding='utf-8', max_wait=2):
        return ""

    # die folgenden befehle sind nur eine uebergangsloesung um die simulation laufen lassen zu können
    def move_to_sim(self, site: str, uuid):
        # some reformating to match the real naming
        if ':' in site:
            site = site[:site.index(':')]
        if site not in self.nests:
            logging.error(f"This site({site}) does not exist in repository.")
        else:
            if site in devices:
                idx = devices.index(site)
            self.do_stuff(5, uuid)
            if site in devices:
                self.position = idx

    def pick_sim(self, uuid):
        if self.occupied:
            self.fatal_crash()
        else:
            self.do_stuff(5, uuid)
            self.occupied = True

    def place_sim(self, uuid):
        if not self.occupied:
            logging.error("nothing grabbed")
        else:
            self.do_stuff(5, uuid)
            self.occupied = False

    def do_stuff(self, duration, uuid=""):
        self.is_running[uuid] = True
        if not self.fast_mode:
            sleep(duration)
        self.is_running[uuid] = False

    def fatal_crash(self, msg=''):
        logging.error(f"AAAAAHHHHHHHRGGGGGG!!!!!!  {msg}")

    def __str__(self):
        d = 12
        o = "   "
        s = f"{'LARA helping hand'.rjust(round(1.9*d), ' ')}\n"
        site = "|_______|"
        arm = ["(o)___(o)",
               "/^^^^^^^\\"]
        empty = "|       |"
        full = "|XXXXXXX|"
        s += o + " "*d*self.position + arm[0] + "\n"
        s += o + " "*d*self.position + arm[1] + "\n"
        if self.occupied:
            s += o + " "*d*self.position + full + "\n"
        else:
            s += o + " "*d*self.position + empty + "\n"
        s += "\n"
        for device in devices:
            s += site.rjust(d, " ")
        s += f"\n{o}"
        for device in devices:
            s += device[:d-2].ljust(d, " ")
        s += "\n"
        return s


class ArmProtocolSimulated(ArmProtocol, VirtualArm):
    def __init__(self):
        ArmProtocol.__init__(self, self, "dummy_wrapper.dll")
        VirtualArm.__init__(self)
