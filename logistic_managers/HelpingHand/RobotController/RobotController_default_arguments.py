# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import RobotController_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()
default_dict['DataType_Site'] = {
    'Site': pb2.DataType_Site.Site_Struct(Device=silaFW_pb2.String(value='default string'), SiteIndex=silaFW_pb2.Integer(value=0))
}

default_dict['MoveToPosition_Parameters'] = {
    'Position': silaFW_pb2.String(value='default string')
}

default_dict['MoveToPosition_Responses'] = {
    
}

default_dict['MoveToSite_Parameters'] = {
    'Site': pb2.DataType_Site(**default_dict['DataType_Site'])
}

default_dict['MoveToSite_Responses'] = {
    
}

default_dict['Retract_Parameters'] = {
    
}

default_dict['Retract_Responses'] = {
    
}

default_dict['ApproachTo_Parameters'] = {
    'Site': pb2.DataType_Site(**default_dict['DataType_Site'])
}

default_dict['ApproachTo_Responses'] = {
    
}

default_dict['MovePlate_Parameters'] = {
    'OriginSite': pb2.DataType_Site(**default_dict['DataType_Site']),
    'DestinationSite': pb2.DataType_Site(**default_dict['DataType_Site']),
    'PlateType': silaFW_pb2.String(value='default string')
}

default_dict['MovePlate_Responses'] = {
    
}

default_dict['PickPlate_Parameters'] = {
    'Site': pb2.DataType_Site(**default_dict['DataType_Site']),
    'PlateType': silaFW_pb2.String(value='default string')
}

default_dict['PickPlate_Responses'] = {
    
}

default_dict['PlacePlate_Parameters'] = {
    'Site': pb2.DataType_Site(**default_dict['DataType_Site']),
    'PlateType': silaFW_pb2.String(value='default string')
}

default_dict['PlacePlate_Responses'] = {
    
}

default_dict['CheckOccupied_Parameters'] = {
    'Site': pb2.DataType_Site(**default_dict['DataType_Site'])
}

default_dict['CheckOccupied_Responses'] = {
    'Occupied': silaFW_pb2.Boolean(value=False)
}


