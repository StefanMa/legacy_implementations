#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HelpingHand client*

:details: HelpingHand:
    SiLA2 service for LARA robotic arm

:file:    RobotController_client.py
:authors: mark doerr, stefan maak

:date: (creation)          2021-04-05T00:11:26.027509
:date: (last modification) 2021-04-05T00:11:26.027509

.. note:: Code generated by sila2codegenerator 0.3.4

_______________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"

# import general packages
import logging
import argparse
import grpc
import time

# import meta packages
from typing import Union, Optional

# import SiLA2 library modules
from sila2lib.framework import SiLAFramework_pb2 as silaFW_pb2
from sila2lib.sila_client import SiLA2Client
from sila2lib.framework.std_features import SiLAService_pb2 as SiLAService_feature_pb2
from sila2lib.error_handling import client_err
#   Usually not needed, but - feel free to modify
# from sila2lib.framework.std_features import SimulationController_pb2 as SimController_feature_pb2

# import feature gRPC modules
# Import gRPC libraries of features
from RobotController.gRPC import RobotController_pb2
from RobotController.gRPC import RobotController_pb2_grpc
# import default arguments for this feature
from RobotController.RobotController_default_arguments import default_dict as RobotController_default_dict


# noinspection PyPep8Naming, PyUnusedLocal
class RobotControllerClient:
    """
        SiLA2 service for LARA robotic arm

    .. note:: For an example on how to construct the parameter or read the response(s) for command calls and properties,
              compare the default dictionary that is stored in the directory of the corresponding feature.
    """
    # The following variables will be filled when run() is executed
    #: Storage for the connected servers version
    server_version: str = ''
    #: Storage for the display name of the connected server
    server_display_name: str = ''
    #: Storage for the description of the connected server
    server_description: str = ''

    def __init__(self,
                 channel = None):
        """Class initialiser"""

        # Create stub objects used to communicate with the server
        self.RobotController_stub = \
            RobotController_pb2_grpc.RobotControllerStub(channel)

        # initialise class variables for server information storage
        self.server_version = ''
        self.server_display_name = ''
        self.server_description = ''

    def MoveToPosition(self, Position,
                      parameter: RobotController_pb2.MoveToPosition_Parameters = None) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Wrapper to call the observable command MoveToPosition on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A command confirmation object with the following information:
            commandExecutionUUID: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution (optional): The (maximum) lifetime of this command call.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Calling MoveToPosition:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.MoveToPosition_Parameters(
                    Position=silaFW_pb2.String(value=Position)
                )
    
            response = self.RobotController_stub.MoveToPosition(parameter)
    
            logging.debug('MoveToPosition response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return response
    
    def MoveToPosition_Info(self,
                           uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Wrapper to get an intermediate response for the observable command MoveToPosition on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the status information that has been defined for this command. The following fields
                  are defined:
                    * *commandStatus*: Status of the command (enumeration)
                    * *progressInfo*: Information on the progress of the command (0 to 1)
                    * *estimatedRemainingTime*: Estimate of the remaining time required to run the command
                    * *updatedLifetimeOfExecution*: An update on the execution lifetime
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command MoveToPosition (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
        try:
            response = self.RobotController_stub.MoveToPosition_Info(uuid)
            logging.debug('MoveToPosition status information: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MoveToPosition_Result(self,
                             uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> RobotController_pb2.MoveToPosition_Responses:
        """
        Wrapper to get an intermediate response for the observable command MoveToPosition on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the result response that has been defined for this command.
    
        .. note:: Whether the result is available or not can and should be evaluated by calling the
                  :meth:`MoveToPosition_Info` method of this call.
        """
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command MoveToPosition (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
    
        try:
            response = self.RobotController_stub.MoveToPosition_Result(uuid)
            logging.debug('MoveToPosition result response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MoveToSite(self, device, site_idx,
                      parameter: RobotController_pb2.MoveToSite_Parameters = None) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Wrapper to call the observable command MoveToSite on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A command confirmation object with the following information:
            commandExecutionUUID: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution (optional): The (maximum) lifetime of this command call.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Calling MoveToSite:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.MoveToSite_Parameters(
                    Site=RobotController_pb2.DataType_Site(
                        Site=RobotController_pb2.DataType_Site.Site_Struct(
                            Device=silaFW_pb2.String(value=device),
                            SiteIndex=silaFW_pb2.Integer(value=site_idx)))
                )
    
            response = self.RobotController_stub.MoveToSite(parameter)
    
            logging.debug('MoveToSite response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MoveToSite_Info(self,
                           uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Wrapper to get an intermediate response for the observable command MoveToSite on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the status information that has been defined for this command. The following fields
                  are defined:
                    * *commandStatus*: Status of the command (enumeration)
                    * *progressInfo*: Information on the progress of the command (0 to 1)
                    * *estimatedRemainingTime*: Estimate of the remaining time required to run the command
                    * *updatedLifetimeOfExecution*: An update on the execution lifetime
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command MoveToSite (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
        try:
            response = self.RobotController_stub.MoveToSite_Info(uuid)
            logging.debug('MoveToSite status information: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MoveToSite_Result(self,
                             uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> RobotController_pb2.MoveToSite_Responses:
        """
        Wrapper to get an intermediate response for the observable command MoveToSite on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the result response that has been defined for this command.
    
        .. note:: Whether the result is available or not can and should be evaluated by calling the
                  :meth:`MoveToSite_Info` method of this call.
        """
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command MoveToSite (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
    
        try:
            response = self.RobotController_stub.MoveToSite_Result(uuid)
            logging.debug('MoveToSite result response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def Retract(self,
                      parameter: RobotController_pb2.Retract_Parameters = None) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Wrapper to call the observable command Retract on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A command confirmation object with the following information:
            commandExecutionUUID: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution (optional): The (maximum) lifetime of this command call.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Calling Retract:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.Retract_Parameters(
                    **RobotController_default_dict['Retract_Parameters']
                )
    
            response = self.RobotController_stub.Retract(parameter)
    
            logging.debug('Retract response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def Retract_Info(self,
                           uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Wrapper to get an intermediate response for the observable command Retract on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the status information that has been defined for this command. The following fields
                  are defined:
                    * *commandStatus*: Status of the command (enumeration)
                    * *progressInfo*: Information on the progress of the command (0 to 1)
                    * *estimatedRemainingTime*: Estimate of the remaining time required to run the command
                    * *updatedLifetimeOfExecution*: An update on the execution lifetime
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command Retract (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
        try:
            response = self.RobotController_stub.Retract_Info(uuid)
            logging.debug('Retract status information: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def Retract_Result(self,
                             uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> RobotController_pb2.Retract_Responses:
        """
        Wrapper to get an intermediate response for the observable command Retract on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the result response that has been defined for this command.
    
        .. note:: Whether the result is available or not can and should be evaluated by calling the
                  :meth:`Retract_Info` method of this call.
        """
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command Retract (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
    
        try:
            response = self.RobotController_stub.Retract_Result(uuid)
            logging.debug('Retract result response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def ApproachTo(self,
                      parameter: RobotController_pb2.ApproachTo_Parameters = None) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Wrapper to call the observable command ApproachTo on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A command confirmation object with the following information:
            commandExecutionUUID: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution (optional): The (maximum) lifetime of this command call.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Calling ApproachTo:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.ApproachTo_Parameters(
                    **RobotController_default_dict['ApproachTo_Parameters']
                )
    
            response = self.RobotController_stub.ApproachTo(parameter)
    
            logging.debug('ApproachTo response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def ApproachTo_Info(self,
                           uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Wrapper to get an intermediate response for the observable command ApproachTo on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the status information that has been defined for this command. The following fields
                  are defined:
                    * *commandStatus*: Status of the command (enumeration)
                    * *progressInfo*: Information on the progress of the command (0 to 1)
                    * *estimatedRemainingTime*: Estimate of the remaining time required to run the command
                    * *updatedLifetimeOfExecution*: An update on the execution lifetime
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command ApproachTo (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
        try:
            response = self.RobotController_stub.ApproachTo_Info(uuid)
            logging.debug('ApproachTo status information: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def ApproachTo_Result(self,
                             uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> RobotController_pb2.ApproachTo_Responses:
        """
        Wrapper to get an intermediate response for the observable command ApproachTo on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the result response that has been defined for this command.
    
        .. note:: Whether the result is available or not can and should be evaluated by calling the
                  :meth:`ApproachTo_Info` method of this call.
        """
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command ApproachTo (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
    
        try:
            response = self.RobotController_stub.ApproachTo_Result(uuid)
            logging.debug('ApproachTo result response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MovePlate(self, origin_device, origin_site_idx, destination_device, destination_site_idx, plate_type="whatever",
                      parameter: RobotController_pb2.MovePlate_Parameters = None) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Wrapper to call the observable command MovePlate on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A command confirmation object with the following information:
            commandExecutionUUID: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution (optional): The (maximum) lifetime of this command call.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Calling MovePlate:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.MovePlate_Parameters(
                    OriginSite=RobotController_pb2.DataType_Site(
                        Site=RobotController_pb2.DataType_Site.Site_Struct(
                            Device=silaFW_pb2.String(value=origin_device),
                            SiteIndex=silaFW_pb2.Integer(value=origin_site_idx))),
                    DestinationSite=RobotController_pb2.DataType_Site(
                        Site=RobotController_pb2.DataType_Site.Site_Struct(
                            Device=silaFW_pb2.String(value=destination_device),
                            SiteIndex=silaFW_pb2.Integer(value=destination_site_idx))),
                    PlateType=silaFW_pb2.String(value=plate_type)
                )
    
            response = self.RobotController_stub.MovePlate(parameter)
    
            logging.debug('MovePlate response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MovePlate_Info(self,
                           uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Wrapper to get an intermediate response for the observable command MovePlate on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the status information that has been defined for this command. The following fields
                  are defined:
                    * *commandStatus*: Status of the command (enumeration)
                    * *progressInfo*: Information on the progress of the command (0 to 1)
                    * *estimatedRemainingTime*: Estimate of the remaining time required to run the command
                    * *updatedLifetimeOfExecution*: An update on the execution lifetime
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command MovePlate (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
        try:
            response = self.RobotController_stub.MovePlate_Info(uuid)
            logging.debug('MovePlate status information: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def MovePlate_Result(self,
                             uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> RobotController_pb2.MovePlate_Responses:
        """
        Wrapper to get an intermediate response for the observable command MovePlate on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the result response that has been defined for this command.
    
        .. note:: Whether the result is available or not can and should be evaluated by calling the
                  :meth:`MovePlate_Info` method of this call.
        """
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command MovePlate (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
    
        try:
            response = self.RobotController_stub.MovePlate_Result(uuid)
            logging.debug('MovePlate result response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def PickPlate(self, device='home', site_idx=0, PlateType: str='whatever'):  # -> (RobotController):
        """
        Wrapper to call the unobservable command PickPlate on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling PickPlate:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.PickPlate_Parameters(
                    Site=RobotController_pb2.DataType_Site(
                        Site=RobotController_pb2.DataType_Site.Site_Struct(
                            Device=silaFW_pb2.String(value=device),
                            SiteIndex=silaFW_pb2.Integer(value=site_idx)))
                    ,
                    PlateType=silaFW_pb2.String(value=PlateType)
                )
    
            response = self.RobotController_stub.PickPlate(parameter, metadata)
            logging.debug(f"PickPlate response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return
    
    def PlacePlate(self, device='home', site_idx=0, PlateType: str='whatever'):  # -> (RobotController):
        """
        Wrapper to call the unobservable command PlacePlate on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling PlacePlate:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.PlacePlate_Parameters(
                    Site=RobotController_pb2.DataType_Site(
                        Site=RobotController_pb2.DataType_Site.Site_Struct(
                            Device=silaFW_pb2.String(value=device),
                            SiteIndex=silaFW_pb2.Integer(value=site_idx)))
                    ,
                    PlateType=silaFW_pb2.String(value=PlateType)
                )
    
            response = self.RobotController_stub.PlacePlate(parameter, metadata)
            logging.debug(f"PlacePlate response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return
    
    def CheckOccupied(self, device='home', site_idx=0):  # -> (RobotController):
        """
        Wrapper to call the unobservable command CheckOccupied on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling CheckOccupied:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RobotController_pb2.CheckOccupied_Parameters(
                    Site=RobotController_pb2.DataType_Site(
                        Site=RobotController_pb2.DataType_Site.Site_Struct(
                            Device=silaFW_pb2.String(value=device),
                            SiteIndex=silaFW_pb2.Integer(value=site_idx)))
                )
    
            response = self.RobotController_stub.CheckOccupied(parameter, metadata)
            logging.debug(f"CheckOccupied response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
        return response.Occupied.value

    #   No properties defined

    @staticmethod
    def grpc_error_handling(error_object: grpc.Call) -> None:
        """Handles exceptions of type grpc.RpcError"""
        # pass to the default error handling
        grpc_error =  client_err.grpc_error_handling(error_object=error_object)

        # Access more details using the return value fields
        logging.error(grpc_error.error_type)
        if hasattr(grpc_error.message, "parameter"):
            logging.error(grpc_error.message.parameter)
        logging.error(grpc_error.message.message)
