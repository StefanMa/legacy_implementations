"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Robot Controller*

:details: RobotController:
    Controlling the robot
           
:file:    RobotController_servicer.py
:authors: mark doerr, stefan maak

:date: (creation)          2021-04-05T00:11:26.018833
:date: (last modification) 2021-04-05T00:11:26.018833

.. note:: Code generated by sila2codegenerator 0.3.4

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.1"

# import general packages
import logging
import grpc

# meta packages
from typing import Union

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
from sila2lib.error_handling.server_err import SiLAError

# import gRPC modules for this feature
from .gRPC import RobotController_pb2 as RobotController_pb2
from .gRPC import RobotController_pb2_grpc as RobotController_pb2_grpc

# import simulation and real implementation
from .RobotController_simulation import RobotControllerSimulation
from .RobotController_real import RobotControllerReal

# import SiLA Defined Error factories
from .RobotController_defined_errors import generate_def_error_SiteNotFound, generate_def_error_LostPlate, generate_def_error_InaccessibleSite


class RobotController(RobotController_pb2_grpc.RobotControllerServicer):
    """
    SiLA2 service for LARA robotic arm
    """
    implementation: Union[RobotControllerSimulation, RobotControllerReal]
    simulation_mode: bool

    def __init__(self, simulation_mode: bool = True, hardware_interface=None):
        """
        Class initialiser.

        :param simulation_mode: Sets whether at initialisation the simulation mode is active or the real mode.
        :param hardware_interface (optional): access to shared hardware interface, like serial interface. 
        """

        self.hardware_interface = hardware_interface

        self.simulation_mode = simulation_mode
        if simulation_mode:
            self._inject_implementation(RobotControllerSimulation())
        else:
            self._inject_implementation(RobotControllerReal(hardware_interface=self.hardware_interface))

    def _inject_implementation(self,
                               implementation: Union[RobotControllerSimulation,
                                                     RobotControllerReal]
                               ) -> bool:
        """
        Dependency injection of the implementation used.
            Allows to set the class used for simulation/real mode.

        :param implementation: A valid implementation of the HelpingHandServicer.
        """

        self.implementation = implementation
        return True

    def switch_to_simulation_mode(self):
        """Method that will automatically be called by the server when the simulation mode is requested."""
        self.simulation_mode = True
        self._inject_implementation(RobotControllerSimulation())

    def switch_to_real_mode(self):
        """Method that will automatically be called by the server when the real mode is requested."""
        self.simulation_mode = False
        self._inject_implementation(RobotControllerReal(hardware_interface=self.hardware_interface))

    def MoveToPosition(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Executes the observable command "Move To Position"
            Move to a given position.
    
        :param request: gRPC request containing the parameters passed:
            request.Position (Position): Position to move to.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A command confirmation object with the following information:
            commandId: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution: The (maximum) lifetime of this command call.
        """
    
        logging.debug(
            "MoveToPosition called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.MoveToPosition(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def MoveToPosition_Info(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Returns execution information regarding the command call :meth:`~.MoveToPosition`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: An ExecutionInfo response stream for the command with the following fields:
            commandStatus: Status of the command (enumeration)
            progressInfo: Information on the progress of the command (0 to 1)
            estimatedRemainingTime: Estimate of the remaining time required to run the command
            updatedLifetimeOfExecution: An update on the execution lifetime
        """
    
        logging.debug(
            "MoveToPosition_Info called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.MoveToPosition_Info(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def MoveToPosition_Result(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.MoveToPosition_Responses:
        """
        Returns the final result of the command call :meth:`~.MoveToPosition`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "MoveToPosition_Result called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.MoveToPosition_Result(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    
    def MoveToSite(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Executes the observable command "Move To Site"
            Moving to a given site including calculated approach positions (if any, and without changing gripping)
    
        :param request: gRPC request containing the parameters passed:
            request.Site (Site): Site to move to
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A command confirmation object with the following information:
            commandId: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution: The (maximum) lifetime of this command call.
        """
    
        logging.debug(
            "MoveToSite called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.MoveToSite(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def MoveToSite_Info(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Returns execution information regarding the command call :meth:`~.MoveToSite`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: An ExecutionInfo response stream for the command with the following fields:
            commandStatus: Status of the command (enumeration)
            progressInfo: Information on the progress of the command (0 to 1)
            estimatedRemainingTime: Estimate of the remaining time required to run the command
            updatedLifetimeOfExecution: An update on the execution lifetime
        """
    
        logging.debug(
            "MoveToSite_Info called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.MoveToSite_Info(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def MoveToSite_Result(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.MoveToSite_Responses:
        """
        Returns the final result of the command call :meth:`~.MoveToSite`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "MoveToSite_Result called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.MoveToSite_Result(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    
    def Retract(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Executes the observable command "Retract"
            Retract from current device if not retracted already
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A command confirmation object with the following information:
            commandId: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution: The (maximum) lifetime of this command call.
        """
    
        logging.debug(
            "Retract called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.Retract(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def Retract_Info(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Returns execution information regarding the command call :meth:`~.Retract`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: An ExecutionInfo response stream for the command with the following fields:
            commandStatus: Status of the command (enumeration)
            progressInfo: Information on the progress of the command (0 to 1)
            estimatedRemainingTime: Estimate of the remaining time required to run the command
            updatedLifetimeOfExecution: An update on the execution lifetime
        """
    
        logging.debug(
            "Retract_Info called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.Retract_Info(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def Retract_Result(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.Retract_Responses:
        """
        Returns the final result of the command call :meth:`~.Retract`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "Retract_Result called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.Retract_Result(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    
    def ApproachTo(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Executes the observable command "Approach To"
            Moving to the safe pose to approach a device (without changing gripping)
    
        :param request: gRPC request containing the parameters passed:
            request.Site (Site): Site to move approach
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A command confirmation object with the following information:
            commandId: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution: The (maximum) lifetime of this command call.
        """
    
        logging.debug(
            "ApproachTo called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.ApproachTo(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def ApproachTo_Info(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Returns execution information regarding the command call :meth:`~.ApproachTo`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: An ExecutionInfo response stream for the command with the following fields:
            commandStatus: Status of the command (enumeration)
            progressInfo: Information on the progress of the command (0 to 1)
            estimatedRemainingTime: Estimate of the remaining time required to run the command
            updatedLifetimeOfExecution: An update on the execution lifetime
        """
    
        logging.debug(
            "ApproachTo_Info called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.ApproachTo_Info(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def ApproachTo_Result(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.ApproachTo_Responses:
        """
        Returns the final result of the command call :meth:`~.ApproachTo`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "ApproachTo_Result called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.ApproachTo_Result(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    
    def MovePlate(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Executes the observable command "Move Plate"
            Move a plate between two given sites
    
        :param request: gRPC request containing the parameters passed:
            request.OriginSite (Origin Site): Site to move the plate from
            request.DestinationSite (Destination Site): Site to move the plate to
            request.PlateType (Plate Type): The plate type to grab
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A command confirmation object with the following information:
            commandId: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution: The (maximum) lifetime of this command call.
        """
    
        logging.debug(
            "MovePlate called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.MovePlate(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def MovePlate_Info(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Returns execution information regarding the command call :meth:`~.MovePlate`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: An ExecutionInfo response stream for the command with the following fields:
            commandStatus: Status of the command (enumeration)
            progressInfo: Information on the progress of the command (0 to 1)
            estimatedRemainingTime: Estimate of the remaining time required to run the command
            updatedLifetimeOfExecution: An update on the execution lifetime
        """
    
        logging.debug(
            "MovePlate_Info called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.MovePlate_Info(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def MovePlate_Result(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.MovePlate_Responses:
        """
        Returns the final result of the command call :meth:`~.MovePlate`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "MovePlate_Result called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.MovePlate_Result(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    
    def PickPlate(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.PickPlate_Responses:
        """
        Executes the unobservable command "Pick Plate"
            Pick up a plate from a given site
    
        :param request: gRPC request containing the parameters passed:
            request.Site (Site): Site to pick plate from
            request.PlateType (Plate Type): The plate type to grab
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "PickPlate called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.PickPlate(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def PlacePlate(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.PlacePlate_Responses:
        """
        Executes the unobservable command "Place Plate"
            Place a plate on a given site
    
        :param request: gRPC request containing the parameters passed:
            request.Site (Site): Site to place the plate on
            request.PlateType (Plate Type): The plate type to grab
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        logging.debug(
            "PlacePlate called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.PlacePlate(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def CheckOccupied(self, request, context: grpc.ServicerContext) \
            -> RobotController_pb2.CheckOccupied_Responses:
        """
        Executes the unobservable command "Check Occupied"
            Check if the given site is occupied with a sample
    
        :param request: gRPC request containing the parameters passed:
            request.Site (Site): Site to check.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Occupied (Occupied): Boolean describing if site is occupied
        """
    
        logging.debug(
            "CheckOccupied called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        # parameter validation
        # if request.my_paramameter.value out of scope :
        #        sila_val_err = SiLAValidationError(parameter="myParameter",
        #                                           msg=f"Parameter {request.my_parameter.value} out of scope!")
        #        sila_val_err.raise_rpc_error(context)
    
        try:
            return self.implementation.CheckOccupied(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)

    
