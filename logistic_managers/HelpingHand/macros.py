from time import time, sleep

def watch_execution(fct, info_fct, args):
	uuid = fct(*args)
	info = info_fct(uuid.commandExecutionUUID)
	while info.is_active():
		n= info.next()
		print(f"time_remaining:{n.estimatedRemainingTime.seconds}/{n.estimatedRemainingTime.seconds+n.updatedLifetimeOfExecution.seconds}: {n.CommandStatus.Name(n.commandStatus)}")
		sleep(.1)
