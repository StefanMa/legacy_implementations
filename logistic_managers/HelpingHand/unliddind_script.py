from time import sleep


def unlid(arm, plate_nest=1, lid_nest=2):
    hotel = "Hotel_1"
    delta_lid = .5
    delta_open = 3
    arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,0,0])
    arm.close_grip("")
    arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,0,17])
    for i in range(5):
        arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,0,16])
        arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,0,14])
    for i in range(5):
        arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,2,14])
        arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,0,14])
    for i in range(5):
        arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[2,0,14])
        arm.move_to('Hotel_1:nest[1]', "dummy_uuid_1", offset=[0,0,14])
    arm.move_to('Hotel_1:safe', "dummy_uuid_1", offset=[0,0,4])
    arm.place('Hotel_1:nest[2]', "dummy_uuid_2", offset=[0,0,-3], force=.2)
    arm.pick('Hotel_1:nest[2]', "dummy_uuid_3", offset=[0,0,-3], force=.2)
    arm.place('Hotel_1:nest[1]', "dummy_uuid_4", offset=[0,0,10], force=.1)
