"""
A unittest for the cytomat client-server pair. It uses the virtual_cytomat to emulate a real one.
The test can be run in fast-mode for fast testing or in real mode for realistic reaction/working times of the device.
"""
from time import sleep
import sys
sys.path.append("/home/stefan/workspace/sila_python/implementations/logistic_managers/PlateStorageHotel")
import unittest
from sila2_simulation.deviceTest import DeviceTest
from sila2lib.error_handling.server_err import SiLAExecutionError
from HelpingHand_client import HelpingHandClient
from HelpingHand_server import HelpingHandServer


class ArmTest(DeviceTest, unittest.TestCase):
    def is_error(self, feedback) -> bool:
        return issubclass(type(feedback), SiLAExecutionError)

    def setUp(self, port=50051) -> None:
        super().setUp()
        self.server = HelpingHandServer(cmd_args=self.create_args(port=port), simulation_mode=True, block=False)
        # give the server some time to register with zeroconfig
        sleep(.5)
        self.client = HelpingHandClient()

    def test_process(self):
        client = self.client.robotController_client
        self.send_command(client.MoveToSite, 1.2, .2, args=["Cytomat1550_1", 0])
        # this will not work until we add handles for the RobotInteractionService clients
        # self.send_command(client.MovePlate, 3.4, .4, args=["hotel", 0, "centrifuge", 0])


if __name__ == '__main__':
    mytest = ArmTest()
    mytest.runSimulation()


def continue_test():
    print("Enter c to continue. Press Enter to scip step.")
    cmd = input()
    return cmd == "c"


def life_test():
    client = HelpingHandClient()
    robot = client.robotController_client
    # open->close->open the gripper
    robot.CheckOccupied()
    rotanta_nest1 = "Rotanta_Transfer:nest[1]"
    home = "SDTloc:safe"
    hotel = "Carousel:nest[1]"
    if continue_test():
        robot.CheckOccupied(device=rotanta_nest1)
    else:
        return
    if continue_test():
        robot.PickPlate(device=rotanta_nest1)
    else:
        return
    if continue_test():
        robot.MoveToPosition(home)
    if continue_test():
        robot.PlacePlate(hotel)
    else:
        return

