import re

from sila2comlib.com.com_serial import ComSerial
from sila2comlib.com.simulated_serial import SimulatedSerial
from typing import Union
from abc import ABC
from aenum import Enum
import logging
from threading import Timer
import time

ADR = ']'
STX = '\x02'
ETX = '\x03'
EOT = '\x04'
ENQ = '\x05'
ACK = '\x06'
NAK = '\x15'

# parameter codes
param_code = dict(
    nonsense='00555',
    actual_rotor_speed='420',  # use for slippage only
    target_motor_speed='00422',
    software_lock='00520',
    control_centrifuge='00521',
    set_rotor_position='00524',
    set_position_and_hatch='00526',
    get_position_and_hatch='00528',
    total_cycles_high='00567',
    total_cycles_low='00568',
    set_speed='00603',
    actual_speed='00604',
    max_speed='00605',
    max_rcf='00608',
    acceleration_speed='00611',
    break_speed='00612',
    set_temperature='00618',
    get_temperature='00619',
    radius="00620",
    SIOF='00685',  # has to be read after an error. Otherwise centrifuge wont accept select-commands
    #  ...many missing yet
)

# the values that have to be given with parameter 00521(control_centrifuge)
start_centrifuge_code = 2
stop_centrifuge_code = 1
# the values that have to be given with parameter 00520(lock_centrifuge)
lock_centrifuge_code = 2**0
unlock_centrifuge_code = 2**3

# possible answers to select-commands
accepted = f"{ADR}{ACK}"
not_accepted = f"{ADR}{NAK}"


# cmd.value gives you the int representation
class MoveCommand(Enum):
    """ A readable dictionary for the values to send with param 526"""
    POSITION_SLOW = 0x1
    POSITION_FAST = 0x2
    CANCEL_POSITIONING = 0x40
    OPEN_COVER = 0x60
    CLOSE_COVER = 0x70
    TERMINATE_POSITIONING = 0x80


def decode_pos_cover_status(val: int):
    return dict(
        brake_implemented=bool(val & 2**15),
        cover_timeout=bool(val & 2 ** 14),
        cover_is_open=bool(val & 2 ** 13),
        cover_is_closed=bool(val & 2 ** 12),
        cover_lock_closed=bool(val & 2 ** 11),
        cover_moving=bool(val & 2 ** 10),
        cover_opening=bool(val & 2**9),
        cover_closing=bool(val & 2**8),
        brake_active=bool(val & 2**5),
        position_reached=bool(val & 2**2),
        rotor_positioning=bool(val & 2**0),
    )


error_code = [
    "Power On (after Reset , mains interrupt)",
    "SIOF parity error",
    "undefined",
    "wrong BCC checksum",
    "framing error , wrong STX , ETX , ENQ or '='",
    "wrong or unknown parameter",
    "modification not permitted (read only parameter)",
    "improper value or command not allowed",
    ]


def calc_bcc(base: str):
    res = 0
    for c in base:
        res ^= ord(c)
    return chr(res)


def encode_temperature(temperature_in_celsius):
    return int((temperature_in_celsius+25)*2)


def decode_temperature(answer_value):
    return answer_value/2 - 25


# patterns for syntax check
enquiry_pattern = r''+EOT+ADR+r'00\d\d\d'+ENQ
answer_pattern = r''+ADR+STX+r'00\d\d\d=[0-9A-Fa-f]{4,4}'+ETX+r'(.|\n)'
command_pattern = r''+EOT+ADR+STX+r'00\d\d\d=[0-9A-Fa-f]{4,4}'+ETX+r'(.|\n)'
ok_pattern = r''+ADR+r'[' + ACK + NAK + r']'


def construct_enquiry(param_name: str):
    return f"{EOT}{ADR}{param_code[param_name]}{ENQ}"


def construct_select(param_name: str, value: int):
    hex_value = str(hex(value)).replace('0x', '')
    bcc = calc_bcc(f"{param_code[param_name]}={hex_value.rjust(4,'0')}{ETX}")
    return f"{EOT}{ADR}{STX}{param_code[param_name]}={hex_value.rjust(4,'0')}{ETX}{bcc}"


# this seems to be necessary, since rotanta more or less randomly sets the first bit of some bytes
def unmask(b):
    hex_bytes = b.hex()
    hex_ar = [hex_bytes[2*i:2*i+2] for i in range(len(b))]
    int_ar = [int(c, 16) for c in hex_ar]
    m_ar = [i if i < 128 else i-128 for idx, i in enumerate(int_ar)]
    chars = [chr(i) for i in m_ar]
    result = ''.join(chars)
    return result


class RotantaProtocol(ABC):
    """
    A class for utility functions to enable easy communication with a HettichRotantaR460 via the inherent protocol
    """
    def __init__(self, com: Union[ComSerial, SimulatedSerial]):
        self.com = com
        # according to manual, the SIOF bit will be set after switching on the centrifuge
        t = Timer(interval=2, function=self.read_SIOF)
        t.start()

    def send_enquiry(self, param: str):
        #print(f"sending enquiry for {param}")
        try:
            command = construct_enquiry(param)
            #print(f"command:{command}")
            answer = self.com.get_centrifuge_answer(command)
            if answer == not_accepted:
                if param == "SIOF":
                    logging.warning("Could not read the error byte... this is bad, infinite loop incoming?")
                self.read_SIOF()
                return answer
            else:
                # return the value as integer
                return int(answer[8:12], 16)
        except Exception as ex:
            print(ex)
            return not_accepted

    def send_command(self, param: str, val: int):
        #print(f"sending command for {param}")
        try:
            command = construct_select(param, val)
            answer = self.com.get_centrifuge_answer(command)
            if answer == not_accepted:
                self.read_SIOF()
            return answer
        except Exception as ex:
            print(ex)
            return not_accepted

    def read_SIOF(self):
        answer = self.send_enquiry("SIOF")
        if not type(answer) == int:
            return
        for bit, text in enumerate(error_code):
            if 2**bit & answer:
                with open("error_log.txt", 'a') as out:
                    out.write(f"{time.ctime()}: {text}\n")


class RotantaProtocolReal(RotantaProtocol, ComSerial):
    def __init__(self):
        RotantaProtocol.__init__(self, self)
        ComSerial.__init__(self)

    def get_centrifuge_answer(self, command, encoding='utf-8', max_wait=2):
        """
        :param max_wait: The maximum waiting time for the communication port becoming available
        :param encoding: the encoding used for the command and the answer
        :param command: the string to write via the serial
        :return: the decoded answer
        """
        start = time.time()
        answer = ""
        while self.com.in_communication:
            time.sleep(.001)
            if time.time() > start + max_wait:
                logging.info("The hardware interface was busy for longer than the given maximum waiting time of"
                             f"{max_wait} seconds. Returning ''.")
                return answer
        self.com.in_communication = True
        try:
            #print(f"raw command: {command.encode(encoding)}")
            self.com.serial_port.write(command.encode(encoding))
            start = time.time()
            buffer = b''
            while time.time() - start < .5:
                time.sleep(.01)
                buffer += self.com.serial_port.read(100)
                unmasked = unmask(buffer)
                for pattern in [ok_pattern, answer_pattern]:
                    if re.search(pattern, unmasked):
                        return unmasked
            #print(f"raw answer: {answer}")
            answer = unmask(buffer)
        finally:
            self.com.in_communication = False
        return answer




