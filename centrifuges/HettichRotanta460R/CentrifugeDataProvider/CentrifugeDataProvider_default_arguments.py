# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import CentrifugeDataProvider_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()




default_dict['Get_MaxForce_Responses'] = {
    'MaxForce': silaFW_pb2.Real(value=1.0)
}

default_dict['Get_MinForce_Responses'] = {
    'MinForce': silaFW_pb2.Real(value=1.0)
}

default_dict['Get_MaxFrequency_Responses'] = {
    'MaxFrequency': silaFW_pb2.Integer(value=1)
}

default_dict['Get_MinFrequency_Responses'] = {
    'MinFrequency': silaFW_pb2.Integer(value=1)
}

default_dict['Get_MaxRadius_Responses'] = {
    'MaxRadius': silaFW_pb2.Real(value=1.0)
}

default_dict['Get_MinRadius_Responses'] = {
    'MinRadius': silaFW_pb2.Real(value=1.0)
}

default_dict['Get_HoursInUse_Responses'] = {
    'HoursInUse': silaFW_pb2.Integer(value=1)
}

default_dict['Get_CyclesRan_Responses'] = {
    'CyclesRan': silaFW_pb2.Integer(value=1)
}

default_dict['Get_MaxCyclesRan_Responses'] = {
    'MaxCyclesRan': silaFW_pb2.Integer(value=1)
}
