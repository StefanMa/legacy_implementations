"""
This is meant so simulate the behavior of a Hettich rotanta centrifuge
"""

from threading import Timer, Thread
from time import sleep
import os
import json
import re
from sila2_simulation.simulated_serial import SimulatedSerial
from rotantaProtocol import *  # EOT, ADR, ENQ, ETX, STX, not_accepted, accepted, param_code, start_centrifuge_code, \
#      lock_centrifuge_code, unlock_centrifuge_code, stop_centrifuge_code


min_frequency = 50  # given in rounds per minute
max_frequency = 20000


# since rotanta uses checksums, the terminator can be any character. So we have to check for a pattern
def find_terminator(command, buffer):
    if re.match(enquiry_pattern, command):
        # in this case we expect an answer or not_accepted
        pattern = f'({answer_pattern})*({not_accepted})*'
    elif re.match(command_pattern, command):
        # in this case we expect only whether the command was accepted
        pattern = f'({accepted})*({not_accepted})*'
    else:
        logging.error("There was a command which was neither enquiry nor select")
        return 0
    m = re.match(pattern, buffer)
    return m.end()


# the way rotanta answers to an enquiry
def construct_answer(param, value):
    hex_value = str(hex(value)).replace('0x', '')
    bcc = calc_bcc(f"{str(param).rjust(5, '0')}={hex_value.rjust(4,'0')}{ETX}")
    return f"{ADR}{STX}{str(param).rjust(5, '0')}={hex_value.rjust(4,'0')}{ETX}{bcc}"


class VirtualCentrifuge(SimulatedSerial):
    def __init__(self):
        self.current_temperature = 22
        self.target_temperature = 25
        self.is_rotating = False
        self.target_frequency = min_frequency
        self.actual_frequency = 0
        self.acceleration_time = 1
        self.break_time = 1
        self.cover_closed = True
        self.position_reached = True
        self.software_lock = True
        self.error_byte = 0
        self.num_positions = 48  # only even numbers 0,..,48
        self.target_position = 4  # must be <= num_positions
        self.current_position = 1
        # We read in the real occupancy, but will never write in it
        if "centrifugeOccupancy.json" not in os.listdir():
            self.occupancy = [False for stack in range(4)]
        else:
            with open("centrifugeOccupancy.json", "r") as instream:
                self.occupancy = json.load(instream)
        heater = Thread(target=self.heat, daemon=True)
        heater.start()
        self.acceleration_cmd = None

    def heat(self):
        while True:
            if abs(self.current_temperature - self.target_temperature) < .1:
                self.current_temperature = self.target_temperature
            elif self.current_temperature < self.target_temperature:
                self.current_temperature += .1
            elif self.current_temperature > self.target_temperature:
                self.current_temperature -= .1
            if self.fast_mode:
                sleep(.1)
            else:
                sleep(5)

    def do_enquiry(self, command):
        param = int(command[2:7])
        if param == 520:
            val = lock_centrifuge_code if self.software_lock else unlock_centrifuge_code
        elif param == 524:
            val = self.target_position + self.num_positions*2**8
        elif param == 528:
            val = 2**12 if self.cover_closed else 2**13
            val |= int(self.target_position == self.current_position) * 2**2
        elif param == 603:
            val = self.target_frequency
        elif param == 604:
            val = self.actual_frequency
        elif param == 605:
            val = max_frequency
        elif param == 611:
            val = self.acceleration_time
        elif param == 611:
            val = self.break_time
        elif param == 618:
            val = encode_temperature(round(self.target_temperature*2)/2)
        elif param == 619:
            val = encode_temperature(round(self.current_temperature*2)/2)
        elif param == 685:  # SIOF
            val = self.error_byte
            self.error_byte = 0
        else:
            self.error_byte = 1
            return not_accepted
        return construct_answer(param, val)

    def accelerate(self):
        acc = round(self.target_frequency/self.acceleration_time)
        self.is_rotating = True
        while not self.actual_frequency == self.target_frequency and self.acceleration_cmd == "rotate":
            self.actual_frequency += acc
            self.actual_frequency = min(self.target_frequency, self.actual_frequency)
            self.do_stuff(1)

    def slow_down(self):
        acc = round(self.target_frequency/self.break_time)
        while not self.actual_frequency > 0 and self.acceleration_cmd == "break":
            self.actual_frequency -= acc
            self.actual_frequency = max(0, self.actual_frequency)
            self.do_stuff(1)
        self.is_rotating = self.actual_frequency == 0

    def process_command(self, command):
        answer = not_accepted
        if self.error_byte:  # rotanta does not accept command while error byte is set
            return answer
        param = int(command[3:8])  # is given in decimal
        val = int(command[9:13], 16)  # is given in hex
        if param == 520:
            if val in [lock_centrifuge_code, unlock_centrifuge_code]:
                answer = accepted
        if param == 521:
            if self.software_lock:
                self.error_byte = 1
            elif val in [start_centrifuge_code, stop_centrifuge_code]:
                answer = accepted
            else:
                self.error_byte = 1
        if param == 524:
            answer = accepted
        if param == 526:
            answer = accepted
        if param == 603:
            if val in range(min_frequency, max_frequency+1):
                answer = accepted
            else:
                self.error_byte = 1
        if param in [611, 612]:
            answer = accepted
        if param == 618:
            if -20 <= decode_temperature(val) <= 40:
                answer = accepted
            else:
                self.error_byte = 1

        if answer == accepted:
            t = Thread(target=self.simulate_command, args=[param, val], daemon=True)
            t.start()
        if answer == not_accepted:
            self.error_byte = 1
        return answer

    # simulate the execution here
    def simulate_command(self, param, val):
        if param == 520:
            self.software_lock = val == lock_centrifuge_code
        if param == 521:  # start/stop rotation
            if val == start_centrifuge_code:
                self.acceleration_cmd = "rotate"
                t = Thread(target=self.accelerate, daemon=True)
            if val == stop_centrifuge_code:
                self.acceleration_cmd = "break"
                t = Thread(target=self.slow_down, daemon=True)
            t.start()
        if param == 524:
            self.target_position = val
        if param == 526:
            if val == MoveCommand.CLOSE_COVER.value:
                if not self.cover_closed:
                    self.do_stuff(1)
                    self.cover_closed = True
            elif val == MoveCommand.OPEN_COVER.value:
                if self.cover_closed:
                    self.do_stuff(1)
                    self.cover_closed = False
            elif val in [MoveCommand.POSITION_FAST.value, MoveCommand.POSITION_SLOW.value]:
                # don't care about speed in simulation
                if not self.current_position == self.target_position:
                    self.do_stuff(1)
                    self.current_position = self.target_position
        if param == 603:
            self.target_frequency = val
        if param == 611:
            self.acceleration_time = val
        if param == 612:
            self.break_time = val
        if param == 618:
            self.target_temperature = decode_temperature(val)

    def get_answer(self, command):
        pass

    def get_centrifuge_answer(self, command, terminator='\r', encoding='utf-8', max_wait=2):
        sleep(.02)
        answer = not_accepted  # by default
        try:
            if re.match(enquiry_pattern, command):  # a enquiry
                answer = self.do_enquiry(command)
            elif re.match(command_pattern, command):  # a select-command
                answer = self.process_command(command)
            else:
                logging.debug(f"got command with unknown pattern: |{command}| , length:{len(command)}")
        except Exception as e:
            logging.error(str(e))
            logging.debug("Some error in command processing. Returning unknown-command-error")
            answer = "unknown error"

        # actually simulate doing something if the answer is ok
        logging.debug(f"Got command {command}. Answered {answer}")
        return answer

    def do_stuff(self, duration):
        if not self.fast_mode:
            sleep(duration)

    def fatal_crash(self, msg=''):
        logging.error(f"AAAAAHHHHHHHRGGGGGG!!!!!!  {msg}")

    def __str__(self):
        d = 10
        o = 15
        e = "".rjust(o, ' ')
        s = "\n"+e+e+"I am a centrifuge :-)\n"
        s += e+f"(Temperature: {self.current_temperature} --> {self.target_temperature})\n"
        cover = "==========" if self.cover_closed else "|        |"
        s += e+cover.rjust(round(2.5*d), "=").ljust(round(4.5*d), '=')+"\n"
        rotating = "ROTATING" if self.is_rotating else "STILL"
        rotating += f"({'LOCK' if self.software_lock else 'FREE'})"
        frequency = f"({self.actual_frequency}->{self.target_frequency})"
        s += rotating.ljust(o, ' ') + \
            f"{''.join([f'Slot_{i+1}'.rjust(d,' ') for i in range(4)])}\n"
        if 'stacker' in self.__dir__():
            self.occupancy = self.stacker.occupied
        s += frequency.ljust(o, ' ') + \
            f"{''.join(['FULL'.rjust(d, ' ') if self.occupancy[i] else 'EMPTY'.rjust(d, ' ') for i in range(4)])}\n"
        s += f"current_pos:|" \
             f"{''.join(['X' if i == d*self.current_position-1 else ' ' for i in range(self.num_positions)])}|\n"
        s += f"target_pos: |" \
             f"{''.join(['X' if i == d*self.target_position-1 else ' ' for i in range(self.num_positions)])}|\n"
        return s


class RotantaProtocolSimualed(RotantaProtocol, VirtualCentrifuge):
    def __init__(self):
        RotantaProtocol.__init__(self, self)
        VirtualCentrifuge.__init__(self)
