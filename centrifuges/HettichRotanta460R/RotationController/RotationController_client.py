#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HettichRotanta460R client*

:details: HettichRotanta460R:
    SiLA2 service for Hettich Rotanta 460R centrifuge

:file:    RotationController_client.py
:authors: mark doerr, stefan maak

:date: (creation)          2021-08-16T15:48:19.498600
:date: (last modification) 2021-08-16T15:48:19.498600

.. note:: Code generated by sila2codegenerator 0.3.6

_______________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"

# import general packages
import logging
import argparse
import grpc
import time
import datetime

# import meta packages
from typing import Union, Optional

# import SiLA2 library modules
from sila2lib.framework import SiLAFramework_pb2 as silaFW_pb2
from sila2lib.sila_client import SiLA2Client
from sila2lib.framework.std_features import SiLAService_pb2 as SiLAService_feature_pb2
from sila2lib.error_handling import client_err
from sila2lib.error_handling.client_err import SiLAClientError
import sila2lib.utils.py2sila_types as p2s
#   Usually not needed, but - feel free to modify
# from sila2lib.framework.std_features import SimulationController_pb2 as SimController_feature_pb2

# import feature gRPC modules
# Import gRPC libraries of features
from CancelController.gRPC import CancelController_pb2
from CancelController.gRPC import CancelController_pb2_grpc
# import default arguments for this feature
from CancelController.CancelController_default_arguments import default_dict as CancelController_default_dict
from PauseController.gRPC import PauseController_pb2
from PauseController.gRPC import PauseController_pb2_grpc
# import default arguments for this feature
from PauseController.PauseController_default_arguments import default_dict as PauseController_default_dict
from CoverController.gRPC import CoverController_pb2
from CoverController.gRPC import CoverController_pb2_grpc
# import default arguments for this feature
from CoverController.CoverController_default_arguments import default_dict as CoverController_default_dict
from TemperatureController2.gRPC import TemperatureController2_pb2
from TemperatureController2.gRPC import TemperatureController2_pb2_grpc
# import default arguments for this feature
from TemperatureController2.TemperatureController2_default_arguments import default_dict as TemperatureController2_default_dict
from StackerController1d.gRPC import StackerController1d_pb2
from StackerController1d.gRPC import StackerController1d_pb2_grpc
# import default arguments for this feature
from StackerController1d.StackerController1d_default_arguments import default_dict as StackerController1d_default_dict
from RotationController.gRPC import RotationController_pb2
from RotationController.gRPC import RotationController_pb2_grpc
# import default arguments for this feature
from RotationController.RotationController_default_arguments import default_dict as RotationController_default_dict
from CentrifugeDataProvider.gRPC import CentrifugeDataProvider_pb2
from CentrifugeDataProvider.gRPC import CentrifugeDataProvider_pb2_grpc
# import default arguments for this feature
from CentrifugeDataProvider.CentrifugeDataProvider_default_arguments import default_dict as CentrifugeDataProvider_default_dict
from RobotInteractionServiceRotanta.gRPC import RobotInteractionServiceRotanta_pb2
from RobotInteractionServiceRotanta.gRPC import RobotInteractionServiceRotanta_pb2_grpc
# import default arguments for this feature
from RobotInteractionServiceRotanta.RobotInteractionServiceRotanta_default_arguments import default_dict as RobotInteractionServiceRotanta_default_dict


# noinspection PyPep8Naming, PyUnusedLocal
class RotationControllerClient:
    """
        SiLA2 service for Hettich Rotanta 460R centrifuge

    .. note:: For an example on how to construct the parameter or read the response(s) for command calls and properties,
              compare the default dictionary that is stored in the directory of the corresponding feature.
    """
    # The following variables will be filled when run() is executed
    #: Storage for the connected servers version
    server_version: str = ''
    #: Storage for the display name of the connected server
    server_display_name: str = ''
    #: Storage for the description of the connected server
    server_description: str = ''

    def __init__(self,
                 channel = None):
        """Class initialiser"""

        # Create stub objects used to communicate with the server
        self.RotationController_stub = \
            RotationController_pb2_grpc.RotationControllerStub(channel)

        # initialise class variables for server information storage
        self.server_version = ''
        self.server_display_name = ''
        self.server_description = ''

    def SetSpeed(self,
                Speed: int = 1
                     ): # -> (RotationController):
        """
        Wrapper to call the unobservable command SetSpeed on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling SetSpeed:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RotationController_pb2.SetSpeed_Parameters(
                                    Speed=silaFW_pb2.Integer(value=Speed)
                )
    
            response = self.RotationController_stub.SetSpeed(parameter, metadata)
            logging.debug(f"SetSpeed response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return 
    
    
    def SetAcceleration(self,
                Acceleration: int = 1
                     ): # -> (RotationController):
        """
        Wrapper to call the unobservable command SetAcceleration on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling SetAcceleration:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RotationController_pb2.SetAcceleration_Parameters(
                                    Acceleration=silaFW_pb2.Integer(value=Acceleration)
                )
    
            response = self.RotationController_stub.SetAcceleration(parameter, metadata)
            logging.debug(f"SetAcceleration response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return 
    
    
    def SetRadius(self,
                Radius: float = 1.0
                     ): # -> (RotationController):
        """
        Wrapper to call the unobservable command SetRadius on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling SetRadius:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RotationController_pb2.SetRadius_Parameters(
                                    Radius=silaFW_pb2.Real(value=Radius)
                )
    
            response = self.RotationController_stub.SetRadius(parameter, metadata)
            logging.debug(f"SetRadius response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return 
    
    
    def StartRotating(self,
                EmptyParameter: bytes = b''
                     ): # -> (RotationController):
        """
        Wrapper to call the unobservable command StartRotating on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling StartRotating:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RotationController_pb2.StartRotating_Parameters(
                                    
                )
    
            response = self.RotationController_stub.StartRotating(parameter, metadata)
            logging.debug(f"StartRotating response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return 
    
    
    def StopRotating(self,
                EmptyParameter: bytes = b''
                     ): # -> (RotationController):
        """
        Wrapper to call the unobservable command StopRotating on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        parameter = None
        metadata = None  # add metadata generator here 
    
        logging.debug("Calling StopRotating:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RotationController_pb2.StopRotating_Parameters(
                                    
                )
    
            response = self.RotationController_stub.StopRotating(parameter, metadata)
            logging.debug(f"StopRotating response: {response}")
    
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return 
    
    
    def RunProtocol(self,
                      parameter: RotationController_pb2.RunProtocol_Parameters = None) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Wrapper to call the observable command RunProtocol on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A command confirmation object with the following information:
            commandExecutionUUID: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution (optional): The (maximum) lifetime of this command call.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Calling RunProtocol:")
        try:
            # resolve to default if no value given
            #   TODO: Implement a more reasonable default value
            if parameter is None:
                parameter = RotationController_pb2.RunProtocol_Parameters(
                    **RotationController_default_dict['RunProtocol_Parameters']
                )
    
            response = self.RotationController_stub.RunProtocol(parameter)
    
            logging.debug('RunProtocol response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def RunProtocol_Info(self,
                           uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Wrapper to get an intermediate response for the observable command RunProtocol on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the status information that has been defined for this command. The following fields
                  are defined:
                    * *commandStatus*: Status of the command (enumeration)
                    * *progressInfo*: Information on the progress of the command (0 to 1)
                    * *estimatedRemainingTime*: Estimate of the remaining time required to run the command
                    * *updatedLifetimeOfExecution*: An update on the execution lifetime
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command RunProtocol (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
        try:
            response = self.RotationController_stub.RunProtocol_Info(uuid)
            logging.debug('RunProtocol status information: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    
    def RunProtocol_Result(self,
                             uuid: Union[str, silaFW_pb2.CommandExecutionUUID]) \
            -> RotationController_pb2.RunProtocol_Responses:
        """
        Wrapper to get an intermediate response for the observable command RunProtocol on the server.
    
        :param uuid: The UUID that has been returned with the first command call. Can be given as string or as the
                     corresponding SiLA2 gRPC object.
    
        :returns: A gRPC object with the result response that has been defined for this command.
    
        .. note:: Whether the result is available or not can and should be evaluated by calling the
                  :meth:`RunProtocol_Info` method of this call.
        """
        if type(uuid) is str:
            uuid = silaFW_pb2.CommandExecutionUUID(value=uuid)
    
        logging.debug(
            "Requesting status information for command RunProtocol (UUID={uuid}):".format(
                uuid=uuid.value
            )
        )
    
        try:
            response = self.RotationController_stub.RunProtocol_Result(uuid)
            logging.debug('RunProtocol result response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response
    

    def Get_Speed(self) \
            -> RotationController_pb2.Get_Speed_Responses:
        """Wrapper to get property Speed from the server."""
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Reading unobservable property Speed:")
        try:
            response = self.RotationController_stub.Get_Speed(
                RotationController_pb2.Get_Speed_Parameters()
            )
            logging.debug(
                'Get_Speed response: {response}'.format(
                    response=response
                )
            )
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response.Speed.value
    
    def Get_Acceleration(self) \
            -> RotationController_pb2.Get_Acceleration_Responses:
        """Wrapper to get property Acceleration from the server."""
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Reading unobservable property Acceleration:")
        try:
            response = self.RotationController_stub.Get_Acceleration(
                RotationController_pb2.Get_Acceleration_Parameters()
            )
            logging.debug(
                'Get_Acceleration response: {response}'.format(
                    response=response
                )
            )
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response.Acceleration.value
    
    def Get_Radius(self) \
            -> RotationController_pb2.Get_Radius_Responses:
        """Wrapper to get property Radius from the server."""
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call
    
        logging.debug("Reading unobservable property Radius:")
        try:
            response = self.RotationController_stub.Get_Radius(
                RotationController_pb2.Get_Radius_Parameters()
            )
            logging.debug(
                'Get_Radius response: {response}'.format(
                    response=response
                )
            )
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None
    
        return response.Radius.value

    @staticmethod
    def grpc_error_handling(error_object: grpc.Call) -> None:
        """Handles exceptions of type grpc.RpcError"""
        # pass to the default error handling
        grpc_error = client_err.grpc_error_handling(error_object=error_object)

        logging.error(grpc_error.error_type)
        if hasattr(grpc_error.message, "parameter"):
            logging.error(grpc_error.message.parameter)
        logging.error(grpc_error.message.message)
        if grpc_error.error_type == client_err.SiLAError.DEFINED_EXECUTION_ERROR:
            if grpc_error.message.errorIdentifier == 'RotorIsLocked':
                raise RotorIsLockedError(grpc_error.message.message)
        if grpc_error.error_type == client_err.SiLAError.DEFINED_EXECUTION_ERROR:
            if grpc_error.message.errorIdentifier == 'RotorNotBalanced':
                raise RotorNotBalancedError(grpc_error.message.message)
        

class RotorIsLockedError(SiLAClientError):
    """RotorIsLocked
    """
    pass


class RotorNotBalancedError(SiLAClientError):
    """Rotor Not Balanced
    """
    pass


