# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: RotationController.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='RotationController.proto',
  package='sila2.org.silastandard.instruments.rotationcontroller.v1',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x18RotationController.proto\x12\x38sila2.org.silastandard.instruments.rotationcontroller.v1\x1a\x13SiLAFramework.proto\"E\n\x13SetSpeed_Parameters\x12.\n\x05Speed\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x14\n\x12SetSpeed_Responses\"S\n\x1aSetAcceleration_Parameters\x12\x35\n\x0c\x41\x63\x63\x65leration\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1b\n\x19SetAcceleration_Responses\"D\n\x14SetRadius_Parameters\x12,\n\x06Radius\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"\x15\n\x13SetRadius_Responses\"\x1a\n\x18StartRotating_Parameters\"\x19\n\x17StartRotating_Responses\"\x19\n\x17StopRotating_Parameters\"\x18\n\x16StopRotating_Responses\"J\n\x16RunProtocol_Parameters\x12\x30\n\x08Protocol\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"M\n\x15RunProtocol_Responses\x12\x34\n\x0bSecondsLeft\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x16\n\x14Get_Speed_Parameters\"E\n\x13Get_Speed_Responses\x12.\n\x05Speed\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1d\n\x1bGet_Acceleration_Parameters\"S\n\x1aGet_Acceleration_Responses\x12\x35\n\x0c\x41\x63\x63\x65leration\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x17\n\x15Get_Radius_Parameters\"D\n\x14Get_Radius_Responses\x12,\n\x06Radius\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real2\xde\x0e\n\x12RotationController\x12\xa9\x01\n\x08SetSpeed\x12M.sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Parameters\x1aL.sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Responses\"\x00\x12\xbe\x01\n\x0fSetAcceleration\x12T.sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Parameters\x1aS.sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Responses\"\x00\x12\xac\x01\n\tSetRadius\x12N.sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Parameters\x1aM.sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Responses\"\x00\x12\xb8\x01\n\rStartRotating\x12R.sila2.org.silastandard.instruments.rotationcontroller.v1.StartRotating_Parameters\x1aQ.sila2.org.silastandard.instruments.rotationcontroller.v1.StartRotating_Responses\"\x00\x12\xb5\x01\n\x0cStopRotating\x12Q.sila2.org.silastandard.instruments.rotationcontroller.v1.StopRotating_Parameters\x1aP.sila2.org.silastandard.instruments.rotationcontroller.v1.StopRotating_Responses\"\x00\x12\x8e\x01\n\x0bRunProtocol\x12P.sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Parameters\x1a+.sila2.org.silastandard.CommandConfirmation\"\x00\x12k\n\x10RunProtocol_Info\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a%.sila2.org.silastandard.ExecutionInfo\"\x00\x30\x01\x12\x95\x01\n\x12RunProtocol_Result\x12,.sila2.org.silastandard.CommandExecutionUUID\x1aO.sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Responses\"\x00\x12\xac\x01\n\tGet_Speed\x12N.sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Parameters\x1aM.sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Responses\"\x00\x12\xc1\x01\n\x10Get_Acceleration\x12U.sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Parameters\x1aT.sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Responses\"\x00\x12\xaf\x01\n\nGet_Radius\x12O.sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Parameters\x1aN.sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Responses\"\x00\x62\x06proto3'
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_SETSPEED_PARAMETERS = _descriptor.Descriptor(
  name='SetSpeed_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Speed', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Parameters.Speed', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=107,
  serialized_end=176,
)


_SETSPEED_RESPONSES = _descriptor.Descriptor(
  name='SetSpeed_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=178,
  serialized_end=198,
)


_SETACCELERATION_PARAMETERS = _descriptor.Descriptor(
  name='SetAcceleration_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Acceleration', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Parameters.Acceleration', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=200,
  serialized_end=283,
)


_SETACCELERATION_RESPONSES = _descriptor.Descriptor(
  name='SetAcceleration_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=285,
  serialized_end=312,
)


_SETRADIUS_PARAMETERS = _descriptor.Descriptor(
  name='SetRadius_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Radius', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Parameters.Radius', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=314,
  serialized_end=382,
)


_SETRADIUS_RESPONSES = _descriptor.Descriptor(
  name='SetRadius_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=384,
  serialized_end=405,
)


_STARTROTATING_PARAMETERS = _descriptor.Descriptor(
  name='StartRotating_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.StartRotating_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=407,
  serialized_end=433,
)


_STARTROTATING_RESPONSES = _descriptor.Descriptor(
  name='StartRotating_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.StartRotating_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=435,
  serialized_end=460,
)


_STOPROTATING_PARAMETERS = _descriptor.Descriptor(
  name='StopRotating_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.StopRotating_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=462,
  serialized_end=487,
)


_STOPROTATING_RESPONSES = _descriptor.Descriptor(
  name='StopRotating_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.StopRotating_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=489,
  serialized_end=513,
)


_RUNPROTOCOL_PARAMETERS = _descriptor.Descriptor(
  name='RunProtocol_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Protocol', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Parameters.Protocol', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=515,
  serialized_end=589,
)


_RUNPROTOCOL_RESPONSES = _descriptor.Descriptor(
  name='RunProtocol_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='SecondsLeft', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Responses.SecondsLeft', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=591,
  serialized_end=668,
)


_GET_SPEED_PARAMETERS = _descriptor.Descriptor(
  name='Get_Speed_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=670,
  serialized_end=692,
)


_GET_SPEED_RESPONSES = _descriptor.Descriptor(
  name='Get_Speed_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Speed', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Responses.Speed', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=694,
  serialized_end=763,
)


_GET_ACCELERATION_PARAMETERS = _descriptor.Descriptor(
  name='Get_Acceleration_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=765,
  serialized_end=794,
)


_GET_ACCELERATION_RESPONSES = _descriptor.Descriptor(
  name='Get_Acceleration_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Acceleration', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Responses.Acceleration', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=796,
  serialized_end=879,
)


_GET_RADIUS_PARAMETERS = _descriptor.Descriptor(
  name='Get_Radius_Parameters',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=881,
  serialized_end=904,
)


_GET_RADIUS_RESPONSES = _descriptor.Descriptor(
  name='Get_Radius_Responses',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Radius', full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Responses.Radius', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=906,
  serialized_end=974,
)

_SETSPEED_PARAMETERS.fields_by_name['Speed'].message_type = SiLAFramework__pb2._INTEGER
_SETACCELERATION_PARAMETERS.fields_by_name['Acceleration'].message_type = SiLAFramework__pb2._INTEGER
_SETRADIUS_PARAMETERS.fields_by_name['Radius'].message_type = SiLAFramework__pb2._REAL
_RUNPROTOCOL_PARAMETERS.fields_by_name['Protocol'].message_type = SiLAFramework__pb2._STRING
_RUNPROTOCOL_RESPONSES.fields_by_name['SecondsLeft'].message_type = SiLAFramework__pb2._INTEGER
_GET_SPEED_RESPONSES.fields_by_name['Speed'].message_type = SiLAFramework__pb2._INTEGER
_GET_ACCELERATION_RESPONSES.fields_by_name['Acceleration'].message_type = SiLAFramework__pb2._INTEGER
_GET_RADIUS_RESPONSES.fields_by_name['Radius'].message_type = SiLAFramework__pb2._REAL
DESCRIPTOR.message_types_by_name['SetSpeed_Parameters'] = _SETSPEED_PARAMETERS
DESCRIPTOR.message_types_by_name['SetSpeed_Responses'] = _SETSPEED_RESPONSES
DESCRIPTOR.message_types_by_name['SetAcceleration_Parameters'] = _SETACCELERATION_PARAMETERS
DESCRIPTOR.message_types_by_name['SetAcceleration_Responses'] = _SETACCELERATION_RESPONSES
DESCRIPTOR.message_types_by_name['SetRadius_Parameters'] = _SETRADIUS_PARAMETERS
DESCRIPTOR.message_types_by_name['SetRadius_Responses'] = _SETRADIUS_RESPONSES
DESCRIPTOR.message_types_by_name['StartRotating_Parameters'] = _STARTROTATING_PARAMETERS
DESCRIPTOR.message_types_by_name['StartRotating_Responses'] = _STARTROTATING_RESPONSES
DESCRIPTOR.message_types_by_name['StopRotating_Parameters'] = _STOPROTATING_PARAMETERS
DESCRIPTOR.message_types_by_name['StopRotating_Responses'] = _STOPROTATING_RESPONSES
DESCRIPTOR.message_types_by_name['RunProtocol_Parameters'] = _RUNPROTOCOL_PARAMETERS
DESCRIPTOR.message_types_by_name['RunProtocol_Responses'] = _RUNPROTOCOL_RESPONSES
DESCRIPTOR.message_types_by_name['Get_Speed_Parameters'] = _GET_SPEED_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_Speed_Responses'] = _GET_SPEED_RESPONSES
DESCRIPTOR.message_types_by_name['Get_Acceleration_Parameters'] = _GET_ACCELERATION_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_Acceleration_Responses'] = _GET_ACCELERATION_RESPONSES
DESCRIPTOR.message_types_by_name['Get_Radius_Parameters'] = _GET_RADIUS_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_Radius_Responses'] = _GET_RADIUS_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SetSpeed_Parameters = _reflection.GeneratedProtocolMessageType('SetSpeed_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETSPEED_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Parameters)
  })
_sym_db.RegisterMessage(SetSpeed_Parameters)

SetSpeed_Responses = _reflection.GeneratedProtocolMessageType('SetSpeed_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETSPEED_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.SetSpeed_Responses)
  })
_sym_db.RegisterMessage(SetSpeed_Responses)

SetAcceleration_Parameters = _reflection.GeneratedProtocolMessageType('SetAcceleration_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETACCELERATION_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Parameters)
  })
_sym_db.RegisterMessage(SetAcceleration_Parameters)

SetAcceleration_Responses = _reflection.GeneratedProtocolMessageType('SetAcceleration_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETACCELERATION_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.SetAcceleration_Responses)
  })
_sym_db.RegisterMessage(SetAcceleration_Responses)

SetRadius_Parameters = _reflection.GeneratedProtocolMessageType('SetRadius_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETRADIUS_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Parameters)
  })
_sym_db.RegisterMessage(SetRadius_Parameters)

SetRadius_Responses = _reflection.GeneratedProtocolMessageType('SetRadius_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETRADIUS_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.SetRadius_Responses)
  })
_sym_db.RegisterMessage(SetRadius_Responses)

StartRotating_Parameters = _reflection.GeneratedProtocolMessageType('StartRotating_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _STARTROTATING_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.StartRotating_Parameters)
  })
_sym_db.RegisterMessage(StartRotating_Parameters)

StartRotating_Responses = _reflection.GeneratedProtocolMessageType('StartRotating_Responses', (_message.Message,), {
  'DESCRIPTOR' : _STARTROTATING_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.StartRotating_Responses)
  })
_sym_db.RegisterMessage(StartRotating_Responses)

StopRotating_Parameters = _reflection.GeneratedProtocolMessageType('StopRotating_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _STOPROTATING_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.StopRotating_Parameters)
  })
_sym_db.RegisterMessage(StopRotating_Parameters)

StopRotating_Responses = _reflection.GeneratedProtocolMessageType('StopRotating_Responses', (_message.Message,), {
  'DESCRIPTOR' : _STOPROTATING_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.StopRotating_Responses)
  })
_sym_db.RegisterMessage(StopRotating_Responses)

RunProtocol_Parameters = _reflection.GeneratedProtocolMessageType('RunProtocol_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _RUNPROTOCOL_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Parameters)
  })
_sym_db.RegisterMessage(RunProtocol_Parameters)

RunProtocol_Responses = _reflection.GeneratedProtocolMessageType('RunProtocol_Responses', (_message.Message,), {
  'DESCRIPTOR' : _RUNPROTOCOL_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.RunProtocol_Responses)
  })
_sym_db.RegisterMessage(RunProtocol_Responses)

Get_Speed_Parameters = _reflection.GeneratedProtocolMessageType('Get_Speed_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_SPEED_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Parameters)
  })
_sym_db.RegisterMessage(Get_Speed_Parameters)

Get_Speed_Responses = _reflection.GeneratedProtocolMessageType('Get_Speed_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_SPEED_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Speed_Responses)
  })
_sym_db.RegisterMessage(Get_Speed_Responses)

Get_Acceleration_Parameters = _reflection.GeneratedProtocolMessageType('Get_Acceleration_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_ACCELERATION_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Parameters)
  })
_sym_db.RegisterMessage(Get_Acceleration_Parameters)

Get_Acceleration_Responses = _reflection.GeneratedProtocolMessageType('Get_Acceleration_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_ACCELERATION_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Acceleration_Responses)
  })
_sym_db.RegisterMessage(Get_Acceleration_Responses)

Get_Radius_Parameters = _reflection.GeneratedProtocolMessageType('Get_Radius_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_RADIUS_PARAMETERS,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Parameters)
  })
_sym_db.RegisterMessage(Get_Radius_Parameters)

Get_Radius_Responses = _reflection.GeneratedProtocolMessageType('Get_Radius_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_RADIUS_RESPONSES,
  '__module__' : 'RotationController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.rotationcontroller.v1.Get_Radius_Responses)
  })
_sym_db.RegisterMessage(Get_Radius_Responses)



_ROTATIONCONTROLLER = _descriptor.ServiceDescriptor(
  name='RotationController',
  full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=977,
  serialized_end=2863,
  methods=[
  _descriptor.MethodDescriptor(
    name='SetSpeed',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.SetSpeed',
    index=0,
    containing_service=None,
    input_type=_SETSPEED_PARAMETERS,
    output_type=_SETSPEED_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='SetAcceleration',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.SetAcceleration',
    index=1,
    containing_service=None,
    input_type=_SETACCELERATION_PARAMETERS,
    output_type=_SETACCELERATION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='SetRadius',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.SetRadius',
    index=2,
    containing_service=None,
    input_type=_SETRADIUS_PARAMETERS,
    output_type=_SETRADIUS_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='StartRotating',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.StartRotating',
    index=3,
    containing_service=None,
    input_type=_STARTROTATING_PARAMETERS,
    output_type=_STARTROTATING_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='StopRotating',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.StopRotating',
    index=4,
    containing_service=None,
    input_type=_STOPROTATING_PARAMETERS,
    output_type=_STOPROTATING_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='RunProtocol',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.RunProtocol',
    index=5,
    containing_service=None,
    input_type=_RUNPROTOCOL_PARAMETERS,
    output_type=SiLAFramework__pb2._COMMANDCONFIRMATION,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='RunProtocol_Info',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.RunProtocol_Info',
    index=6,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=SiLAFramework__pb2._EXECUTIONINFO,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='RunProtocol_Result',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.RunProtocol_Result',
    index=7,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=_RUNPROTOCOL_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Get_Speed',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.Get_Speed',
    index=8,
    containing_service=None,
    input_type=_GET_SPEED_PARAMETERS,
    output_type=_GET_SPEED_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Get_Acceleration',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.Get_Acceleration',
    index=9,
    containing_service=None,
    input_type=_GET_ACCELERATION_PARAMETERS,
    output_type=_GET_ACCELERATION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Get_Radius',
    full_name='sila2.org.silastandard.instruments.rotationcontroller.v1.RotationController.Get_Radius',
    index=10,
    containing_service=None,
    input_type=_GET_RADIUS_PARAMETERS,
    output_type=_GET_RADIUS_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_ROTATIONCONTROLLER)

DESCRIPTOR.services_by_name['RotationController'] = _ROTATIONCONTROLLER

# @@protoc_insertion_point(module_scope)
