# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import RotationController_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()


default_dict['SetSpeed_Parameters'] = {
    'Speed': silaFW_pb2.Integer(value=1)
}

default_dict['SetSpeed_Responses'] = {
    
}

default_dict['SetAcceleration_Parameters'] = {
    'Acceleration': silaFW_pb2.Integer(value=1)
}

default_dict['SetAcceleration_Responses'] = {
    
}

default_dict['SetRadius_Parameters'] = {
    'Radius': silaFW_pb2.Real(value=1.0)
}

default_dict['SetRadius_Responses'] = {
    
}

default_dict['StartRotating_Parameters'] = {
    
}

default_dict['StartRotating_Responses'] = {
    
}

default_dict['StopRotating_Parameters'] = {
    
}

default_dict['StopRotating_Responses'] = {
    
}

default_dict['RunProtocol_Parameters'] = {
    'Protocol': silaFW_pb2.String(value='default string')
}

default_dict['RunProtocol_Responses'] = {
    'SecondsLeft': silaFW_pb2.Integer(value=1)
}

default_dict['Get_Speed_Responses'] = {
    'Speed': silaFW_pb2.Integer(value=1)
}

default_dict['Get_Acceleration_Responses'] = {
    'Acceleration': silaFW_pb2.Integer(value=1)
}

default_dict['Get_Radius_Responses'] = {
    'Radius': silaFW_pb2.Real(value=1.0)
}
