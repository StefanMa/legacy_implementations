"""
A unittest for the cytomat client-server pair. It uses the virtual_cytomat to emulate a real one.
The test can be run in fast-mode for fast testing or in real mode for realistic reaction/working times of the device.
"""
from time import sleep
import unittest
from sila2lib.error_handling.client_err import SiLAClientError
from sila2_simulation.deviceTest import DeviceTest, parse_command_line
from CoverController.CoverController_client import CoverOperationInterruptedError
from RotationController.RotationController_client import RotorNotBalancedError
from HettichRotanta460R_client import HettichRotanta460RClient
from HettichRotanta460R_server import HettichRotanta460RServer


class CentrifugeTest(DeviceTest, unittest.TestCase):
    def is_error(self, feedback) -> bool:
        return issubclass(type(feedback), SiLAClientError)

    def setUp(self, port=50051) -> None:
        super().setUp()
        self.server = HettichRotanta460RServer(cmd_args=self.create_args(port=port),
                                               simulation_mode=True, block=False)
        # give the server some time to register with zeroconfig
        self.server.hardware_interface.get_in_fast_mode()
        sleep(.5)
        self.client = HettichRotanta460RClient()
        self.generated_files.append("centrifugeOccupancy.json")

    def test_process(self):
        cover_client = self.client.coverController_client
        stacker_client = self.client.stackerController1d_client
        rotation_client = self.client.rotationController_client
        temperature_client = self.client.temperatureController2_client

        self.assertTrue(cover_client.Get_CoverIsClosed())

        self.send_command(cover_client.OpenCover, 1.5, .2)
        self.assertFalse(cover_client.Get_CoverIsClosed())

        self.send_command(stacker_client.LoadContainerToStacker, 4, 1, [1])

        # should not start because cover is open
        self.send_command(rotation_client.StartRotating, to_fail=True)

        self.send_command(cover_client.CloseCover, 1, .2)

        self.send_command(temperature_client.ControlTemperature, .5, .1, args=[310])

        self.send_command(rotation_client.SetSpeed, .5, .2, args=[1000])

        # should not work because unbalanced
        self.send_command(rotation_client.StartRotating, to_fail=True, error_type=RotorNotBalancedError)

        # not we load it balancing. this should work
        self.send_command(cover_client.OpenCover, 1.5, .2)
        self.send_command(stacker_client.LoadContainerToStacker, 4, 1, args=[3])
        self.send_command(stacker_client.LoadContainerToStacker, 4, 1, args=[0])
        self.send_command(stacker_client.LoadContainerToStacker, 4, 1, args=[2])
        self.send_command(cover_client.CloseCover, 1.5, .3)

        self.send_command(rotation_client.SetAcceleration, .2, .2, args=[300])
        # this time it should work
        self.send_command(rotation_client.StartRotating, 5, .5)
        assert rotation_client.Get_Speed() > 0

        # should not open while rotating
        self.send_command(cover_client.OpenCover, to_fail=True, error_type=CoverOperationInterruptedError)

        # stop the centrifuge, open cover
        self.send_command(rotation_client.StopRotating, 1.5, .3)
        self.send_command(cover_client.OpenCover, 1.5, .2)

        # unload one plate, now there should be 3 free positions
        self.send_command(stacker_client.UnloadContainerFromStacker, 4, 1, args=[1])
        self.send_command(stacker_client.Get_NumFreePositions)
        assert self.feedback == 1

        # next free Position should be 1
        self.send_command(stacker_client.Get_NextFreePosition, .2, .2)
        assert self.feedback == 1


if __name__ == '__main__':
    mytest = CentrifugeTest()
    mytest.runSimulation()

