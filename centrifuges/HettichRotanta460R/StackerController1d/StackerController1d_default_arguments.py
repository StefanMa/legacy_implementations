# This file contains default values that are used for the implementations to supply them with 
#   working, albeit mostly useless arguments.
#   You can also use this file as an example to create your custom responses. Feel free to remove
#   Once you have replaced every occurrence of the defaults with more reasonable values.
#   Or you continue using this file, supplying good defaults..

# import the required packages
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
import sila2lib.framework.SiLABinaryTransfer_pb2 as silaBinary_pb2
from .gRPC import StackerController1d_pb2 as pb2

# initialise the default dictionary so we can add keys. 
#   We need to do this separately/add keys separately, so we can access keys already defined e.g.
#   for the use in data type identifiers
default_dict = dict()


default_dict['LoadContainerToStacker_Parameters'] = {
    'StackNumber': silaFW_pb2.Integer(value=1),
    'ContainerPosition': silaFW_pb2.Integer(value=1),
    'TransferStation': silaFW_pb2.Integer(value=1)
}

default_dict['LoadContainerToStacker_Responses'] = {
    
}

default_dict['UnloadContainerFromStacker_Parameters'] = {
    'StackNumber': silaFW_pb2.Integer(value=1),
    'ContainerPosition': silaFW_pb2.Integer(value=1),
    'TransferStation': silaFW_pb2.Integer(value=1)
}

default_dict['UnloadContainerFromStacker_Responses'] = {
    
}

default_dict['NullifyOccupancy_Parameters'] = {
    
}

default_dict['NullifyOccupancy_Responses'] = {
    
}

default_dict['Get_NumFreePositions_Responses'] = {
    'NumFreePositions': silaFW_pb2.Integer(value=1)
}

default_dict['Get_NextFreePosition_Responses'] = {
    'NextFreePosition': pb2.Get_NextFreePosition_Responses.NextFreePosition_Struct(Stack=silaFW_pb2.Integer(value=1), Position=silaFW_pb2.Integer(value=1))
}

default_dict['Get_Occupancy_Responses'] = {
    'Occupancy': [silaFW_pb2.Boolean(value=False)]
}

default_dict['Get_IsBalanced_Responses'] = {
    'IsBalanced': silaFW_pb2.Boolean(value=False)
}

default_dict['Get_NumberOfStacks_Responses'] = {
    'NumberOfStacks': silaFW_pb2.Integer(value=1)
}

default_dict['Get_PositionsPerStack_Responses'] = {
    'PositionsPerStack': silaFW_pb2.Integer(value=1)
}
